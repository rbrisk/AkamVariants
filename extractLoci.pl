#!/usr/bin/env perl

use strict;
use warnings;

#use Data::Dumper;
#$Data::Dumper::Sortkeys = 1;

use Carp;
use Getopt::Long;
use Log::Log4perl qw(:easy);
use Pod::Usage;

use Bio::SeqIO;
use IO::Uncompress::AnyUncompress;

my $man = 0;
my $help = 0;
my $logLvlStr = "i";
my $lineWidth = 100;
my ($pathAssembly, $pathLoci, $pathOut, $withDesc);

GetOptions(
		"h|help" => \$help,
		"man"    => \$man,
		"asmb=s" => \$pathAssembly,
		"loci=s" => \$pathLoci,
		"o=s"    => \$pathOut,
		"desc"   => \$withDesc,
		"w=i"    => \$lineWidth,
		"v=s"    => \$logLvlStr,
	) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;

die("Assembly path is required") unless $pathAssembly;
die("Loci list is required") unless $pathLoci;
die("Output path is required") unless $pathOut;

my %logLvlDict = ("d" => $DEBUG, "i" => $INFO, "w" => $WARN, "e" => $ERROR, "f" => $FATAL);
$logLvlStr = "i" unless exists($logLvlDict{$logLvlStr});
Log::Log4perl->easy_init({level => $logLvlDict{$logLvlStr}, layout => "%d %p %m%n"});


sub getFileHandle {
	my ($path) = @_;
	my $cmdTempl = sprintf("| %%s -c > '%s'", $path);
	my ($cmd, $fh);
	if ($path =~ /\.bz2$/) {
		$cmd = sprintf($cmdTempl, "bzip2");
	} elsif ($path =~ /\.gz$/) {
		$cmd = sprintf($cmdTempl, "gzip");
	} elsif ($path =~ /\.xz$/) {
		$cmd = sprintf($cmdTempl, "xz");
	} elsif ($path =~ /\.zip$/) {
		$cmd = sprintf($cmdTempl, "zip");
	} else {
		$cmd = "> $path";
	}
	open($fh, $cmd);
	return $fh;
}


INFO("Loading loci list");
my %loci;
open(my $fhLoci, '<', $pathLoci) or confess("Unable to read the loci list from '$pathLoci'. $!");
my $header = <$fhLoci>;
while (<$fhLoci>) {
	chomp;
	my ($label, $seqId, $start, $stop, $strand) = split("\t");
	$loci{$seqId} = [] unless defined($loci{$seqId});
	# If start is not specified, we begin with position 1
	# If end is not specified, we assume the end position but we do not know what the end position is
	# at this point.
	push(@{$loci{$seqId}}, { 
			-label  => $label, 
			-start  => $start ? $start : 1,
			-stop   => $stop  ? $stop  : 0, 
			-strand => $strand 
		} );	
}

INFO("Extracting loci");
my $fhOut = getFileHandle($pathOut);
my $writer = Bio::SeqIO->new( -fh => $fhOut, -format => 'fasta', -alphabet => 'dna' );
$writer->width($lineWidth) if $lineWidth;
my $fhAssembly = IO::Uncompress::AnyUncompress->new($pathAssembly);
my $reader = Bio::SeqIO->new( -fh => $fhAssembly, -format => 'fasta', -alphabet => 'dna' );
while (my $seq = $reader->next_seq) {
	my $seqId = $seq->display_id;
	next unless defined($loci{$seqId});
	for my $locus (sort { 
				$a->{'-start'} <=> $b->{'-start'} || 
				$a->{'-stop'}  <=> $b->{'-stop'} } @{$loci{$seqId}}) {
		my ($label, $start, $stop, $strand) = @$locus{qw/ -label -start -stop -strand /};
		$stop = $stop ? $stop : $seq->length;
		if ($start > $stop) {
			ERROR("Incorrect coordinates for '$label'. Start should not be bigger than stop.");
			next;
		}
		if ($start > $seq->length) {
			ERROR("Start position '$start' is out of bounds for '$label'. " .
					"Sequence length is " . $seq->length);
			next;
		}
		if ($stop > $seq->length) {
			ERROR("Stop position '$stop' is out of bounds for '$label'. " .
					"Sequence length is " . $seq->length);
			next;
		}
		my $locusSeq = $seq->subseq($start, $stop);
		my $locusSeqObj = Bio::Seq->new( -id  => $label, -seq => $locusSeq);
		if ($withDesc) {
			my $desc = sprintf("%s:%d..%d %s", $seqId, $start, $stop, $strand);
			$locusSeqObj->desc($desc);
		}
		if ($strand eq '-') {
			$locusSeqObj = $locusSeqObj->revcom();
		}
		$writer->write_seq($locusSeqObj);
	}
}
$writer->close();
$reader->close();

INFO("Done");


__END__

=encoding utf8

=head1 NAME

extractLoci.pl - extracts specified loci from the genome assembly

=head1 SYNOPSIS

extractLoci.pl --asmb assembly.fa -loci loci.txt -o loci.fa.xz

=head1 OPTIONS

=over 8

=item B<--asmb>

Path to the genome assembly in FASTA format. The file may be compressed.

=item B<--loci>

Path to the list of loci in a tab delimited format. One-line header is required. The file should contain the following columns: label (used as locus display id in the output), sequence id, start, stop, strand (+ forward, - reverse). If start position is not specified, position 1 is assumed. If stop position is not specified, the last position is assumed. Start position should not be larger than the stop position.

=item B<-o>

Path to the output file in FASTA format. The file may be compressed based on the extension.

=item B<--desc>

(Optional) Flag that instructs the script to add the description to each fasta record. The description has the following format: "seq_id:start..stop strand".

=item B<-w>

(Optional) Line width for the fasta output file. Default: 100

=back

=head1 DESCRIPTION

Extracts the specified list of loci sequences from the genome assembly in FASTA format. If negative strand is specified for a locus, the extracted sequence converted into the reverse complementary sequence. All sequences are writted in the same output file. The output has the same sorting as the input assembly file. Loci within the same sequence are sorted by start position and stop position.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut

