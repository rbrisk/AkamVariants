#!/usr/bin/env perl

use strict;

use Bio::AlignIO;
use Bio::SeqIO;
use Carp;
use File::Basename;
use File::Spec;
use Getopt::Long;
use Log::Log4perl qw(:easy);
use Pod::Usage;

our $delim = "\t";
my $help = 0;
my $man = 0;
my $logLvlStr = "i";
my $alignFnPtrn = qr/^([^_.]+).+$/;
my ($pathGenes, $dirAlign, $pathOut);

GetOptions(
		"h|help" => \$help,
		"man"    => \$man,
		"g=s"    => \$pathGenes,
		"a=s"    => \$dirAlign,
		"o=s"    => \$pathOut,
		"p=s"    => \$alignFnPtrn,
		"l=s"    => \$logLvlStr,
) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;

my %logLvlDict = ("d" => $DEBUG, "i" => $INFO, "w" => $WARN, "e" => $ERROR, "f" => $FATAL);
$logLvlStr = "i" unless exists($logLvlDict{$logLvlStr});
Log::Log4perl->easy_init({level => $logLvlDict{$logLvlStr}, layout => "%d %p %m%n"});

die("Gene list is required") unless $pathGenes;
die("Alignment directory is required") unless $dirAlign;
die("Output path is required") unless $pathOut;
die("Alignment directory ($dirAlign) does not exist") unless -d $dirAlign;


# Reads the gene file
sub getGeneList {
	my ($pathIn) = @_;
	my %genes;
	open(my $fh, "<", $pathIn) or confess("Unable to read the gene list ($pathIn). $!");
	while (<$fh>) {
		chomp;
		$genes{$_} = 0;
	}
	return \%genes;
}


sub getFileList {
	my ($dirAlign, $alignFnPtrn, $genes) = @_;
	my @files;
	for my $f (glob("$dirAlign/*.fa*")) {
		my ($fName, $fPath) = fileparse($f);
		(my $geneName = $fName) =~ s/$alignFnPtrn/$1/;
		if (defined $genes->{$geneName}) {
			# We found the gene
			$genes->{$geneName} = 1;
			# Add to the list of files for processing
			push(@files, $f);
		}
	}
	# Make sure we have found all requested genes
	my @missingGenes = grep { $genes->{$_} == 0 } keys(%$genes);
	if (@missingGenes > 0) {
		my $msg = "Alignments for the following genes is not available:\n";
		if (@missingGenes > 20) {
			$msg .= join(", ", @missingGenes[0..19]) . " (first 20 are shown)...";
		} else {
			$msg .= join(", ", @missingGenes);
		}
		croak($msg);
	}
	return \@files;
}


# Determines the order of the accessions from an alignment file
sub getAccessionList {
	my ($pathIn, $refPos) = @_;
	my @accs;
	open(my $fh, "<", $pathIn) or confess("Unable to read the alignment from ($pathIn). $!");
	my $reader = Bio::SeqIO->new( -fh => $fh, -format => 'fasta', -alphabet => 'dna' );
	while (my $seq = $reader->next_seq()) {
		push(@accs, $seq->display_name());
	}
	return \@accs;
}

#


INFO("Reading the gene list from $pathGenes");
my $genes = getGeneList($pathGenes);

INFO("Looking up the alignment files");
my $alignmentFiles = getFileList($dirAlign, $alignFnPtrn, $genes);

INFO("Determining accession order");
DEBUG("Accession order is based on " . $alignmentFiles->[0]);
my $accs = getAccessionList($alignmentFiles->[0]);
INFO("Found " . scalar(@$accs) . " accessions");

INFO("Concatenating the data");
# This is where concatenated sequences are stored for each accession
my %accSeqs;
for my $a (@$accs) {
	$accSeqs{$a} = "";
}
# We need to make sure that all accessions are present in each file. Just in case...
for my $f (@$alignmentFiles) {
	my $accCnt = 0;
	open(my $fh, "<", $f) or confress("Unable to read the alignment from ($f). $!");
	my $reader = Bio::SeqIO->new(-fh => $fh, -format => 'fasta', -alphabet => 'dna');
	while (my $seq = $reader->next_seq()) {
		my $accName = $seq->display_name();
		confess("Invalid accession ($accName) in ($f).") unless defined($accSeqs{$accName});
		$accSeqs{$accName} .= $seq->seq;
		$accCnt++;
	}
	if ($accCnt != scalar(@$accs)) {
		confess("($f) is incomplete. Expected " . scalar(@$accs) . " accessions but found $accCnt.");
	}
}

# Unfortunately, we cannot just write out features, they have to be wrapped into Bio::Align::AlignI.
# The easiest way is to write them to stringstream in fasta format.
my $ss;
open(my $fhSsOut, ">", \$ss) or confess("Cannot open a string stream for writing. $!");
my $ssWriter = Bio::SeqIO->new(-fh => $fhSsOut, -format => 'fasta', -alphabet => 'dna');
for $a (@$accs) {
	#(my $dispId = $a) =~ s/^[^_]+_//;
	# MrBayes does not like dashes
	(my $dispId = $a) =~ s/-/_/g;
	my $accSeq = new Bio::Seq(-display_id => $dispId, -seq => $accSeqs{$a});
	$ssWriter->write_seq($accSeq);
}
$ssWriter.close();


INFO("Printing results");
open(my $fhSsIn, "<", \$ss) or confess("Unable to read from the string stream. $!");
my $ssReader = new Bio::AlignIO(-fh => $fhSsIn, -format => 'fasta', -alphabet => 'dna');
open(my $fhOut, ">", $pathOut) or confess("Unable to write to the output file ($pathOut). $!");
my $writer = new Bio::AlignIO(-fh => $fhOut, -format => 'nexus', -alphabet => 'dna');
while (my $aln = $ssReader->next_aln()) {
	$writer->write_aln($aln);
}
$writer.close();

INFO("Done");



__END__


=encoding utf8

=head1 NAME

12_concatGenes.pl - concatenates gene sequences and output an alignment file in Nexus format.

=head1 SYNOPSIS

12_concatGenes.pl -g genes.txt -a alignmentDir -o output.nex

=head1 OPTIONS

=over 8

=item B<-g>

List of genes to process. The file should have a single column without header.

=item B<-a>

Directory where gene alignments are present. Files should have ".fa" or ".fasta" extension.

=item B<-o>

Path to the output file (Nexus format)

=item B<-p>

(Optional) Alignment file name pattern. Used to retrieve gene name. Default: '^([^_.]).+$'

=back

=head1 DESCRIPTION

The script concatenates previously aligned gene sequences into a single alignment file. The output
file uses the Nexus format. The script is similar to 01_align2Bin.pl except it does not select
polymorphic sites and does not convert the data into binary format.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut
