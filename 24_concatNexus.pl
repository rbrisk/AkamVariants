#!/usr/bin/env perl

use strict;
use warnings;
use autodie;

use Bio::AlignIO;
use Bio::Align::Utilities qw(:all);
use Set::Scalar;
use Getopt::Long;
use Log::Log4perl qw(:easy);
use Pod::Usage;

our $delim = "\t";
my $help = 0;
my $man = 0;
my $logLvlStr = "i";
my $alphabet = "dna";
my ($pathA, $pathB, $pathOut);

GetOptions(
		"h|help" => \$help,
		"man"    => \$man,
		"a=s"    => \$pathA,
		"b=s"    => \$pathB,
		"o=s"    => \$pathOut,
		"t=s"    => \$alphabet,
		"l=s"    => \$logLvlStr,
) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;

my %logLvlDict = ("d" => $DEBUG, "i" => $INFO, "w" => $WARN, "e" => $ERROR, "f" => $FATAL);
$logLvlStr = "i" unless exists($logLvlDict{$logLvlStr});
Log::Log4perl->easy_init({level => $logLvlDict{$logLvlStr}, layout => "%d %p %m%n"});

die("First nexus file is required") unless $pathA;
die("Second nexus file is required") unless $pathB;
die("Output path is required") unless $pathOut;


sub readAlignments {
	my ($path, $alphabet) = @_;
	my @alignments;
	open(my $fh, "<", $path);
	my $reader = Bio::AlignIO->new(-fh => $fh, -format => 'nexus', -alphabet => $alphabet);
	while (my $aln = $reader->next_aln()) {
		my (%sequences, @seqNames);
		for my $seq ($aln->each_seq) {
			$sequences{$seq->display_name} = $seq;
			push(@seqNames, $seq->display_name);
		}
		push(@alignments, [ $aln, \@seqNames ]);
	}
	close($fh);
	return \@alignments;
}

sub findIndices {
	my ($seqAll, $seqBoth) = @_;
	my @indices;
	for my $i (0..$#$seqAll) {
		push(@indices, $i + 1) if $seqBoth->contains($seqAll->[$i]);
	}
	return \@indices;
}

my $alignmentsA = readAlignments($pathA, $alphabet);
my $nA = scalar(@$alignmentsA);
DEBUG("There are $nA alignments in file A");
my $alignmentsB = readAlignments($pathB, $alphabet);
my $nB = scalar(@$alignmentsB);
DEBUG("There are $nB alignments in file B");
if ($nA != $nB) {
	LOGCROAK("The input files have different number of alignments ($nA and $nB respectively)");
}

open(my $fhOut, ">", $pathOut);
my $writer = Bio::AlignIO->new(-fh => $fhOut, -format => "nexus", -alphabet => $alphabet);
for my $i (0..$#$alignmentsA) {
	my $sequencesA = $alignmentsA->[$i];
	my $sequencesB = $alignmentsB->[$i];
	my $setA = Set::Scalar->new(@{$sequencesA->[1]});
	my $setB = Set::Scalar->new(@{$sequencesB->[1]});
	my $setBoth = $setA->intersection($setB);
	DEBUG("Alignment $i: Set A " . $setA->size . " Set B " . $setB->size . " Intersect " . $setBoth->size);
	LOGCROAK("The input files do not contain an overlapping set of sequence names") if $setBoth->is_empty;
	my $indicesA = findIndices($sequencesA->[1], $setBoth);
	my $indicesB = findIndices($sequencesB->[1], $setBoth);
	my $concatAln = cat(
			$alignmentsA->[$i][0]->select_noncont(@$indicesA),
			$alignmentsB->[$i][0]->select_noncont(@$indicesB)
		);
	$writer->write_aln($concatAln);
}
$writer->close();
	

INFO("Done");



__END__


=encoding utf8

=head1 NAME

24_concatNexus.pl - concatenates two nexus files

=head1 SYNOPSIS

24_concatNexus.pl -a fileA.nex -b fileB.nex -o output.nex

=head1 OPTIONS

=over 8

=item B<-a>

First nexus file

=item B<-b>

Second nexus file

=item B<-o>

Path to the output nexus file

=item B<-t>

(Optional) Alphabet (dna, rna, or protein). Default: 'dna'

=back

=head1 DESCRIPTION

Concatenates two nexus files. The sequences that are present only in one file but not the other are
dropped. The order of output sequences matches the order in the first file.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut
