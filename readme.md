# Polymorphism analysis in _Arabidopsis kamchatica_

This file describes computational steps of the polymorphism analysis in _A. kamchatica_ and accompanies the following paper:

Paape T, Briskine RV, Halstead-Nussloch G, Lischer HE, Shimizu-Inatsugi R, Hatakeyama M, Tanaka K, Nishiyama T, Sabirov R, Sese J, Shimizu KK. Patterns of polymorphism and selection in the subgenomes of the allopolyploid Arabidopsis kamchatica. Nature communications. 2018 Sep 25;9(1):3909. doi: [10.1038/s41467-018-06108-1](https://doi.org/10.1038/s41467-018-06108-1)




## 0. Environment and input data

Most of the computation analyses were performed on a Debian server (v7 and later v8) with 32 cores and 1 TiB of memory. The server uses SunGridEngine for job management and LMOD for software management. Each step of the pipeline indicates what additional applications and versions are used by providing module loading commands. All scripts reside in the local `src` directory. Logs are generally sent to the local `log` directory. Gaps in step numbering are caused by omission of the analyses that were not reported in the paper.

Unless otherwise stated or inferred, all commands are executed from the working directory using relative paths. Some applications fail to work properly with relative paths. In those cases, we use `$wd` variable to indicate the absolute path to the working directory. For instance, the absolute path to the input data is `$wd/00_input`.

All modules export a variable whose name matches the application name and points to the application prefix, e.g. `seq/picard` module exports `$picard` variable that points to picard installation directory. In addition some modules may export additional variables that point to `lib` and `include` directories within the installation prefix. Those variables have suffixes `_lib` and `_inc` respectively. Finally, applications distributed as jar archives export the variable ending with `_jar` and pointing to the jar file, e.g. `$picard_jar`.



### Short reads

Short reads have been deposited to the DNA Data Bank of Japan (DDBJ) and received the following accession numbers:

- DRX093435 – DRX093457

In addition, we created a synthetic set by subsampling the 200 bp insert libraries used to assemble _A. halleri_ and _A. lyrata_ genomes. We randomly select 10 mln reads from each library and combined them. The obtained data set will be referred to as `Asyn`.

The reads were sorted into _A. halleri_ origin reads (`kam*_Ahal_orig.bam`), _A. lyrata_ origin reads (`kam*_Alyr_orig.bam`), and common reads (`kam*_common.bam`) using HomeoRoq pipeline modified for DNA data. Here, the analysis starts with the processing of those `bam` files, which had been placed into the `00_input` directory.

All genotype names including `Asyn` were saved to `00_input/genotypes.txt` file (one entry per line without header). Genotype names matched the identifiers used in bam files.


### Reference genomes and annotation

_A. halleri_ reference genome and annotation is from [Briskine et al. 2016](http://dx.doi.org/10.1111/1755-0998.12604). The genome of _A. lyrata_ ssp. _petraea_ was assembled and annotation as part of this project using the same pipeline as for _A. halleri_. The genome was deposited to EBI and received accession number `ERZ473538`. The annotation was deposited to DryAd and received accession number XXXXXXXX.

Genome assemblies (`Ahal.fa` and `Alyr.fa` from EBI), annotations (`Ahal.gff` and `Alyr.gff` from DryAd), and reciprocal best blast hits (RBH) with _A. thaliana_ (`Ahal_on_Atha.rbh` and `Alyr_on_Atha.rbh` from DryAd) were placed in the `00_input` directory. The assemblies were indexed for `bwa` alignment.

```shell
bwa index Ahal.fa
bwa index Alyr.fa
```


### _Arabidopsis thaliana_ genomic resources

TAIR10 genomic sequences and gene annotations were downloaded from the [TAIR FTP site](ftp://ftp.arabidopsis.org/home/tair/Genes/TAIR10_genome_release). Chromosome display names were updated to use shorter format. CDS were extracted from the genome based on the gene annotation.

```shell
tairFtp=ftp://ftp.arabidopsis.org/home/tair/Genes/TAIR10_genome_release
curl -L $tairFtp/TAIR10_gff3/TAIR10_GFF3_genes.gff | xz - > 00_input/Atha.gff.xz
curl -L $tairFtp/TAIR10_chromosome_files/TAIR10_chr_all.fas | xz - > 00_input/Atha_chr_all.fas.xz
xzcat 00_input/Atha_chr_all.fas.xz | sed -r 's/^>(.).+/>Chr\U\1/' | xz - > Atha.fa.xz
src/extractCds.pl -f 00_input/Atha.fa.xz -g 00_input/Atha.gff.xz -l -o 00_input/Atha_cds.fa.xz
```


### _Arabidopsis lyrata_ ssp. _lyrata_ genomic resources

_Arabidopsis lyrata_ ssp. _lyrata_ v1.0 genomic sequences, gene annotation, and coding sequences were downloaded from the phytozome website on 2012-12-13. Henceforth, this genome assembly will be referred to as **AlyrJGI**.

* `AlyrJGI.fa` := assembly
* `AlyrMPGI.gff.gz` := gene annotation
* `AlyrMPGI_cds.fa.gz` := coding sequences

Rawat et al. released the improved [v2.0 annotation](http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0137391) for AlyrJGI assembly. The GFF file was downloaded from the [PLOS ONE website](http://journals.plos.org/plosone/article/asset?unique&id=info:doi/10.1371/journal.pone.0137391.s002) on 2016-07-08 and saved as `AlyrMPGI_orig.gff.gz`. The annotation was amended to make sure all features have unique IDs. Coding sequences were extracted and saved in `AlyrMPGI_cds.fa.xz`. To distinguish this annotation from the JGI v1.0 annotation, we will refer to it as **AlyrMPGI**.

```shell
src/fixGffMpgi.pl -i 00_input/AlyrMPGI_orig.gff.xz -m | xz > 00_input/AlyrMPGI.gff.xz
src/extractCds.pl -f 00_input/AlyrJGI.fa -g 00_input/AlyrMPGI.gff.xz -l -o 00_input/AlyrMPGI_cds.fa.xz
```



## 1. Extract reads

```shell
module load seq/picard/1.130
src/01_extractReads 00_input/genotypes.txt 00_input 01_reads
```



## 2. Align reads

```shell
module load seq/bwa/0.7.2 seq/samtools/1.2
nice src/02_align 00_input/genotypes.txt 01_reads 02_align >& log/02_align &
```

Count the number of aligned reads

```shell
module load seq/bamtools/2.3.0
bash -c 'for i in `cat 06_joint/genotypes.txt`; do echo $i `bamtools filter -in 02_align/kam${i}_Ahal.bam -isPrimaryAlignment true -isMapped true | bamtools count`; done' > 02_align/CountsAhal.txt
bash -c 'for i in `cat 06_joint/genotypes.txt`; do echo $i `bamtools filter -in 02_align/kam${i}_Alyr.bam -isPrimaryAlignment true -isMapped true | bamtools count`; done' > 02_align/CountsAlyr.txt
```



## 3. Calculate coverage

```shell
mkdir 03_cov
```

Index the FASTA files and build dictionaries.

```shell
module load seq/samtools/1.2 seq/picard/1.130
samtools faidx 00_input/Ahal.fa
samtools faidx 00_input/Alyr.fa

java -jar $picard_jar CreateSequenceDictionary R=00_input/Ahal.fa O=00_input/Ahal.dict
java -jar $picard_jar CreateSequenceDictionary R=00_input/Alyr.fa O=00_input/Alyr.dict
```

Index the bam files.

```shell
for i in 02_align/*.bam; do echo $i; samtools index $i; done
````

Create the file list. The file must have `.list` extension.

```shell
for i in 02_align/*Ahal.bam; do echo $i >> 03_cov/Ahal.list; done
for i in 02_align/*Alyr.bam; do echo $i >> 03_cov/Alyr.list; done
```

Estimate the coverage.

```shell
module load java/jdk/7/79 seq/gatk/3.4-0
nice java -Xmx32g -jar $gatk_jar -R 00_input/Ahal.fa -T DepthOfCoverage --omitDepthOutputAtEachBase --omitIntervalStatistics --omitLocusTable -o 03_cov/Ahal -I 03_cov/Ahal.list >& log/03_cov_Ahal &
nice java -Xmx32g -jar $gatk_jar -R 00_input/Alyr.fa -T DepthOfCoverage --omitDepthOutputAtEachBase --omitIntervalStatistics --omitLocusTable -o 03_cov/Alyr -I 03_cov/Alyr.list >& log/03_cov_Alyr &
```



## 4. Call variants

While we did not use the variants from this step (we reran HaplotypeCaller using joint genotyping), we re-used the final bam files produced in this step to take advantage of the already performed mate fixing and duplicate marking. In addition, we will be using `Asyn` variants later for filtering and we did not want to include that synthetic data for joint genotyping to avoid any potential effects on other samples.

```
module load java/jdk/7/79 seq/queue/3.4-0
nice src/04_runCall Ahal 00_input/genotypes.txt 8 >& log/04_runCall_Ahal &
nice src/04_runCall Alyr 00_input/genotypes.txt 8 >& log/04_runCall_Alyr &
```



## 5. Realign around indels

*Warning*: PCR and optical duplicates should have been removed before performing the realignment!

```shell
module load java/jdk/7/79 seq/gatk/3.4-0
nice src/05_realign Ahal 00_input/genotypes.txt 16 >& log/05_realignAhal &
nice src/05_realign Alyr 00_input/genotypes.txt 16 >& log/05_realignAlyr &
```

To determine the threshold for the high coverage filter, we estimated coverage for each base in each genotype using bamtools (it yields values that are very close to GATK DepthOfCoverage but the computation is much faster). We assumed the Poisson distribution and calculated the combined and per-line thresholds as `mean + 5 * std`.

```shell
module load seq/bamtools/2.3.0
nice src/05_jointCoverage Ahal 06_joint/genotypes.txt &> 05_jointCoverageAhal &
nice src/05_jointCoverage Alyr 06_joint/genotypes.txt &> 05_jointCoverageAlyr &
```

Ahal has information about 195,327,068 positions out of 196,243,198 or 99.53%. Alyr has information on 174,376,617 positions. The rest are not reported because bamtools ignores zero coverage at the beginning and the end of a scaffold. After adding the missing number of 0's, the following statistics were obtained.

|      | Size, bp    | Mean   | Std     | Median | MAD    |
|:-----|------------:|-------:|--------:|-------:|-------:|
| Ahal | 196,243,198 | 481.86 | 2628.73 |    483 | 296.52 |
| Alyr | 175,182,717 | 418.16 | 2226.36 |    398 | 383.99 |

The thresholds assuming the Normal distribution are also provided for comparison (`median + 5 * MAD`).

|      | Normal | Per Line | Poisson | Per Line |
|:-----|-------:|---------:|--------:|---------:|
| Ahal |   1966 |     75.6 |    2891 |    111.2 |
| Alyr |   2318 |     89.2 |    2509 |     96.5 |



## 6. Joint variant calling

Call variants jointly. Note that we excluded `Asyn` from the joint run to avoid any potential effects of the synthetic data set on other samples.

```shell
module load java/jdk/7/79 seq/queue/3.4-0
mkdir 06_joint
grep -v Asyn 00_input/genotypes.txt > 06_joint/genotypes.txt
nice src/06_runJointCall Ahal 06_joint/genotypes.txt 2891 8 16 &> log/06_jointAhal &
nice src/06_runJointCall Alyr 06_joint/genotypes.txt 2318 8 16 &> log/06_jointAlyr &
```



## 7. Mask regions with abnormal coverage

We will mask regions with coverage below 2 and above 250 counting only the reads with MQ >= 20.

```shell
module load seq/bamtools/2.3.0
nice src/07_maskAbnormalCov Ahal 06_joint/genotypes.txt 4 &> log/07_maskCovAhal &
nice src/07_maskAbnormalCov Alyr 06_joint/genotypes.txt 4 &> log/07_maskCovAlyr &
```


## 8 through 18

Omitted



## 19. Filter parental variants

We will refer to the variants called in step 4 for the `Asyn` data set as parental variants. These are not the actual variants because they are based on reads that come from the libraries used to assemble the reference genomes. These variants were potentially caused by either incorrect read sorting or repetitive regions with higher copy number in the genome compared to the assembly. It is important to note that these variants are likely to appear in _A. kamchatica_ lines if the corresponding regions are somewhat conserved. Since all these variants would also be false positives in _A. kamchatica_, we can use the parental variants for filtering.

VariantFiltration masks all variants regardless of the filter status. Therefore, we have to remove filtered variants before applying the mask. In addition, the function filters out SNPs that are spanned by deletions from the mask data set. This is not ideal because with coverage it would look like the reference even though we would have three possible states: deletion, no deletion, and SNP. So, if we consider that the deletion is not real, the SNP should be real then. To address this problem, we will split both the mask and the variants into SNPs and non-SNPs. Then we will use the corresponding mask to filter the subset and merge both filtered subsets together. The approach would still remove non-SNP variants spanned by deletions but at least the SNPs will remain.

```shell
module load java/jdk/7/79 seq/gatk/3.4-0
mkdir -p 19_parentalF/{Ahal,Alyr}
nice src/19_applyParentalMask -i 06_joint/Ahal/filtered.vcf.gz -m 04_variants/Ahal/Asyn/filtered.vcf.gz -r 00_input/Ahal.fa -o 19_parentalF/Ahal/filteredAsyn.vcf.gz &> log/19_applyParentalMask_AhalAsyn &
nice src/19_applyParentalMask -i 06_joint/Alyr/filtered.vcf.gz -m 04_variants/Alyr/Asyn/filtered.vcf.gz -r 00_input/Alyr.fa -o 19_parentalF/Alyr/filteredAsyn.vcf.gz &> log/19_applyParentalMask_AlyrAsyn &
```

The numbers do not add up because some variants have previously failed quality filters.

| Species | Variants  | Marked Asyn | Pass Asyn |
|:--------|----------:|------------:|----------:|
| Ahal    | 3,618,864 |      59,856 | 3,565,857 |
| Alyr    | 3,576,475 |      58,645 | 3,523,187 |


### Count variants per genotype

The script is inefficient because it parses the data set once for each genotype. However, it saves us from writing a special script as we just use the standard VariantFiltration here. It takes about 6-8 minutes per genotype. The script counts only homozygous called SNPs.

```shell
module load java/jdk/7/79 seq/gatk/3.4-0

nice src/19_countGenotypeSnps 19_parentalF/Ahal/filteredAsyn.vcf.gz 00_input/Ahal.fa 06_joint/genotypes.txt > 19_parentalF/Ahal/SnpCounts.txt 2>19_parentalF/Ahal/SnpCounts.log &

nice src/19_countGenotypeSnps 19_parentalF/Alyr/filteredAsyn.vcf.gz 00_input/Alyr.fa 06_joint/genotypes.txt > 19_parentalF/Alyr/SnpCounts.txt 2>19_parentalF/Alyr/SnpCounts.log &
```



## 20. Apply SNPs and deletions


```shell
mkdir 20_applyVariantsF
module load java/jdk/7/79 seq/gatk/3.4-0 seq/picard/1.130 seq/samtools/1.2

nice src/08_applyVariants -v 19_parentalF/Ahal/filteredAsyn.vcf.gz -i 07_maskCov -o 20_applyVariantsF -p Ahal -g 06_joint/genotypes.txt -t 8 -a &> log/20_applyVariantsF_Ahal &

nice src/08_applyVariants -v 19_parentalF/Alyr/filteredAsyn.vcf.gz -i 07_maskCov -o 20_applyVariantsF -p Alyr -g 06_joint/genotypes.txt -t 8 -a &> log/20_applyVariantsF_Alyr &
```



## 21. Extract CDS

Stop codons should be included because some alleles have one or more Ns at the end and we cannot tell easily whether a stop codon present or not.

```shell
mkdir 21_cdsF

nice src/09_extractCds -p Ahal -i 20_applyVariantsF -a 00_input -o 21_cdsF -g 06_joint/genotypes.txt &> log/21_extractCds_Ahal &
nice src/09_extractCds -p Alyr -i 20_applyVariantsF -a 00_input -o 21_cdsF -g 06_joint/genotypes.txt &> log/21_extractCds_Alyr &
```



## 22. Make CDS alignments

For each _A. thaliana_ RBH, cut CDS from each genotype and append the reference sequence (_A. halleri_ or _A. lyrata_) at the end.

```shell
src/10_mkAlignments.pl -p Ahal --dirCds=21_cdsF -g 06_joint/genotypes.txt -r 00_input/Ahal_on_Atha.rbh -o 22_alignedF
src/10_mkAlignments.pl -p Alyr --dirCds=21_cdsF -g 06_joint/genotypes.txt -r 00_input/Alyr_on_Atha.rbh -o 22_alignedF
```

This is redundant but it would allow us to analyse the alignments for HMA4 copies and other genes that do not have homologs.

```shell
src/10_mkAlignments.pl -p Ahal --dirCds=21_cdsF -g 06_joint/genotypes.txt -o 22_alignedF/AllGenes
```

Count the missing data percentage for each genotype within each gene alignment.

```shell
src/10_countNs.pl -a 22_alignedF/Ahal -o 22_alignedF/MissingDataAhal.txt
src/10_countNs.pl -a 22_alignedF/Alyr -o 22_alignedF/MissingDataAlyr.txt
```



## 23. Align outgroup

Align _A. thaliana_ sequences as outgroup. The alignment of all _A. kamchatica_ sequences and the reference is preserved.

Originally, we removed the reference sequence before the alignment. However, some genes have very poor coverage in Akam, leading to suboptimal results. Since the reference sequence has virtually no missing nucleotides, it would be better to remove the reference after the alignment.

```shell
module load bio/muscle/3.8.31
src/alignOutgroup.pl --ing 22_alignedF/Ahal --outg 00_input/Atha_cds.fa.xz -o 23_outgroupF/Ahal --outgLbl Atha --rmRefSeqId=-2
src/alignOutgroup.pl --ing 22_alignedF/Alyr --outg 00_input/Atha_cds.fa.xz -o 23_outgroupF/Alyr --outgLbl Atha --rmRefSeqId=-2
```



## 24. Phylogenetic trees

Generate phylogenetic trees based on the alignment of randomly selected genes from each _A. halleri_ and _A. lyrata_. In addition, we combined _A. halleri_ and _A. lyrata_ genes in an AhalAlyr data set.

### Generate nexus files

```shell
mkdir -p 24_phylogenyF/{Ahal,Alyr,AhalAlyr}
src/12_concatGenes.pl -g src/24_genes_random_Ahal.txt -a 22_alignedF/Ahal -o 24_phylogenyF/Ahal.nex
src/12_concatGenes.pl -g src/24_genes_random_Alyr.txt -a 22_alignedF/Alyr -o 24_phylogenyF/Alyr.nex
src/24_concatNexus.pl -a 24_phylogenyF/Ahal.nex -b 24_phylogenyF/Alyr.nex -o 24_phylogenyF/AhalAlyr.nex -l d
```

### Run MrBayes

A preliminary test showed that beagle runs a bit faster than the default implementation. We will use the default 2 runs and 4 chains meaning that we can use 8 threads. By default, MrBayes uses the first accession as the outgroup. We will run it with the default outgroup to determine the placement of Ahal and Alyr relative to Akam accessions. Then we will set the outgroup to Ahal or Alyr and run the analysis again. Such outgroup setting may improve the trees.

If two analysis are run within the same batch file without re-executing, MrBayes complains about negative gamma parameter. Therefore, the analyses with outgroup and without outgroup were separated into two individual batch files.

```shell
module load bio/mrbayes/3.2.6 dev/openmpi/1.8.5
nice mpirun -np 8 pmb src/24_mb/Ahal.mb &> log/24_Ahal &
nice mpirun -np 8 pmb src/24_mb/Alyr.mb &> log/24_Alyr &
nice mpirun -np 8 pmb src/24_mb/AhalAlyr.mb &> log/24_AhalAlyr &

nice mpirun -np 8 pmb src/24_mb/Ahal_woOutgroup.mb &> log/24_Ahal_woOutgroup &
nice mpirun -np 8 pmb src/24_mb/Alyr_woOutgroup.mb &> log/24_Alyr_woOutgroup &
```



## 25. Convert alignments to binary

Binary alignments are necessary to run Structure.

```shell
mkdir -p 25_binaryF
src/align2Bin.pl -genes src/24_genes_random_Ahal.txt -aln 22_alignedF/Ahal -o 25_binaryF/Ahal.txt --keepMiss
src/align2Bin.pl -genes src/24_genes_random_Alyr.txt -aln 22_alignedF/Alyr -o 25_binaryF/Alyr.txt --keepMiss
src/align2Bin.pl -genes src/24_genes_random_Ahal.txt -aln 22_alignedF/Ahal -o 25_binaryF/AhalWithRef.txt --keepMiss --keepRef
src/align2Bin.pl -genes src/24_genes_random_Alyr.txt -aln 22_alignedF/Alyr -o 25_binaryF/AlyrWithRef.txt --keepMiss --keepRef
```

| Parent | Markers | With Ref |
|:-------|--------:|---------:|
| Ahal   |  21,341 |   22,896 |
| Alyr   |  16,223 |   23,637 |


Combine _A. halleri_ and _A. lyrata_ data. We are not combining the files that contain references because we would have two references then.

```shell
wd=25_binaryF
paste -d "\t" $wd/Ahal.txt <(cut -f 2- $wd/Alyr.txt) > $wd/AhalAlyr.txt
```



## 26. Structure analysis

### Structure

Run structure using `K=1..9` for `10` iterations each.

```shell
mkdir -p 26_structF
module load bio/structure/2.3.4
mkdir log/S26_{Ahal,Alyr,AhalAlyr,AhalWithRef,AlyrWithRef}

qsub -q GT -l h=fgcz-c-047 -cwd -t 1-90 -v kMax=9 -v pathParaMain=src/26_struct/mainparams -v pathParaExtra=src/26_struct/extraparams -v pathData=25_binaryF/Ahal.txt -v dirOut=26_structF/Ahal -N S26_Ahal src/14_jobs/runStructure
qsub -q GT -l h=fgcz-c-047 -cwd -t 1-90 -v kMax=9 -v pathParaMain=src/26_struct/mainparams -v pathParaExtra=src/26_struct/extraparams -v pathData=25_binaryF/Alyr.txt -v dirOut=26_structF/Alyr -N S26_Alyr src/14_jobs/runStructure

qsub -q GT -l h=fgcz-c-047 -cwd -t 1-90 -v kMax=9 -v pathParaMain=src/26_struct/mainparams -v pathParaExtra=src/26_struct/extraparams -v pathData=25_binaryF/AhalWithRef.txt -v dirOut=26_structF/AhalWithRef -N S26_AhalWithRef src/14_jobs/runStructure
qsub -q GT -l h=fgcz-c-047 -cwd -t 1-90 -v kMax=9 -v pathParaMain=src/26_struct/mainparams -v pathParaExtra=src/26_struct/extraparams -v pathData=25_binaryF/AlyrWithRef.txt -v dirOut=26_structF/AlyrWithRef -N S26_AlyrWithRef src/14_jobs/runStructure

qsub -q GT -l h=fgcz-c-047 -cwd -t 1-90 -v kMax=9 -v pathParaMain=26_structF/mainparams -v pathParaExtra=26_structF/extraparams -v pathData=25_binaryF/Asyn/AhalAlyr.txt -v dirOut=26_structF/Asyn/AhalAlyr -N S26_Asyn_AhalAlyr src/14_jobs/runStructure
```

### Harvester

Structure Harvester calculates the likelihood of each K as well as variability across the iterations for each K.

```shell
module load bio/harvester/0.6.94
pyenv shell 2.7.12
for t in Ahal Alyr AhalWithRef AlyrWithRef AhalAlyr; do
	echo $t
	structureHarvester.py --dir=26_structF/$t --out=26_structF/${t}Harvest --evanno --clumpp
	src/26_graphEvanno.R -i 26_structF/${t}Harvest/evanno.txt -o graphs/26_structF/$t
done
```

The optimal K is 4 for _A. halleri_ and 3 for _A. lyrata_. For the combined data set AhalAlyr, the optimal K is 2.

### CLUMPP

CLUMPP re-arranges the clusters within each run to have comparable membership ratios. However, it does not accept raw output from Structure. Q matrices have to be extracted and placed into a single file. In addition, CLUMPP expects the output to have population id column and population information absent from the output. Finally, the accession names also have to be replaced with ids.

```shell
module load bio/clumpp/1.1.2
nice src/14_runClumpp 26_structF 26_structF/clumpp Ahal,Alyr,AhalAlyr,AhalWithRef,AlyrWithRef 9 >& log/26_runClumpp &
cat 06_joint/genotypes.txt > 26_structF/genotypes.txt
cp 26_structF/genotypes{,Ahal}.txt && echo "Ahal" >> 26_structF/genotypesAhal.txt
cp 26_structF/genotypes{,Alyr}.txt && echo "Alyr" >> 26_structF/genotypesAlyr.txt
module purge
src/14_makeGraphs 26_structF Ahal,Alyr,AhalAlyr pdf
src/14_makeGraphs 26_structF Ahal,Alyr pdf WithRef
```



## 27. Selected scaffolds

Produce alignments for selected scaffolds includining those that contain MTP1, HMA3, HMA4, MTP3, and FRD3 regions. The regions are the same for both _A. halleri_ and _A. lyrata_; the scaffolds ids are obviously different.

```shell
mkdir 27_lociF
src/27_extractLoci Ahal 06_joint/genotypes.txt
src/extractLoci.pl --asmb 00_input/Ahal.fa --loci src/15_lociAhal.txt -o 27_lociF/Ahal/Ahal.fa.xz
src/27_extractLoci Alyr 06_joint/genotypes.txt
src/extractLoci.pl --asmb 00_input/Alyr.fa --loci src/15_lociAlyr.txt -o 27_lociF/Alyr/Alyr.fa.xz
```



## 28. Align selected scaffolds

Align selected scaffolds. Include the reference in the alignments.

```shell
src/alignLoci.pl --par Ahal --gt 06_joint/genotypes.txt --data 27_lociF -o 28_lociAlignedF
src/alignLoci.pl --par Alyr --gt 06_joint/genotypes.txt --data 27_lociF -o 28_lociAlignedF
```



## 29. Extract genes

Extract gene sequences (UTR + CDS + intron) from each genotype. We will use the coordinates reported in the gene records of the GFF file.

```shell
mkdir 29_genesF
src/17_mkGeneList.pl -i 00_input/Ahal.gff -o 29_genesF/GenesAhal.txt
src/29_extractGenes Ahal 06_joint/genotypes.txt
src/extractLoci.pl --asmb 00_input/Ahal.fa --loci 29_genesF/GenesAhal.txt -o 29_genesF/Ahal/Ahal.fa.xz --desc

src/17_mkGeneList.pl -i 00_input/Alyr.gff -o 29_genesF/GenesAlyr.txt
src/29_extractGenes Alyr 06_joint/genotypes.txt
src/extractLoci.pl --asmb 00_input/Alyr.fa --loci 29_genesF/GenesAlyr.txt -o 29_genesF/Alyr/Alyr.fa.xz --desc
```



## 30. Align genes

Align genes for each parent. Include the reference in the alignment.

```shell
src/alignLoci.pl --par Ahal --gt 06_joint/genotypes.txt --data 29_genesF -o 30_genesAlignedF
src/alignLoci.pl --par Alyr --gt 06_joint/genotypes.txt --data 29_genesF -o 30_genesAlignedF
```

Align the parental groups without the references.

```shell
module load bio/muscle/3.8.31
src/18_alignParentGroups.pl --ortho 00_input/Ahal_on_Alyr.rbh --data 30_genesAlignedF --pref AhalAlyr -a Ahal -b Alyr --atha 00_input/Ahal_on_Atha.rbh
```

Count the missing data percentage for each genotype within each gene alignment.

```shell
src/10_countNs.pl -a 30_genesAlignedF/Ahal -o 30_genesAlignedF/MissingDataAhal.txt
src/10_countNs.pl -a 30_genesAlignedF/Alyr -o 30_genesAlignedF/MissingDataAlyr.txt
```



## 31. Count missing bases

Count missing bases in each genomic context. I hoped to re-use the existing CDS and gene alignments but CDS alignments contain only Atha orthologs and gene alignments include UTRs. So, instead, I will extract separately introns and CDS. Then `UTR = gene - (CDS + intron)` and `intergenic = Total - gene`. This might be easier than calculating for each context directly.

```shell
src/31_countNs.pl --parent Ahal -gt 06_joint/genotypes.txt --annot 00_input/Ahal.gff --data 20_applyVariantsF/Ahal -o 31_countN/Ahal.txt -v d
src/31_countNs.pl --parent Alyr -gt 06_joint/genotypes.txt --annot 00_input/Alyr.gff --data 20_applyVariantsF/Alyr -o 31_countN/Alyr.txt -v d
```

The counts are also needed for the reference genomes rather than for resequenced genomes. I will just copy them in the input directories, so that I could re-use the `31_countNs.pl` script.

```shell
cat 00_input/Ahal.fa | xz > 20_applyVariantsF/Ahal/kamAhal_Ahal.fa.xz
cat 00_input/Alyr.fa | xz > 20_applyVariantsF/Alyr/kamAlyr_Alyr.fa.xz
src/31_countNs.pl --parent Ahal -gt <(echo "Ahal") --annot 00_input/Ahal.gff --data 20_applyVariantsF/Ahal -o 31_countN/AhalRef.txt -v d
src/31_countNs.pl --parent Alyr -gt <(echo "Alyr") --annot 00_input/Alyr.gff --data 20_applyVariantsF/Alyr -o 31_countN/AlyrRef.txt -v d
```

Count Ns specifically in HMA4 and MTP3 regions.

| Species | Region | Scaffold     | Start   | Stop    | GeneStart | GeneStop |
|:--------|:-------|:-------------|--------:|--------:|:----------|:---------|
| Ahal    | HMA4   | scaffold_116 |  182627 |  487662 | g22684    | g22716   |
| Alyr    | HMA4   | scaffold_52  |  828766 |  984300 | g20882    | g20903   |
| Ahal    | MTP3   | scaffold_55  |   30286 |  134671 | g16486    | g16516   |
| Alyr    | MTP3   | scaffold_1   | 2273167 | 2359702 | g00508    | g00532   |

```shell
mkdir 31_countN/{HMA4,MTP1}
src/extractLoci.pl --asmb 00_input/Ahal.fa --loci 31_countN/HMA4_Ahal.loci -o 31_countN/HMA4_Ahal.fa
src/extractLoci.pl --asmb 00_input/Alyr.fa --loci 31_countN/HMA4_Alyr.loci -o 31_countN/HMA4_Alyr.fa
src/extractLoci.pl --asmb 00_input/Ahal.fa --loci 31_countN/MTP3_Ahal.loci -o 31_countN/MTP3_Ahal.fa
src/extractLoci.pl --asmb 00_input/Alyr.fa --loci 31_countN/MTP3_Alyr.loci -o 31_countN/MTP3_Alyr.fa

sed -rn '/g22684/,/g22717/ p' 00_input/Ahal.gff | sed '$ d' | perl -nale '$F[0] = "HMA4"; $F[3] -= 182626; $F[4] -= 182626; print join("\t", @F);' > 31_countN/Loci_Ahal.gff
sed -rn '/g20882/,/g20903/ p' 00_input/Alyr.gff | sed '$ d' | perl -nale '$F[0] = "HMA4"; $F[3] -= 828765; $F[4] -= 828765; print join("\t", @F);' > 31_countN/Loci_Alyr.gff
sed -rn '/g16486/,/g16516/ p' 00_input/Ahal.gff | sed '$ d' | perl -nale '$F[0] = "MTP3"; $F[3] -= 30285; $F[4] -= 30285; print join("\t", @F);' >> 31_countN/Loci_Ahal.gff
sed -rn '/g00508/,/g00532/ p' 00_input/Alyr.gff | sed '$ d' | perl -nale '$F[0] = "MTP3"; $F[3] -= 2273166; $F[4] -= 2273166; print join("\t", @F);' >> 31_countN/Loci_Alyr.gff

src/31_countNs.pl --fnptrn '%1$s_%2$s.fa' --parent Ahal -gt <(echo -e "HMA4\nMTP3") --annot 31_countN/Loci_Ahal.gff --data 31_countN -o 31_countN/Loci_Ahal.txt -v d
src/31_countNs.pl --fnptrn '%1$s_%2$s.fa' --parent Alyr -gt <(echo -e "HMA4\nMTP3") --annot 31_countN/Loci_Alyr.gff --data 31_countN -o 31_countN/Loci_Alyr.txt -v d
```



## 32. Promoter and enhancer alignments

Alignments of the flanking regions. We will take 500 bp on each side of genes. Only flanking regions that do not overlap with a CDS of another gene will be included. Genes that span other genes or are spanned by other genes are excluded as well. The output depends on strand, i.e. regions are separated into 5' and 3' files.

Find distances between neighbouring genes.

```shell
perl -I src/lib src/32_printGeneDist.pl --gff 00_input/Ahal.gff --fa 00_input/Ahal.fa -o 32_flanks/GeneDistAhal.txt --type mRNA
perl -I src/lib src/32_printGeneDist.pl --gff 00_input/Alyr.gff --fa 00_input/Alyr.fa -o 32_flanks/GeneDistAlyr.txt --type mRNA
```

```shell
mkdir 32_flanks
perl -I src/lib src/32_makeFlankSeqList.pl --gff 00_input/Ahal.gff --fa 00_input/Ahal.fa -5 32_flanks/List5pAhal.txt -3 32_flanks/List3pAhal.txt --width 500 --type mRNA
perl -I src/lib src/32_makeFlankSeqList.pl --gff 00_input/Alyr.gff --fa 00_input/Alyr.fa -5 32_flanks/List5pAlyr.txt -3 32_flanks/List3pAlyr.txt --width 500 --type mRNA

src/32_extractLoci Ahal 06_joint/genotypes.txt
src/32_extractLoci Alyr 06_joint/genotypes.txt
src/extractLoci.pl --asmb 00_input/Ahal.fa --loci 32_flanks/List5pAhal.txt -o 32_flanks/5p/Ahal/Ahal.fa.xz
src/extractLoci.pl --asmb 00_input/Ahal.fa --loci 32_flanks/List3pAhal.txt -o 32_flanks/3p/Ahal/Ahal.fa.xz
src/extractLoci.pl --asmb 00_input/Alyr.fa --loci 32_flanks/List5pAlyr.txt -o 32_flanks/5p/Alyr/Alyr.fa.xz
src/extractLoci.pl --asmb 00_input/Alyr.fa --loci 32_flanks/List3pAlyr.txt -o 32_flanks/3p/Alyr/Alyr.fa.xz

src/alignLoci.pl --par Ahal --gt 06_joint/genotypes.txt --data 32_flanks/5p -o 32_flanks/aligned5p
src/alignLoci.pl --par Ahal --gt 06_joint/genotypes.txt --data 32_flanks/3p -o 32_flanks/aligned3p
src/alignLoci.pl --par Alyr --gt 06_joint/genotypes.txt --data 32_flanks/5p -o 32_flanks/aligned5p
src/alignLoci.pl --par Alyr --gt 06_joint/genotypes.txt --data 32_flanks/3p -o 32_flanks/aligned3p
```

There were 16,194 and 14,709 alignments for _A. halleri_ and _A. lyrata_ respectively.

At this point we need to align the outgroup, which would be _A. halleri_ for _A. lyrata_ and _A. lyrata_ for _A. halleri_ mapped through RBH to _A. thaliana_. We cannot reuse `alignOutgroup.pl` because the aligned files do not have corresponding _A. thaliana_ RBH in their names. Besides, the desired output name should contain ingroup gene name, outgroup gene name, and _A. thaliana_ RBH. So, the best way would be to create a special version of `alignOutgroup.pl` just for this purpose.

```shell
module load bio/muscle/3.8.31
src/32_alignOutgroup.pl --ing 32_flanks/aligned5p/Ahal --outg 32_flanks/5p/Alyr/Alyr.fa.xz --rbhoutg 00_input/Alyr_on_Atha.rbh --rbhing 00_input/Ahal_on_Atha.rbh -o 32_flanks/outgroup5p/Ahal --outgLbl Alyr --rmRefSeqId -1
src/32_alignOutgroup.pl --ing 32_flanks/aligned3p/Ahal --outg 32_flanks/3p/Alyr/Alyr.fa.xz --rbhoutg 00_input/Alyr_on_Atha.rbh --rbhing 00_input/Ahal_on_Atha.rbh -o 32_flanks/outgroup3p/Ahal --outgLbl Alyr --rmRefSeqId -1

src/32_alignOutgroup.pl --ing 32_flanks/aligned5p/Alyr --outg 32_flanks/5p/Ahal/Ahal.fa.xz --rbhoutg 00_input/Ahal_on_Atha.rbh --rbhing 00_input/Alyr_on_Atha.rbh -o 32_flanks/outgroup5p/Alyr --outgLbl Ahal --rmRefSeqId -1
src/32_alignOutgroup.pl --ing 32_flanks/aligned3p/Alyr --outg 32_flanks/3p/Ahal/Ahal.fa.xz --rbhoutg 00_input/Ahal_on_Atha.rbh --rbhing 00_input/Alyr_on_Atha.rbh -o 32_flanks/outgroup3p/Alyr --outgLbl Ahal --rmRefSeqId -1
```

The outgroup alignments have flanking sequences for 7238 genes.

Finally, we extract the files that correspond to the candidate genes.

```shell
mkdir -p 32_flanks/candidates/outgroup{5,3}p/{Ahal,Alyr}
for i in `sed -r -e 's/\t/_/g' -e 's/$/.fa/' src/32_candidates/listAhal.txt`; do path=32_flanks/outgroup5p/Ahal/$i; if [[ -f $path ]]; then cp -a $path 32_flanks/candidates/outgroup5p/Ahal; fi; done
for i in `sed -r -e 's/\t/_/g' -e 's/$/.fa/' src/32_candidates/listAhal.txt`; do path=32_flanks/outgroup3p/Ahal/$i; if [[ -f $path ]]; then cp -a $path 32_flanks/candidates/outgroup3p/Ahal; fi; done

for i in `sed -r -e 's/\t/_/g' -e 's/$/.fa/' src/32_candidates/listAlyr.txt`; do path=32_flanks/outgroup5p/Alyr/$i; if [[ -f $path ]]; then cp -a $path 32_flanks/candidates/outgroup5p/Alyr; fi; done
for i in `sed -r -e 's/\t/_/g' -e 's/$/.fa/' src/32_candidates/listAlyr.txt`; do path=32_flanks/outgroup3p/Alyr/$i; if [[ -f $path ]]; then cp -a $path 32_flanks/candidates/outgroup3p/Alyr; fi; done
```



## 33. CDS with the opposite reference

Probably the easiest way is to strip the _A. thaliana_ gene name from CDS alignments and process those files with `32_alignOutgroup.pl`.

```shell
module load bio/muscle/3.8.31
mkdir -p 33_cdsOppRef/{Ahal,Alyr,AhalTmp,AlyrTmp}
for i in 22_alignedF/Ahal/*.fa; do cp -a $i 33_cdsOppRef/AhalTmp/${i##*_}; done
for i in 22_alignedF/Alyr/*.fa; do cp -a $i 33_cdsOppRef/AlyrTmp/${i##*_}; done

src/32_alignOutgroup.pl --ing 33_cdsOppRef/AhalTmp --outg 00_input/Alyr_cds.fa --rbhoutg 00_input/Alyr_on_Atha.rbh --rbhing 00_input/Ahal_on_Atha.rbh -o 33_cdsOppRef/Ahal --outgLbl Alyr --rmRefSeqId -1
src/32_alignOutgroup.pl --ing 33_cdsOppRef/AlyrTmp --outg 00_input/Ahal_cds.fa --rbhoutg 00_input/Ahal_on_Atha.rbh --rbhing 00_input/Alyr_on_Atha.rbh -o 33_cdsOppRef/Alyr --outgLbl Ahal --rmRefSeqId -1
```



## 34. Variant annotation and effect prediction

SnpEff assumes that the installation is central and the only one. So, the configuration file is in the installation directory and you are running it from the installation directory. By default, the data directory is in the installation directory as well. This is not desirable because we want to associate the data with each project in order to have a complete control over versions. Fortunately, they provide a flag to specify the configuration file. Since I am keeping the config file under the version control, I have removed all redundant entries. I assume that we will only use it for processing _A. halleri_ and _A. lyrata_ data.

### Add genome data

```
mkdir -p 34_snpeff/data
module load bio/snpeff/4.2
mkdir -p 34_snpeff/data/{Ahal,Alyr}2.2
ln -s ../../../00_input/Ahal.fa 34_snpeff/data/Ahal2.2/sequences.fa
ln -s ../../../00_input/Alyr.fa 34_snpeff/data/Alyr2.2/sequences.fa
ln -s ../../../00_input/Ahal.gff 34_snpeff/data/Ahal2.2/genes.gff
ln -s ../../../00_input/Alyr.gff 34_snpeff/data/Alyr2.2/genes.gff

java -jar $snpeff_jar build -c src/34_snpeff.cfg -gff3 -v Ahal2.2 &> 34_snpeff/Ahal2.2_db.log
java -jar $snpeff_jar build -c src/34_snpeff.cfg -gff3 -v Alyr2.2 &> 34_snpeff/Alyr2.2_db.log
```

### Update VCF

SnpEff ignores all quality filters. Therefore, the feature counts in the report are considerably inflated (from our perspective). Another big problem is two contiguous indels that look like a replacement. For instance 5 bp deletion followed by 5 bp insertion that basically restored the frame. They are reported as two frame-shift mutations.

First, we apply the following filtering.

- Remove all variants that failed the variant filters
- Replace all genotypes that failed the genotype filters with ./.
- If all remaining genotypes are 0/0, remove the variant

We have to use GATK v3.5-0 because v3.4-0 does not provide all the filter types we need. Unused alternates are not removed if they become unused during the same run. Therefore, we need to run it twice.

```shell
module load seq/gatk/3.5-0
java -jar $gatk_jar -T SelectVariants -R 00_input/Ahal.fa -V 19_parentalF/Ahal/filteredAsyn.vcf.gz -o 34_snpeff/AhalSelectedTmp.vcf.gz --excludeFiltered --setFilteredGtToNocall --excludeNonVariants --maxFractionFilteredGenotypes 0.99 --removeUnusedAlternates -nt 4
java -jar $gatk_jar -T SelectVariants -R 00_input/Ahal.fa -V 34_snpeff/AhalSelectedTmp.vcf.gz -o 34_snpeff/AhalSelected.vcf.gz --excludeNonVariants --removeUnusedAlternates -nt 4

java -jar $gatk_jar -T SelectVariants -R 00_input/Alyr.fa -V 19_parentalF/Alyr/filteredAsyn.vcf.gz -o 34_snpeff/AlyrSelectedTmp.vcf.gz --excludeFiltered --setFilteredGtToNocall --excludeNonVariants --maxFractionFilteredGenotypes 0.99 --removeUnusedAlternates -nt 4
java -jar $gatk_jar -T SelectVariants -R 00_input/Alyr.fa -V 34_snpeff/AlyrSelectedTmp.vcf.gz -o 34_snpeff/AlyrSelected.vcf.gz --excludeNonVariants --removeUnusedAlternates -nt 4

rm 34_snpeff/*SelectedTmp*
```

### Annotate the variants

SnpEff cannot use multithreading if collecting stats.

```shell
module load bio/snpeff/4.2
java -Xmx32g -jar $snpeff_jar -c src/34_snpeff.cfg -canon -no-intergenic -no-downstream -no-upstream -s 34_snpeff/Ahal_summary.html Ahal2.2 34_snpeff/AhalSelected.vcf.gz > 34_snpeff/Ahal.vcf
java -Xmx32g -jar $snpeff_jar -c src/34_snpeff.cfg -canon -no-intergenic -no-downstream -no-upstream -s 34_snpeff/Alyr_summary.html Alyr2.2 34_snpeff/AlyrSelected.vcf.gz > 34_snpeff/Alyr.vcf
```

```shell
module load java/jdk/8
java -cp "lib/*" cli.SummariseEffects -i 34_snpeff/Ahal.vcf -o 34_snpeff/Ahal_high.txt --impact HIGH
java -cp "lib/*" cli.SummariseEffects -i 34_snpeff/Alyr.vcf -o 34_snpeff/Alyr_high.txt --impact HIGH
mkdir effects{,_bin}
for ann in frameshift_variant start_lost stop_gained stop_lost; do
	for s in Ahal Alyr; do
		echo "$ann $s"
		java -cp "lib/*" cli.SummariseEffects -i 34_snpeff/${s}.vcf -o 34_snpeff/effects/${s}_${ann}.txt --annotation $ann
		java -cp "lib/*" cli.SummariseEffects -i 34_snpeff/${s}.vcf -o 34_snpeff/effects_bin/${s}_${ann}.txt --annotation $ann -b 1
	done
done
```

### Merge with summary

Note that the `HIGH_gt` value is calculated by combining the values from other `_gt` fields with `or` operator. Thus, it is not the same as the HIGH reported by SnpEff because SnpEff reports more high-impact mutation types than used in this script.

```shell
src/34_mergeEffects.pl -s 34_snpeff/Ahal_summary.genes.txt -d 34_snpeff/effects -p Ahal -g 25 -o 34_snpeff/Ahal_genotypes.txt
src/34_mergeEffects.pl -s 34_snpeff/Alyr_summary.genes.txt -d 34_snpeff/effects -p Alyr -g 25 -o 34_snpeff/Alyr_genotypes.txt

src/34_mergeEffects.pl -s 34_snpeff/Ahal_summary.genes.txt -d 34_snpeff/effects_bin -p Ahal -g 25 -o 34_snpeff/Ahal_genotypes_bin.txt
src/34_mergeEffects.pl -s 34_snpeff/Alyr_summary.genes.txt -d 34_snpeff/effects_bin -p Alyr -g 25 -o 34_snpeff/Alyr_genotypes_bin.txt
```

### Combine parents

To find mutations that are present on both parental sides of the genome, we will combine the genotype binary string using `AND` operator. Note that the `HIGH_gt` field is not a sum of the rest of genotype fields because the types of parental mutations may be different. Thus, a non-zero value in `HIGH_gt` may not have a counterpart in another `gt` field but a non-zero value in another `gt` field will always have a corresponding non-zero value in `HIGH_gt`.

```
src/34_joinGt.pl -a 34_snpeff/Ahal_genotypes_bin.txt -b 34_snpeff/Alyr_genotypes_bin.txt --rbha 00_input/Ahal_on_Atha.rbh --rbhb 00_input/Alyr_on_Atha.rbh --lbla Ahal --lblb Alyr -o 34_snpeff/Both_genotypes.txt -l d
```



## 35. HMA4

Synteny graphs for the HMA4 region. We will drop the BAC sequence because it is much shorter than the other sequences.

### Location

- Atha
	- AT2G18800 8144912
	- AT2G19390 8397862
- Ahal
	- g22664 (AT2G18800) scaffold_116:47046
	- g22716 (AT2G19190) scaffold_116:489911 (end)
	- g29391 (AT2G19210) scaffold_273:1 (start)
	- g29407 (AT2G19390) scaffold_273:102051
- Alyrpet
	- g20852 (AT2G18800) scaffold_52:635351
	- g20903 (AT2G19190) scaffold_52:985017 (end)
	- g29696 (AT2G19210) scaffold_222:91092 (end)
	- g29692 (AT2G19220) scaffold_222:1 (start)
	- g30090 (AT2G19260) scaffold_270:49789 (end)
	- g30079 (AT2G19390) scaffold_270:1466
- Alyrlyr (annotation v2)
	- AL3G53120 (AT2G18800) scaffold_3:23697633
	- AL3G52710 (AT2G19220) scaffold_3:23393337
	- AL3G49070 (AT2G19210) scaffold_3:21242229
	- AL3G48920 (AT2G19400) scaffold_3:21098320

### Extract features

```shell
wd=35_hma4
mkdir -p 35_hma4/gff

src/selectFeaturesGff.pl -l Chr2:8144912..8397862 -a 00_input/Atha.gff.xz -o 35_hma4/gff/Atha.gff

src/selectFeaturesGff.pl -l scaffold_116:47046..489911 -a 00_input/Ahal.gff --tSeqId HMA4_locus --tOffset 1 -o 35_hma4/gff/Ahal.gff
src/selectFeaturesGff.pl -l scaffold_273:1..102051 -a 00_input/Ahal.gff --tSeqId HMA4_locus --tGap --tOffset $((489911 - 47046 + 2)) >> 35_hma4/gff/Ahal.gff

src/selectFeaturesGff.pl -l scaffold_52:635351..985017 -a 00_input/Alyr.gff --tSeqId HMA4_locus --tOffset 1 -o 35_hma4/gff/Alyrpet.gff
src/selectFeaturesGff.pl -l scaffold_222:1..91092 -a 00_input/Alyr.gff --tSeqId HMA4_locus --tOffset $((985017 - 635351 + 2)) --tStrand --tGap >> 35_hma4/gff/Alypetr.gff
src/selectFeaturesGff.pl -l scaffold_270:1466..49789 -a 00_input/Alyr.gff --tSeqId HMA4_locus --tOffset $((985017 - 635351 + 91092 + 2)) --tStrand --tGap >> 35_hma4/gff/Alyrpet.gff

src/selectFeaturesGff.pl -l scaffold_3:23393337..23697633 -a 00_input/AlyrMPGI.gff.xz --tSeqId HMA4_locus --tOffset 1 --tStrand -o 35_hma4/gff/Alyrlyr.gff
src/selectFeaturesGff.pl -l scaffold_3:21098320..21242229 -a 00_input/AlyrMPGI.gff.xz --tSeqId HMA4_locus --tOffset $((23697633 - 23393337 + 2)) --tStrand --tGap >> 35_hma4/gff/Alyrlyr.gff
```

### Extract corresponding CDS.

We need a separate set of GFF files with records that have not been transformed.

```shell
mkdir $wd/cds

ln -s ../gff/Atha.gff 35_hma4/cds/Atha.gff

src/selectFeaturesGff.pl -l scaffold_116:47046..489911 -a 00_input/Ahal.gff --tSeqId HMA4_locus --tOffset 1 -o 35_hma4/gff/Ahal.gff
src/selectFeaturesGff.pl -l scaffold_273:1..102051 -a 00_input/Ahal.gff --tSeqId HMA4_locus --tGap 5000 --tOffset $((489911 - 47046 + 2)) >> 35_hma4/gff/Ahal.gff

src/selectFeaturesGff.pl -l scaffold_52:635351..985017 -a 00_input/Alyr.gff --tSeqID HMA4_locus --tOffset 1 -o 35_hma4/gff/Alyrpet.gff
src/selectFeaturesGff.pl -l scaffold_222:1..91092 -a 00_input/Alyr.gff --tSeqID HMA4_locus --tOffset $((985017 - 635351 + 2)) --tGap 5000 >> 35_hma4/gff/Alyrpet.gff
src/selectFeaturesGff.pl -l scaffold_270:1466..49789 -a 00_input/Alyr.gff --tSeqID HMA4_locus --tOffset $((985017 - 635351 + 91092 + 5000 + 2)) --tStrand --tGap 5000 >> 35_hma4/gff/Alyrpet.gff

src/selectFeaturesGff.pl -l scaffold_3:23393337..23697633 -a 00_input/AlyrMPGI.gff.xz --tSeqId HMA4_locus --tOffset 1 --tStrand -o 35_hma4/gff/Alyrlyr.gff
selectFeaturesGff.pl -l scaffold_3:21097320..21242229 -a 00_input/AlyrMPGI.gff.xz --tSeqId HMA4_locus --tOffset $((23697633 - 23393337 + 2)) --tStrand --tGap 5000 >> 35_hma4/gff/Alyrlyr.gff
```

### Extraction

```
src/extractCds.pl -f 00_input/Atha.fa.xz -g 35_hma4/cds/Atha.gff -o 35_hma4/cds/Atha.fa -l
src/extractCds.pl -f 00_input/Ahal.fa -g 35_hma4/cds/Ahal.gff -o 35_hma4/cds/Ahal.fa -l
src/extractCds.pl -f 00_input/Alyr.fa -g 35_hma4/cds/Alyrpet.gff -o 35_hma4/cds/Alyrpet.fa -l
src/extractCds.pl -f 00_input/AlyrJGI.fa.gz -g 35_hma4/cds/Alyrlyr.gff -o 35_hma4/cds/Alyrlyr.fa -l
```

### All-vs-all blast

This will be more precise than using RBH results from corresponding data projects. Then extract gene pairs.

```shell
module load bio/blast/2.5.0
for i in $wd/cds/*.fa; do species=$(basename ${i%.fa}); sed -r 's/^>/>'$species'_/' $i >> $wd/cds_all.fa; done
blastn -task blastn -query $wd/cds_all.fa -subject $wd/cds_all.fa -evalue 1e-10 -outfmt '6 qacc sacc pident length mismatch gapopen qstart qend sstart send evalue bitscore qcovs qcovhsp qcovus' -out $wd/blast.txt
src/35_pairToList.pl $wd/blast{,_lists}.txt
```

### Synteny lists

Manually generate `synteny_all.txt` based on the identity percentage and query coverage. The key should be either _A. thaliana_ or _A. halleri_ gene id based on their availability.

Extract column pairs that will be used to add homolog tags.

```shell
mkdir $wd/synteny
cut -f 1,5 $wd/synteny_all.txt > $wd/synteny/Ahal.txt
cut -f 2,5 $wd/synteny_all.txt > $wd/synteny/Alyrlyr.txt
cut -f 3,5 $wd/synteny_all.txt > $wd/synteny/Alyrpet.txt
cut -f 4,5 $wd/synteny_all.txt > $wd/synteny/Atha.txt
```

### Append _A. thaliana_ gene id and common names

_A. thaliana_ has to be processed as well because we need to add common tag to the mRNA features.

```shell
cNamePath=00_input/CommonGeneNames.txt
mkdir $wd/gffM
src/appendHomologTags.pl -o $wd/gffM/Ahal.gff --annot $wd/gff/Ahal.gff --hmlg $wd/synteny/Ahal.txt --cNames $cNamePath --hNameTag "atha" --woDesc --addToMrna
src/appendHomologTags.pl -o $wd/gffM/Alyrlyr.gff --annot $wd/gff/Alyrlyr.gff --hmlg $wd/synteny/Alyrlyr.txt --cNames $cNamePath --hNameTag "atha" --woDesc --addToMrna
src/appendHomologTags.pl -o $wd/gffM/Alyrpet.gff --annot $wd/gff/Alyrpet.gff --hmlg $wd/synteny/Alyrpet.txt --cNames $cNamePath --hNameTag "atha" --woDesc --addToMrna
src/appendHomologTags.pl -o $wd/gffM/Atha.gff --annot $wd/gff/Atha.gff --hmlg $wd/synteny/Atha.txt --cNames $cNamePath --hNameTag "atha" --woDesc --addToMrna
```

### Select gene features

Such subset would be easier to comprehend.

```shell
mkdir $wd/gffFS
for i in $wd/gffM/*.gff; do sed -rn '/(\bmRNA\b.+ID=[^.]+\.t?1|\bgap\b)/ p' $i | sed -r 's/(ID=[^.]+)\.t?1/\1/' |  sort -k 4,4n > $wd/gffFS/$(basename $i); done
```

### Make synteny plot


```shell
mkdir graphs/35_hma4
src/syntenyPlot.pl -c src/35_syntenyPlot.yml -o graphs/35_hma4/default.svg
```



## 36. HMA4 copies

Align HMA4 copies for phylogenetic analysis.

### Genes

```shell
wd=36_hma4Copies
mkdir -p $wd/genes
i=0
for f in 30_genesAlignedF/Ahal/g22{694,702,707}.fa; do i=$((i+1)); sed 's/^\(>.\+\)/\1-HMA4-'$i'/' $f > $wd/genes/$(basename $f); done
```

### Align group by group and add _A. thaliana_.

```shell
module load bio/muscle/3.8.3
muscle -profile -in1 $wd/genes/g22694.fa -in2 $wd/genes/g22702.fa -out $wd/genes/1-2.fa
muscle -profile -in1 $wd/genes/1-2.fa -in2 $wd/genes/g22707.fa -out $wd/genes/1-2-3.fa
```

### CDS

```shell
mkdir -p $wd/cds
i=0
for f in 22_alignedF/AllGenes/Ahal/g22{694,702,707}.fa; do i=$((i+1)); sed 's/^\(>\S\+\).*/\1-HMA4-'$i'/' $f > $wd/cds/$(basename $f); done
```

### Align group by group and add _A. thaliana_.

```shell
module load bio/muscle/3.8.3
muscle -profile -in1 $wd/cds/g22694.fa -in2 $wd/cds/g22702.fa -out $wd/cds/1-2.fa
muscle -profile -in1 $wd/cds/1-2.fa -in2 $wd/cds/g22707.fa -out $wd/cds/1-2-3.fa
sed -n '/^>Atha/,$ p' 23_outgroupF/Ahal/AT2G19110_g22694.fa > $wd/cds/Atha.fa
muscle -profile -in1 $wd/cds/1-2-3.fa -in2 $wd/cds/Atha.fa -out $wd/cds/final.fa
```

### Remove the reference

```shell
for f in $wd/cds/g22{694,702,707}.fa; do muscle -profile -in1 $f -in2 $wd/cds/Atha.fa -out ${f%.fa}_Atha.fa; done
```

