#!/usr/bin/env perl

use strict;
use warnings;
use autodie;
use Getopt::Long;
use Pod::Usage;

use IO::Uncompress::AnyUncompress;

my ($man, $help) = (0) x 2;
my ($renameTrans, $renameMRna) = (0) x 2;
my ($pathIn);

GetOptions(
		"h|help" => \$help,
		"man"    => \$man,
		"i=s"    => \$pathIn,
		"t"      => \$renameTrans,
		"m"      => \$renameMRna,
) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;


sub makeId {
	my ($F, $prevParent, $prevId) = @_;
	# Parental mRNA id is in the ID field. Parent field lists parental gene id.
	$F->[8] =~ /ID=([^;]+)/;
	my $parentId = $1;
	my $t = $F->[2];
	if ( ! defined($prevParent->{$t}) || $parentId ne $prevParent->{$t}) {
		$prevId->{$t} = 0;
		$prevParent->{$t} = $parentId;
	}
	$prevId->{$t}++;
	my $i = $prevId->{$t};
	my $newId = sprintf("%s.%s%d", $1, lc($t), $i);
	my $lct = lc($t);
	$F->[8] =~ s/ID=([^;]+)/ID=$newId/;
	# The parent should be transcript rather than gene
	$F->[8] =~ s/Parent=([^;]+)/Parent=$parentId/;
}

# Unless specific label is requested, scan the file to find whether mRNA records are labeled 'mRNA'
# or 'transcript'
my $mRnaLbl = "";
if ($renameTrans) {
	$mRnaLbl = "transcript";
} elsif ($renameMRna) {
	$mRnaLbl = "mRNA";
} else {
	my $fhScan = new IO::Uncompress::AnyUncompress($pathIn);
	while (<$fhScan>) {
		last if $mRnaLbl;
		next if (/^#/);
		chomp;
		my @F = split("\t", $_, 4);
		$mRnaLbl = $F[2] if $F[2] =~ /^(mRNA|transcript)/i;
	}
	close($fhScan);
}

my $fh = new IO::Uncompress::AnyUncompress($pathIn);
my %prevParent;
my %prevId;
my $prevGene;
my $mRnaMissingN = 0;
while (<$fh>) {
	# No commments
	next if (/^#/);

	chomp;
	my @F = split("\t", $_, 9);
	my $type = $F[2];

	# Save gene record
	if ($type =~ /^gene$/i) {
		$prevGene = \@F;
	} elsif ($prevGene) {
		# If the record follows the gene record and is not mRNA, create an mRNA record
		if ($type !~ /^(mRNA|transcript)$/i) {
			$prevGene->[2] = $mRnaLbl;
			# Presumably, records sets without mRNA only have a single transcript. We can't fix 'em all
			$prevGene->[8] =~ s/(ID=[^;]+)/$1.t1/;
			my $gId = $1;
			$prevGene->[8] =~ s/Name/Parent/;
			$prevGene->[8] =~ s/Note=[^;]+;?//;
			print join("\t", @$prevGene), "\n";
			++$mRnaMissingN;
		}
		$prevGene = 0;
	}

	# Make IDs unique
	if ($type =~ /^(intron|exon|cds)$/i) {
		makeId(\@F, \%prevParent, \%prevId);
	}

	# Rename transcript/mRNA if requested
	if ($renameTrans && $type eq 'mRNA') {
		$F[2] = 'transcript';
	} elsif ($renameMRna && $type eq 'transcript') {
		$F[2] = 'mRNA';
	}

	print join("\t", @F), "\n";
}
close($fh);

print STDERR "Added $mRnaMissingN mRNA records\n";



__END__

=encoding utf8

=head1 NAME

fixGffAlyr.pl - fixes problems in the Alyr annotation v2.0.

=head1 SYNOPSIS

fixGffAlyr.pl -i input.gff | xz - > output.gff.xz

=head1 OPTIONS

=over 8

=item B<-i>

Alyr annotation file v2.0 (or a file with similar formatting).

=item B<-t>

(Optional) If specified, 'mRNA' type will be renamed to 'transcript'

=item B<-m>

(Optional) If specified, 'transcript' type will be renamed to 'mRNA'

=back

=head1 DESCRIPTION

The script Fixes the following problems in the functional annotation of Alyr.

=over 8

=item

1. Fix Parent values for introns, exons, and CDS. The original value of Parent is gene ID while the
transcript parent is specified as ID.

=item

2. Makes cds, intron, exon, transcription_start_site, transcription_end_site, start_codon, and
stop_codon IDs unique by adding ordinal numbers (within mRNA)

=item

3. Adds mRNA record to genes that lack it.

=item

4. (Optional) Rename 'mRNA' type to 'transcript' or vice versa

=back

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut


