#!/usr/bin/env perl

use strict;
use warnings;
use autodie;
use Getopt::Long;
use Log::Log4perl qw(:easy);
use Pod::Usage;

use Bio::SeqIO;
use Capture::Tiny qw( capture );
use File::Path qw( make_path);
use File::Basename;
use File::Spec;
use File::Temp;
use IO::Uncompress::AnyUncompress;


our $delim = "\t";
my $alignCmdPtrn = "muscle -profile -in1 %s -in2 %s";
# Pattern to extract gene name from the existing alignment file name
my $fnPtrn = qr/^([^_.]+).+$/;
my $man = 0;
my $help = 0;
my $logLvlStr = 'i';
my $rmRefBefore = 0;
my ($dirIn, $pathOutgroup, $dirOut, $outgroupLbl, $rmRefSeqId, $fnPtrnStr);

GetOptions(
	"h|help" => \$help,
	"man"    => \$man,
	"v=s"    => \$logLvlStr,
	"ing=s"  => \$dirIn,
	"outg=s" => \$pathOutgroup,
	"o=s"    => \$dirOut,
	"outgLbl=s" => \$outgroupLbl,
	"rmRefSeqId:i" => \$rmRefSeqId,
	"rmRefBefore"  => \$rmRefBefore,
	"fnPtrn:s" => \$fnPtrnStr,
) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;

die("Input directory is required") unless $dirIn;
die("FASTA file with outgroup sequences is required") unless $pathOutgroup;
die("Output directory is required") unless $dirOut;
die("Outgroup species label is required") unless $outgroupLbl;

$fnPtrn = qr/$fnPtrnStr/ if $fnPtrnStr;
# If positive, convert into origin 0.
--$rmRefSeqId if ($rmRefSeqId && $rmRefSeqId > 0);

my %logLvlDict = ("d" => $DEBUG, "i" => $INFO, "w" => $WARN, "e" => $ERROR, "f" => $FATAL);
$logLvlStr = "i" unless exists($logLvlDict{$logLvlStr});
Log::Log4perl->easy_init({level => $logLvlDict{$logLvlStr}, layout => "%d %p %m%n"});

# Loads FASTA file into a hash using display id as key and Bio::Seq as value
sub loadFasta {
	my ($pathFasta) = @_;
	my $fh = new IO::Uncompress::AnyUncompress($pathFasta);
	my $reader = new Bio::SeqIO( -fh => $fh, -format => 'fasta' );
	my %out;
	while (my $seq = $reader->next_seq()) {
		# Remove description and set display name to the outgroup label
		my $geneId = $seq->display_id;
		$seq->desc('');
		$seq->display_id($outgroupLbl);
		$out{$geneId} = $seq;
	}
	$reader->close();
	return \%out;
}

INFO("Loading outgroup sequences");
my $outgroupSeqs = loadFasta($pathOutgroup);

INFO("Preparing the output directory");
make_path($dirOut) unless (-d $dirOut);

INFO("Aligning outgroup sequences");
my $cnt = 0;
for my $pathGene (glob("$dirIn/*.fa*")) {
	my ($fn, $p, $sfx) = fileparse($pathGene, qr/\.[^.]*/);
	(my $geneName = $fn) =~ s/$fnPtrn/$1/;
	my $pathOut = File::Spec->join($dirOut, $fn . $sfx);

	my $fhOutgroup = File::Temp->new(TEMPLATE => 'tempOutgroupXXXXXXX', DIR => $dirOut, SUFFIX => '.fa');
	my $writerOutgroup = new Bio::SeqIO(-fh => $fhOutgroup, -format => 'fasta', -alphabet => 'dna');
	my $outgroupSeq = $outgroupSeqs->{$geneName};
	if (defined($outgroupSeq)) {
		$writerOutgroup->write_seq($outgroupSeq);
	} else {
		ERROR("($geneName) is not in ($pathOutgroup)");
		next;
	}

	# We have to loop twice in case we need to remove the reference
	my @ingroup;
	my $readerIngroup = new Bio::SeqIO(-file => $pathGene, -format => 'fasta', -alphabet => 'dna');
	while (my $seq = $readerIngroup->next_seq()) {
		push(@ingroup, $seq);
	}
	
	splice(@ingroup, $rmRefSeqId, 1) if ($rmRefBefore && $rmRefSeqId);
	
	my $fhIngroup = File::Temp->new(TEMPLATE => 'tempIngroupXXXXXXX', DIR => $dirOut, SUFFIX => '.fa');
	my $writerIngroup = new Bio::SeqIO(-fh => $fhIngroup, -format => 'fasta', -alphabet => 'dna');
	for my $seq (@ingroup) {
		$writerIngroup->write_seq($seq);
	}

	my ($alignCmdOut, $alignCmdErr, $alignCmdExit) = capture {
		system(sprintf($alignCmdPtrn, $fhIngroup->filename, $fhOutgroup->filename));
	};
	if ($alignCmdExit != 0) {
		ERROR("($geneName) alignment failed with exit code $alignCmdExit");
		ERROR($alignCmdErr);
	}

	my @alignment;
	open(my $fhAlignment, "<", \$alignCmdOut);
	my $readerAlignment = new Bio::SeqIO(-fh => $fhAlignment, -format => 'fasta', -alphabet => 'dna');
	while (my $seq = $readerAlignment->next_seq()) {
		push(@alignment, $seq);
	}
	# Remove the reference if necessary
	splice(@alignment, $rmRefSeqId, 1) if ( ! $rmRefBefore && $rmRefSeqId);

	my $writerAlignment = new Bio::SeqIO(-file => "> $pathOut", -format => 'fasta', -alphabet => 'dna');
	for my $seq (@alignment) {
		$writerAlignment->write_seq($seq);
	}

	if (++$cnt % 1000 == 0) {
		INFO("Aligned $cnt genes");
	}
}

INFO "Aligned $cnt genes total";
INFO("Done");



__END__

=head1 NAME

alignOutgroup.pl - aligns outgroup sequence to previously aligned ingroup sequences

=head1 SYNOPSIS

alignOutgroup.pl --ing dirIngroup --outg outgroupSeqs.fa -o dirOut --outgLbl Atha --rmRefSeqId '-1'

=head1 OPTIONS

=over 8

=item B<--ing>

Directory where aligned ingroup sequences reside (one file per gene). The file naming should follow
the pattern specified by C<--fnPtrn>

=item B<--outg>

Fasta file with outgroup sequences. The display id should match gene names used in the ingroup file
names.

=item B<-o>

Output directory. Input file names will be preserved.

=item B<--outgLbl>

Outgroup label to use as display_id

=item B<--rmRefSeqId>

(Optional) Origin 1 id of the reference sequence to be removed. Use negative number to count from
the end (the last sequence is -1). Use 0 to keep all sequences. If C<--rmRefBefore> is specified,
take into account the outgroup sequences, i.e. the last sequence will be the outgroup sequence.
Default: 0

=item B<--rmRefBefore>

(Optional) If specified, the reference is removed before the alignment is done. If there are many
sites with missing data, it may be better to keep the reference until after the alignment. The
reference may have very little missing data and that would improve alignments. If there are few
missing sites, it should not matter when the reference needs to be removed.

=item B<--fnPtrn>

File name pattern of the input fasta files. It will be used to extract gene names. Default: '^([^_.]+).+$'

=back

=head1 DESCRIPTION

The script aligns the corresponding outgroup sequence to each of the ingroup alignments that reside
in the input directory. The aligned sequence will be named according to the specified C<--outgLbl>
while any description will be removed. Reference sequence may be removed by specifying
C<--rmRefSeqId>. The script uses C<muscle> aligner, so its installation path should be in C<$PATH>.
The output file names will match the input file names.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut


