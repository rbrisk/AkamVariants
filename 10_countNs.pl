#!/usr/bin/env perl

use strict;
use warnings;

use Getopt::Long;
use Log::Log4perl qw(:easy);
use Pod::Usage;
use Bio::SeqIO;
use File::Basename;
use File::Spec;

our $delim = "\t";
my $man = 0;
my $help = 0;
my $logLvlStr = "i";
# File name pattern to extract gene id. Default: everything before the file extension
my $fnPtrn = qr/^(.+)\.[^.]+$/;
my ($dirAlignments, $pathOut);

GetOptions(
	"h|help" => \$help,
	"man"    => \$man,
	"v=s"    => \$logLvlStr,
	"a=s"    => \$dirAlignments,
	"o=s"    => \$pathOut,
	"fnPtrn:s" => \$fnPtrn,
) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;

my %logLvlDict = ("d" => $DEBUG, "i" => $INFO, "w" => $WARN, "e" => $ERROR, "f" => $FATAL);
$logLvlStr = "i" unless exists($logLvlDict{$logLvlStr});
Log::Log4perl->easy_init({level => $logLvlDict{$logLvlStr}, layout => "%d %p %m%n"});

die("Directory with alignment files is required") unless $dirAlignments;
die("Output path is required") unless $pathOut;
die("Directory with alignment files ($dirAlignments) does not exist") unless -d $dirAlignments;

open(FOUT, ">", $pathOut) or die("Unable to write to the output file ($pathOut). $!");
my $cnt = 0;
for my $f (glob(File::Spec->join($dirAlignments, "*"))) {
	if ($cnt == 0) {
		my $reader = Bio::SeqIO->new(-file => $f, -format => 'fasta');
		my @colNames = qw/ Gene /;
		while (my $seq = $reader->next_seq()) {
			push(@colNames, $seq->display_name());
		}
		print FOUT join("\t", @colNames), "\n";
	}
	(my $geneName = basename($f)) =~ s/$fnPtrn/$1/;
	my $reader = Bio::SeqIO->new(-file => $f, -format => 'fasta');
	my @row = ($geneName);
	while (my $seq = $reader->next_seq()) {
		my $n = 0;
		map { $n++ if $_ =~ /^[Nn]$/ } split(//, $seq->seq);
		push(@row, sprintf("%0.4f", ($n + 0.0) / $seq->length));
	}
	print FOUT join("\t", @row), "\n";
	$cnt++;
	INFO("Processed $cnt files") if ($cnt % 1000 == 0);
	#last if $cnt == 8000;
}
close(FOUT);
INFO("Processed $cnt files");


__END__

=encoding utf8

=head1 NAME

10_countNs.pl - counts the percentage of Ns in alignment files

=head1 SYNOPSIS

10_countNs.pl -a dirAlignments -o output.txt

=head1 OPTIONS

=over 8

=item B<-a>

Path to the directory where gene alignments are stored

=item B<-o>

Path to the output file

=item B<--fnPtrn>

(Optional) File name pattern to extract gene id from the file name. Default: '^(.+)\.[^.]+$'

=item B<-v>

Log level ('d' - debug, 'i' - info, 'w' - warn, 'e' - error, 'f' - fatal). Default: 'i'

=back

=head1 DESCRIPTION

The script counts the percentage of missing nucleotides in each entry of each fasta alignment file
from the specified directory. The results are written as a tab-delimited table where gene names
obtained from file names are stored in rows and accession names are in columns.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut



