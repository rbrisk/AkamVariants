#!/usr/bin/env perl

# Converts pairs from blast output into lists, i.e. first column is used for keys while
# corresponding values in the second column will be joined into a list.

use strict;
use warnings;
use autodie;
use POSIX ();

my ($pathIn, $pathOut) = @ARGV;

open(my $fhIn,  '<', $pathIn);

my $hits = {};
while (<$fhIn>) {
	chomp;
	my @F = split(/\s+/, $_);
	my ($geneA, $geneB, $pident, $bitscore, $qcovs) = @F[0,1,2,11,12];
	next if $geneA eq $geneB;
	$hits->{$geneA} = {} unless defined $hits->{$geneA};
	$hits->{$geneA}{$geneB} = join(",", POSIX::round($pident), $qcovs, $bitscore);
}
close($fhIn);

open(my $fhOut, '>', $pathOut);
for my $geneA (sort { $a cmp $b } keys(%{$hits})) {
	my @valList = sort { $a cmp $b } keys %{$hits->{$geneA}};
	my @valOut = map { "$_ (" . $hits->{$geneA}{$_} . ")" } @valList;
	print $fhOut join("\t", $geneA, join("; ", @valOut)), "\n";
}
close($fhOut);



