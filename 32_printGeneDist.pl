#!/usr/bin/env perl

use strict;
use warnings;
use autodie;

use Carp;
use Getopt::Long;
use FlankSeqListMaker;
use Log::Log4perl qw(:easy);
use Pod::Usage;

my $help = 0;
my $man = 0;
my $featureType = 'mRNA';
my $logLvlStr = "i";
# This value is irrelevant but required by the module
my $flankWidth = 500;
my ($pathGff, $pathFasta, $pathOut);

GetOptions(
		"h|help" => \$help,
		"man"    => \$man,
		"gff=s"  => \$pathGff,
		"fa=s"   => \$pathFasta,
		"o=s"    => \$pathOut,
		"type:s" => \$featureType,
		"l=s"    => \$logLvlStr,
) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;

my %logLvlDict = ("d" => $DEBUG, "i" => $INFO, "w" => $WARN, "e" => $ERROR, "f" => $FATAL);
$logLvlStr = "i" unless exists($logLvlDict{$logLvlStr});
Log::Log4perl->easy_init({level => $logLvlDict{$logLvlStr}, layout => "%d %p %m%n"});

die("Input GFF file is required") unless $pathGff;
die("Input assembly FASTA file is required") unless $pathFasta;
die("Output path is required") unless $pathOut;
die("Feature type is required") unless $featureType;


my $fslm = FlankSeqListMaker->new();

INFO("Retrieving sequence lengths");
open(my $fhFasta, '<', $pathFasta);
$fslm->loadSeqLengths($fhFasta);
$fhFasta->close();

INFO("Loading $featureType features");
open(my $fhGff, '<', $pathGff);
$fslm->loadFeatures($fhGff, $featureType, $flankWidth);
$fhGff->close();

INFO("Removing spanned features");
$fslm->filterSpanned();

INFO("Printing gene distances");
open(my $fhOut, '>', $pathOut);
$fslm->printDistances($fhOut);
$fhOut->close();


__END__


=encoding utf8

=head1 NAME

32_makeFlankSeqList.pl - Generates lists of flanking sequences that do not overlap

=head1 SYNOPSIS

32_makeFlankSeqList.pl --gff annot.gff --fa sequences.fasta -5 out5p.txt -3 out3p.txt --width 500 --type mRNA

=head1 OPTIONS

=over 8

=item B<--gff>

Path to the annotation file (compression supported)

=item B<--fa>

Path to the FASTA file with corresponding sequences (compression supported)

=item B<-5>

Path to the output file for 5' flanking sequences (txt)

=item B<-3>

Path to the output file for 3' flanking sequences (txt)

=item B<--width>

(Optional) Length of the flanking sequences. Default: 500

=item B<--type>

(Optional) Type of features to process. Typically mRNA or Transcript. Default: 'mRNA'

=back

=head1 DESCRIPTION

The script generates two lists of flanking sequences (5' and 3') that do not overlap. In order to be
included, the originating feature should not overlap, span, or be included into another feature. In
addition, its flanking sequences should not overlap with any other flanking sequences. If only one
of the flanking sequences overlaps with another, neither of the feature's flanking sequences is
reported. The script makes sure that all flanking sequences have the specified length, i.e. if the
feature is too close to the sequence end resulting in a truncated flanking sequence, the feature's
flanking sequences are dropped.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut

