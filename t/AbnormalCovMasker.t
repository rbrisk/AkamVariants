use strict;
use warnings;
use Bio::PrimarySeq;
use Carp qw(confess);

use Test::More tests => 6;

BEGIN {
	use_ok("AbnormalCovMasker");
}

require_ok("AbnormalCovMasker");

sub addSeqCov {
	my ($data, $id, $start, @cov) = @_;
	$data->{$id} = [ $start, \@cov ];
}


sub asFh {
	my ($data) = @_;
	my $dataStr = "";
	open(my $fh, ">", \$dataStr) or confess("Something is wrong. $!");
	for my $seqId (keys %$data) {
		my $pos = $data->{$seqId}[0];
		my $cov = $data->{$seqId}[1];
		for my $c (@$cov) {
			print $fh join("\t", $seqId, $pos++, $c), "\n";
		}
	}
	close($fh);
	open($fh, "<", \$dataStr) or confess("Something is wrong. $!");
	return $fh;
}


sub mkSeq {
	my ($id, $s) = @_;
	return Bio::PrimarySeq->new(-id => $id, -seq => $s);
}


sub mkDefSeq {
	mkSeq('s1', 'ACGT' x 4);
}



subtest "makeMissingRanges" => sub {
	plan tests => 11;
	my ($data, $r, $last, $expR, $expLast);

	$data = {};
	addSeqCov($data, 's1', 0, qw/ 4 4 5 2 3 2 0 4 5/);
	$expR = { 's1' => [[3,6]] };
	$expLast = { 's1' => 8 };
	($r, $last) = AbnormalCovMasker->makeMissingRanges(-covFh => asFh($data), -minCov => 4);
	is_deeply($last, $expLast, "Last position");
	is_deeply($r, $expR, "Min threshold");

	$data = {};
	addSeqCov($data, 's1', 0, qw/ 4 4 6 8 7 9 8 6 5/);
	($r, $last) = AbnormalCovMasker->makeMissingRanges(-covFh => asFh($data), -maxCov => 6);
	is_deeply($r, $expR, "Max threshold");

	$data = {};
	addSeqCov($data, 's1', 4, qw/ 5 5 6 7 8 9 9 8 /);
	$expR = { 's1' => [[0, 3]] };
	$expLast = { 's1' => 11 };
	($r, $last) = AbnormalCovMasker->makeMissingRanges(-covFh => asFh($data));
	is_deeply($r, $expR, "Leading period");
	is_deeply($r, $expR, "Leading period last position");

	$data = {};
	addSeqCov($data, 's1', 4, qw/ 1 2 3 3 4 3 0 1 2 1 0 /);
	$expR = { 's1' => [] };
	$expLast = { 's1' => 14 };
	($r, $last) = AbnormalCovMasker->makeMissingRanges(-covFh => asFh($data), -minCov => 0);
	is_deeply($r, $expR, "Zero min coverage");
	is_deeply($last, $expLast, "Zero min coverage last position");

	$data = {};
	addSeqCov($data, 's1', 0, qw/ 1 3 4 5 6 5 3 2 1 0 1 4 5 4 0 0 /);
	$expR = { 's1' => [ [0, 0], [7, 10], [14, 15] ] };
	$expLast = { 's1' => 15 };
	($r, $last) = AbnormalCovMasker->makeMissingRanges(-covFh => asFh($data), -minCov => 3);
	is_deeply($r, $expR, "Multiple ranges");
	is_deeply($last, $expLast, "Tail is range");
	
	$data = {};
	addSeqCov($data, 's1', 0, qw/ 1 3 4 5 6 5 3 2 1 0 1 4 5 4 0 0 /);
	addSeqCov($data, 's2', 5, qw/ 7 7 8 9 9 8 7 5 2 1 /);
	addSeqCov($data, 's3', 0, qw/ 4 4 5 5 6 5 4 3 /);
	$expR = { 
			's1' => [ [0, 0], [7, 10], [14, 15] ],
			's2' => [ [0, 4], [13, 14] ],
			's3' => [] 
		};
	$expLast = { 's1' => 15, 's2' => 14, 's3' => 7 };
	($r, $last) = AbnormalCovMasker->makeMissingRanges(-covFh => asFh($data), -minCov => 3);
	is_deeply($r, $expR, "Multiple sequence ranges");
	is_deeply($last, $expLast, "Multiple sequence last position");
	
};	


subtest "maskSeq" => sub {
	plan tests => 6;
	my ($seq, $ranges, $expSeq);
	
	$seq = mkDefSeq();
	$ranges = [ [0, 5] ];
	$expSeq = mkSeq('s1', ('N' x 6) . 'GT' . ('ACGT' x 2));
	AbnormalCovMasker->maskSeq(-seq => $seq, -ranges => $ranges);
	is_deeply($seq, $expSeq, "Range left");

	$seq = mkDefSeq();
	$ranges = [ [13, 15] ];
	$expSeq = mkSeq('s1', 'ACGT' x 3 . 'ANNN');
	AbnormalCovMasker->maskSeq(-seq => $seq, -ranges => $ranges);
	is_deeply($seq, $expSeq, "Range right");

	$seq = mkDefSeq();
	$ranges = [ [4, 4] ];
	$expSeq = mkSeq('s1', 'ACGT' . 'NCGT' . 'ACGT' x 2);
	AbnormalCovMasker->maskSeq(-seq => $seq, -ranges => $ranges);
	is_deeply($seq, $expSeq, "Range middle");

	$seq = mkDefSeq();
	$ranges = [ [0, 3], [7, 8], [15, 15] ];
	$expSeq = mkSeq('s1', 'NNNN' . 'ACGN' . 'NCGT' . 'ACGN');
	AbnormalCovMasker->maskSeq(-seq => $seq, -ranges => $ranges);
	is_deeply($seq, $expSeq, "Multiple ranges");

	$seq = mkDefSeq();
	$ranges = [ ];
	$expSeq = mkSeq('s1', 'ACGT' x 4);
	AbnormalCovMasker->maskSeq(-seq => $seq, -ranges => $ranges);
	is_deeply($seq, $expSeq, "No ranges");

	$seq = mkDefSeq();
	$ranges = [ [0, 3], [7, 8], [15, 15] ];
	$expSeq = mkSeq('s1', '----' . 'ACG-' . '-CGT' . 'ACG-');
	AbnormalCovMasker->maskSeq(-seq => $seq, -ranges => $ranges, -char => '-');
	is_deeply($seq, $expSeq, "Custom char");
};



subtest "addLastRange" => sub {
	plan tests => 5;
	my ($ranges, $seq, $exp);

	$seq = mkDefSeq();
	$ranges = [ [3, 7] ];
	AbnormalCovMasker->addLastRange($seq, $ranges, 14);
	$exp = [ [3, 7], [15, 15] ];
	is_deeply($ranges, $exp, "Last position before the end");

	$ranges = [ [3, 7] ];
	AbnormalCovMasker->addLastRange($seq, $ranges, 15);
	$exp = [ [3, 7] ];
	is_deeply($ranges, $exp, "Last position at the end");

	$ranges = [ ];
	AbnormalCovMasker->addLastRange($seq, $ranges, 11);
	$exp = [ [12, 15] ];
	is_deeply($ranges, $exp, "Empty range array");

	$ranges = [ [3, 5], [9, 11] ];
	AbnormalCovMasker->addLastRange($seq, $ranges, 13);
	$exp = [ [3, 5], [9, 11], [14, 15] ];
	is_deeply($ranges, $exp, "Multiple ranges");

	is_deeply($seq, mkDefSeq(), "Sequence preserved");
};



subtest "fixLastRange" => sub {
	plan tests => 5;
	my ($ranges, $seq, $exp);

	$seq = mkDefSeq();

	$ranges = [ [2, 5], [16, 16] ];
	AbnormalCovMasker->fixLastRange($seq, $ranges);
	$exp = [ [2, 5] ];
	is_deeply($ranges, $exp, "Last range out of bounds");

	$ranges = [ [2, 5], [14, 16] ];
	AbnormalCovMasker->fixLastRange($seq, $ranges);
	$exp = [ [2, 5], [14, 15] ];
	is_deeply($ranges, $exp, "Right boundary ouf of bounds");

	$ranges = [ [2, 5], [7, 9] ];
	AbnormalCovMasker->fixLastRange($seq, $ranges);
	$exp = [ [2, 5], [7, 9] ];
	is_deeply($ranges, $exp, "Ranges in bounds");

	$ranges = [ [16, 16] ];
	AbnormalCovMasker->fixLastRange($seq, $ranges);
	$exp = [ ];
	is_deeply($ranges, $exp, "Single range");

	is_deeply($seq, mkDefSeq(), "Sequence preserved");
};
	
