package AbnormalCovMasker;

use strict;
use warnings;

use Bio::Seq;
use Carp;


BEGIN {
	use Exporter;
	our ( $VERSION, @ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS );

	$VERSION     = 0.10;
	@ISA         = qw(Exporter);
	@EXPORT      = ();
	@EXPORT_OK   = ();
	%EXPORT_TAGS = ();
}



=head2 makeMissingRanges
Title    : makeMissingRanges
Usage    : makeMissingRanges(-covFh => $fh, -minCov => 2, -maxCov = 100)
Function : Combines regions with abnormal coverage into ranges
Returns  : Array that consists of a reference to a dictionary whose keys are sequence ids and values 
		are arrays whose elements contain start and stop positions of a range. The second element if a
		reference to a dictionary whose keys are sequence ids and values are last seen positions.
Args     : -covFh - file handle to the tab-delimited coverage data. Each line should have sequence i
				id, position, and coverage.
		-minCov - (Optional) Minimum allowed coverage. Default: 1
		-maxCov - (Optional) Maximum allowed coverage. Default: 1000
=cut

sub makeMissingRanges {
	my $self = shift;
	my %args = @_;
	my $fh = $args{'-covFh'};
	Carp::confess("-covFh is required") unless $fh;
	my $minCov = defined $args{'-minCov'} ? $args{'-minCov'} : 1;
	my $maxCov = defined $args{'-maxCov'} ? $args{'-maxCov'} : 1000;

	my $seqCov = {};
	# Since zeros at the end of sequence may not be printed, we record the last reported position for
	# each sequence.
	my $seqLastPos = {};
	my $prevSeqId = "";
	while (<$fh>) {
		chomp;
		my ($seqId, $pos, $posCov) = split("\t", $_);
		$seqLastPos->{$seqId} = $pos;
		if ($seqId ne $prevSeqId) {
			$prevSeqId = $seqId;
			$seqCov->{$seqId} = [];
			# Bamtools skips the first positions if they do not have coverage
			if ($pos > 0 && $minCov > 0) {
				push(@{$seqCov->{$seqId}}, [0, $pos-1]);
			}
		}
		if ($posCov < $minCov || $posCov > $maxCov) {
			if (@{$seqCov->{$seqId}} == 0 || $pos != $seqCov->{$seqId}[-1][1] + 1) {
				push(@{$seqCov->{$seqId}}, [$pos, $pos]);
			} else {
				$seqCov->{$seqId}[-1][1]++;
			}
		}
	}
	close($fh);
	
	return ($seqCov, $seqLastPos);
}



=head2 maskSeq
Title    : maskSeq
Usage    : maskSeq(-seq => $seq, -ranges => $ranges)
Function : Masks the specified ranges in place
Returns  : Bio::Seq where the specified ranges were replaced with Ns.
Args     : -seq - Bio::Seq object whose sequence should be masked
		-ranges - Array of ranges. Each element of the array should have origin 0 start and stop positions.
		-char - (Optional) Character to use for masking. Default: 'N'
=cut

sub maskSeq {
	my $self = shift;
	my %args = @_;
	my $seq = $args{'-seq'};
	my $ranges = $args{'-ranges'};
	my $char = defined $args{'-char'} ? $args{'-char'} : 'N';
	Carp::confess('-seq argument is required') unless $seq;
	Carp::confess('-ranges argument is required') unless $ranges;
	Carp::confess("Invalid masking character '$char'.") unless length($char) == 1;
	for my $r (@$ranges) {
		my $n = $r->[1] - $r->[0] + 1;
		$seq->subseq(
				-start => $r->[0] + 1,
				-end   => $r->[1] + 1,
				-replace_with => $char x $n,
			);
	}
}



=head2 fixLastRange
Title    : fixLastRange
Usage    : fixLastRange($seq, $ranges)
Function : Sometimes bamtools return one extra position at the end. This will amend the last range to be within limits.
Returns  :
Args     : $seq - Bio::Seq object
		$ranges - Ref ot array of ranges. Each element of the array should have origin 0 start and stop positions.
=cut

sub fixLastRange {
	my $self = shift;
	my ($seq, $ranges) = @_;
	my $maxPos = $seq->length - 1;
	# If the whole last range is out of bounds, remove it. Otherwise, move the right boundary within
	# bounds.
	if ($ranges->[-1][0] > $maxPos) {
		pop @$ranges;
	} elsif ($ranges->[-1][1] > $maxPos) {
		$ranges->[-1][1] = $maxPos;
	}
}



=head2 addLastRange
Title    : addLastRange
Usage    : addLastRange($seq, $ranges, $lastPos)
Function : If the last reported position less than sequence length, add the range to cover the remaining sequence.
Returns  : 
Args     : $seq - Bio::Seq object
		$ranges - Ref to array of ranges. Each element of the array should have origin 0 start and stop positions.
		$lastPos - Last reported position with coverage, origin 0
=cut

sub addLastRange {
	my $self = shift;
	my ($seq, $ranges, $lastPos) = @_;
	if ($seq->length > $lastPos + 1) {
		# From the next position to the end. Coverage positions are origin 0.
		push(@$ranges, [$lastPos + 1, $seq->length - 1]);
	}
}


1;

__END__

=encoding utf8

=head1 NAME

AbnormalCovMasker - masks abnormal coverage of the reference

=head1 SYNOPSIS

TODO

=head1 DESCRIPTION

AbnormalCovMasker replaces nucleotides with N's if the coverage of the base is too low or too high.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut
