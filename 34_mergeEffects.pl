#!/usr/bin/env perl

use strict;
use warnings;
use autodie;

use Carp;
use Getopt::Long;
use File::Basename;
use Log::Log4perl qw(:easy);
use Pod::Usage;

my $help = 0;
my $man = 0;
my $logLvlStr = "i";
my $delim = "\t";
my $effExt = ".txt";
my ($pathSummary, $dirData, $pathOut, $parent, $gtN, $combineLbl);

GetOptions(
		"h|help"   => \$help,
		"man"      => \$man,
		"s|sum=s"  => \$pathSummary,
		"d|data=s" => \$dirData,
		"o|out=s"  => \$pathOut,
		"p|parent=s"  => \$parent,
		"g|gtn=i"     => \$gtN,
		"c|combine:s" => \$combineLbl,
		"l|loglvl:s"  => \$logLvlStr,
) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;

my %logLvlDict = ("d" => $DEBUG, "i" => $INFO, "w" => $WARN, "e" => $ERROR, "f" => $FATAL);
$logLvlStr = "i" unless exists($logLvlDict{$logLvlStr});
Log::Log4perl->easy_init({level => $logLvlDict{$logLvlStr}, layout => "%d %p %m%n"});

die("Input summary file is required") unless $pathSummary;
die("Data directory is required") unless $dirData;
die("Output path is required") unless $pathOut;
die("Parental species is required") unless $parent;
die("Genotype number is required") unless $gtN;
die("Data directory does not exist") unless -d $dirData;
die("Genotype number should be positive") unless $gtN > 0;


INFO("Loading summary");
open(my $fhSummary, '<', $pathSummary);
my (@genes, %geneData);
# Skip the useless comment
<$fhSummary>;
my $headerStr = <$fhSummary>;
chomp($headerStr);
$headerStr =~ s/^#//;
my @header = split($delim, $headerStr);
my @headerNew = map { 
		$_ =~ s/^variants_impact_(.+)$/$1_impact/;
		$_ =~ s/^variants_effect_(.+)$/$1/;
	} @header;
while (<$fhSummary>) {
	chomp;
	my @F = split($delim);
	my $geneId = $F[1];
	push(@genes, $geneId);
	$geneData{$geneId} = \@F;
}
$fhSummary->close();

my $defaultGtStr = "0" x $gtN;

for my $pathEff (glob("$dirData/${parent}_*$effExt")) {
	my %effects;
	(my $effLabel = basename($pathEff, $effExt)) =~ s/^${parent}_//;

	INFO("Merging $effLabel");
	open(my $fhEff, '<', $pathEff);
	my $headerStr = <$fhEff>;
	while (<$fhEff>) {
		chomp;
		my ($geneId, $gtStr) = split($delim);
		if (length($gtStr) != $gtN) {
			confess("Invalid number of genotypes in '$gtStr' string of '$pathEff'.");
		}
		$effects{$geneId} = $gtStr;
	}
	$fhEff->close();

	push(@header, $effLabel . "_gt");
	for my $g (@genes) {
		my $gtStr = defined($effects{$g}) ? $effects{$g} : $defaultGtStr;
		push(@{$geneData{$g}}, $gtStr);
	}
}


INFO("Printing results");
open(my $fhOut, '>', $pathOut);
print $fhOut join($delim, @header), "\n";
for my $g (@genes) {
	print $fhOut join($delim, @{$geneData{$g}}), "\n";
}
$fhOut->close();

INFO("Done");



__END__


=encoding utf8

=head1 NAME

34_mergeGtEffects.pl - Merges genotype effect files into SnpEff summary file.

=head1 SYNOPSIS

34_mergeGtEffects.pl -s summary.genes.txt -d effects -p Ahal -g 25

=head1 OPTIONS

=over 8

=item B<-s>, B<--sum>

Path to SnpEff summary gene file.

=item B<-d>, B<--data>

Data directory where genotype effect files reside. The file names should have
'<parent>_<effect_name>.txt' format. The script will merge all files that match the specified
parent.

=item B<-o>, B<--out>

Output file.

=item B<-p>, B<--parent>

Name of the parental species used to select the genotype effect files in the data directory.

=item B<-g>, B<--gtn>

Number of genotypes in each genotype effect file. The script does not validate the incoming data.


=back

=head1 DESCRIPTION

The script merges genotype effect files with the SnpEff gene summary file. If the effect file is
missing any genes, they are added as all-zero strings. The effect file names are expected to be in
'<parent>_<effect_name>.txt' format. The number of genotypes should be the same in all effect files.
If the number of genotypes in an effect file does not match the specified genotype number, the
script will fail.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut

