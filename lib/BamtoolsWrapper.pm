package BamtoolsWrapper;

use strict;
use warnings;
use Carp;

BEGIN {
	use Exporter;
	our ($VERSION, @ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS);

	$VERSION     = 0.10;
	@ISA         = qw(Exporter);
	@EXPORT      = ();
	@EXPORT_OK   = ();
	%EXPORT_TAGS = ();
}


=head2 getCoverageFh
Title    : getCoverageFh
Usage    : BamtoolsWrapper->getCoverage(-pathBam => "file.bam", -minMQ => 10)
Function : Obtains coverage from the specified BAM file. Reads are filtered for duplicates, QC,
		mapping, and primary alignment. Bamtools should be in the PATH.
Returns  : File handle to the output (pipe)
Args     : -pathBam - path to the alignment file in BAM format
		-minMQ - (Optional) minimum mapping quality. Reads with lower MQ will not count towards the coverage. Default: 0
=cut
sub getCoverageFh {
	my $self = shift;
	my %args = @_;
	my $pathBam = $args{'-pathBam'};
	Carp::confess("-pathBam is required") unless $pathBam;
	Carp::confess("Alignment BAM file '$pathBam' does not exist") unless -f $pathBam;
	
	my $minMQ = defined $args{'-minMQ'} ? $args{'-minMQ'} : 0;

	my $cmd = "bamtools filter -in $pathBam " . 
			"-mapQuality '>=$minMQ' " .
			"-isDuplicate 'false' " .
			"-isFailedQC 'false' " .
			"-isMapped 'true' " .
			"-isPrimaryAlignment 'true' " .
			"| bamtools coverage ";
	open(my $ph, "$cmd |") or Carp::confess("Unable to read bamtools output. $!");

	return $ph;
}


1;

__END__

=encoding utf8

=head1 NAME

BamtoolsWrapper - wraps bamtools functionality

=head1 SYNOPSIS

	use BamtoolsWrapper;
	my $fh = BamtoolsWrapper->getCoverageFh(-pathBam => "file.bam", -minMQ => 10);
	while (<$fh>) {
		print $_;
	}
	$fh->close();

=head1 DESCRIPTION

Wraps bamtools functionality.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut

