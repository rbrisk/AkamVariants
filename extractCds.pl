#!/usr/bin/env perl

use strict;
use warnings;

#use Data::Dumper;
#$Data::Dumper::Sortkeys = 1;

use Getopt::Long;
use Log::Log4perl qw(:easy);
use Pod::Usage;

use Bio::SeqIO;
use IO::Uncompress::AnyUncompress;

use Bio::DB::SeqFeature::Store;
use Bio::DB::SeqFeature::Store::GFF3Loader;

my $man = 0;
my $help = 0;
my $logLvlStr = "i";
my $lineWidth = 100;
my $minLen = 1;
my ($pathAssembly, $pathAnnot, $pathOut, $onlyLongest, $woStop);

GetOptions(
		"h|help" => \$help,
		"man"    => \$man,
		"f=s"    => \$pathAssembly,
		"g=s"    => \$pathAnnot,
		"o=s"    => \$pathOut,
		"w=i"    => \$lineWidth,
		"l"      => \$onlyLongest,
		"woStop" => \$woStop,
		"minLen=i" => \$minLen,
		"v=s"    => \$logLvlStr,
	) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;

die("Assembly path is required") unless $pathAssembly;
die("Annotation path is required") unless $pathAnnot;
die("Output path is required") unless $pathOut;

my %logLvlDict = ("d" => $DEBUG, "i" => $INFO, "w" => $WARN, "e" => $ERROR, "f" => $FATAL);
$logLvlStr = "i" unless exists($logLvlDict{$logLvlStr});
Log::Log4perl->easy_init({level => $logLvlDict{$logLvlStr}, layout => "%d %p %m%n"});


sub getFileHandle {
	my ($path) = @_;
	my $cmdTempl = sprintf("| %%s -c > '%s'", $path);
	my ($cmd, $fh);
	if ($path =~ /\.bz2$/) {
		$cmd = sprintf($cmdTempl, "bzip2");
	} elsif ($path =~ /\.gz$/) {
		$cmd = sprintf($cmdTempl, "gzip");
	} elsif ($path =~ /\.xz$/) {
		$cmd = sprintf($cmdTempl, "xz");
	} elsif ($path =~ /\.zip$/) {
		$cmd = sprintf($cmdTempl, "zip");
	} else {
		$cmd = "> $path";
	}
	open($fh, $cmd);
	return $fh;
}


INFO("Loading the annotation data");
my $db = Bio::DB::SeqFeature::Store->new( -adaptor => 'memory');
my $dbLoader = Bio::DB::SeqFeature::Store::GFF3Loader->new( -store => $db );
my $fhAssembly = new IO::Uncompress::AnyUncompress($pathAssembly);
my $fhAnnot = new IO::Uncompress::AnyUncompress($pathAnnot);
$dbLoader->load($fhAssembly, $fhAnnot);
$fhAnnot->close();

# We will iterate through the FASTA file to make sure the output has the same sequence order
# Oops, AnyUncompress cannot seek backwards
# seek $fhAssembly, 0, 0;
$fhAssembly->close;
$fhAssembly = new IO::Uncompress::AnyUncompress($pathAssembly);


INFO("Extracting transcripts");
my $fhOut = getFileHandle($pathOut);
my $writer = Bio::SeqIO->new( -fh => $fhOut, -format => 'fasta' );
$writer->width($lineWidth) if $lineWidth;
my $reader = Bio::SeqIO->new( -fh => $fhAssembly, -format => 'fasta' );
while (my $seq = $reader->next_seq) {
	# Some genes start at the same position. Clearly a problem with Augustus.
	# In the unlikely event of completely matching gene boundaries, we would still have a
	# deterministic tie-break. In principle, we could just order based on primary_id but some GFF
	# files may not be sorted properly.
	my @genes = sort { 
				$a->start <=> $b->start || 
				$a->stop <=> $b->stop || 
				$a->primary_id <=> $b->primary_id 
			} $db->features( -seq_id => $seq->display_id, -type => 'gene' );
	for my $g (@genes) {
		# Some GFF files do not have Name attribute returned by display_name method. Try to use
		# load_id instead. Normally, it is populated with ID attribute. Fail miserably if it is not
		# available either.
		if ( ! $g->display_name ) {
			if ( ! $g->has_tag('load_id') ) {
				my $errMsg = "Malformed GFF document. Neither Name nor ID attribute is present. " .
						"Gene at " . $g->seq_id . " " . $g->start;
				die($errMsg);
			}
			my ($dispName) = $g->get_tag_values('load_id');
			$g->display_name($dispName);
		}
		my $geneName = $g->display_name;
		# Make sure they are in the same order as they were loaded
		# Try to get mRNAs first. If they are absent, look for transcripts
		my @mRnas;
		if (scalar($g->get_SeqFeatures('mRNA')) > 0) {
			@mRnas = sort { $a->primary_id <=> $b->primary_id } $g->get_SeqFeatures('mRNA');
		} else {
			@mRnas = sort { $a->primary_id <=> $b->primary_id } $g->get_SeqFeatures('transcript');
		}
		# Skip if it is not a gene with mRNA, e.g. miRNA should be skipped.
		next unless @mRnas > 0;
		# Reconstruct CDS for each mRNA
		my $i = 0;
		my @mRnaCds;
		for my $mRna (@mRnas) {
			my $cdsSeq = "";
			my @sortedCds = sort { $a->start <=> $b->start } $mRna->get_SeqFeatures('CDS');
			# Since seq always returns revcom for genes on the - strand, we have to reverse the order
			# for such genes.
			@sortedCds = reverse(@sortedCds) if $mRna->strand == -1;
			map { $cdsSeq .= $_->seq->seq } @sortedCds;
			# By default, mRNA's display_name will be used as display_id
			my $displayId = $mRna->display_name;
			# If it is not available, use load_id, which stores the original ID attribute value
			($displayId) = $mRna->get_tag_values('load_id') if ( ! $displayId);
			# We expect it to start with gene name followed by the transcript's ordinal number. If 
			# that's not the case, make it so
			if ($displayId !~ /^${geneName}\.\d+$/) {
				$displayId = join(".", $geneName, ++$i);
			}
			# Find the beg and end of the CDS. This is needed because start < stop for all CDS.
			my ($beg, $end);
			if ($mRna->strand == -1) {
				$beg = $sortedCds[ 0]->stop;
				$end = $sortedCds[-1]->start;
			} else {
				$beg = $sortedCds[ 0]->start;
				$end = $sortedCds[-1]->stop;
			}
			# Drop the stop codon from the end if requested and if it is present
			if ($woStop && $cdsSeq =~ s/(?:TAA|TAG|TGA)$//i) {
				$end = $mRna->strand == -1 ? $end + 3 : $end - 3;
			}
			# Description is the start and stop points of the CDS
			my $desc = sprintf("%s:%d..%d", $mRna->seq_id, $beg, $end);
			my $cdsSeqObj = Bio::Seq->new( -id => $displayId, -desc => $desc, -seq => $cdsSeq);
			push(@mRnaCds, $cdsSeqObj);
		}

		# If only the longest transcript is needed, find it and change the name to gene's name
		if ($onlyLongest) {
			@mRnaCds = sort { $b->length <=> $a->length } @mRnaCds;
			@mRnaCds = ( $mRnaCds[0] );
			$mRnaCds[0]->display_id($geneName);
		}

		map { $writer->write_seq($_) if $_->length >= $minLen } @mRnaCds;
	}
}
$writer->close();
$reader->close();

INFO("Done");


__END__

=encoding utf8

=head1 NAME

extractCds.pl - extracts CDS sequences from the genome assembly

=head1 SYNOPSIS

extractCds.pl -f assembly.fa -g annotation.gff -o cds.fa

=head1 OPTIONS

=over 8

=item B<-f>

Path to the genome assembly in FASTA format. The file may be compressed.

=item B<-g>

Path to the genome annotation in GFF format. The file may be compressed.

=item B<-o>

Path to the output file in FASTA format. The file may be compressed based on the extension.

=item B<-w>

(Optional) Line width for the fasta output file. Default: 100

=item B<-l>

(Optional) If specified, only the longest transcripts will be printed.

=item B<--woStop>

(Optional) If specified, the stop codons will be stripped from the end of each output sequence.

=item B<--minLen>

(Optional) If specified, only transcripts that are at least minLen long are extracted. Default: 1

=back

=head1 DESCRIPTION

Extracts CDS sequences from the genome assembly in FASTA format using the specified annotation in
GFF format. Genes are sorted by sequence id (following the order of the assembly FASTA file) and
position. If the B<-l> flag is specified, the script would only return the longest transcript per
gene. If the B<--woStop> flag is specified, the script will stip stop codons from the end of each 
output sequence.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut

