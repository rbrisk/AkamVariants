#!/usr/bin/env perl
use strict;
use Carp;
use Getopt::Long;
use Log::Log4perl qw(:easy);
use Pod::Usage;
#use Data::Dumper;
#$Data::Dumper::Sortkeys = 1;
use Bio::SeqIO;
use File::Path qw(make_path);
use File::Spec;
use IO::Uncompress::AnyUncompress;


our $delim = "\t";
my $man = 0;
my $help = 0;
# Genotype file name pattern
my $gtFnPtrn = "kam%s_%s.fa.xz";
# Parental reference file name pattern
my $parFnPtrn = "%s.fa.xz";
my $lineWidth = 100;

my %logLvlDict = ("d" => $DEBUG, "i" => $INFO, "w" => $WARN, "e" => $ERROR, "f" => $FATAL);
my ($parent, $pathGtList, $dirData, $dirOut, $logLvlStr);

GetOptions(
		"h|help" => \$help,
		"man"    => \$man,
		"l=s"    => \$logLvlStr,
		"par=s"  => \$parent,
		"gt=s"   => \$pathGtList,
		"data=s" => \$dirData,
		"o:s"    => \$dirOut,
		"w:i"    => \$lineWidth,
		"gtFnPtrn:s" => \$gtFnPtrn,
		"parFnPtrn:s" => \$parFnPtrn,
) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;

die("Genotype list is required") unless $pathGtList;
die("Parent name is required") unless $parent;
die("Data directory is required") unless $dirData;
die("Output directory is required") unless $dirOut;
die("Data directory '$dirData' does not exist") unless -d $dirData;

$logLvlStr = "i" unless exists($logLvlDict{$logLvlStr});
Log::Log4perl->easy_init({level => $logLvlDict{$logLvlStr}, layout => "%d %p %m%n"});


sub getFileHandle {
	my ($path) = @_;
	my %extHash = ("bz2" => "bzip2", "gz" => "gzip", "xz" => "xz", "zip" => "zip");
	(my $ext = $path) =~ s/^.+\.([^.]+)$/$1/;
	my ($cmd, $fh);
	if (defined($extHash{$ext})) {
		$cmd = sprintf("| %s -c > '%s'", $extHash{$ext}, $path);
	} else {
		$cmd = "> $path";
	}
	open($fh, $cmd) or die("Unable to write to ($path). $!");
	return $fh;
}


DEBUG("Checking the output directory");
$dirOut = File::Spec->join($dirOut, $parent);
if ( ! -d $dirOut) {
	make_path($dirOut);
}
DEBUG("Output will be in $dirOut");


INFO("Reading the list of genotypes");
my @genotypes;
open(FGT, "<", $pathGtList) or confess("Unable to read '$pathGtList'. $!");
while (<FGT>) {
	chomp;
	push(@genotypes, $_);
}
close(FGT);


INFO("Spawning a reader for each genotype");
my %gtReader;
for my $gt (@genotypes) {
	my $path = File::Spec->join($dirData, $parent, sprintf($gtFnPtrn, $gt, $parent));
	my $fh = new IO::Uncompress::AnyUncompress($path);
	$gtReader{$gt} = Bio::SeqIO->new(-fh => $fh, -format => 'fasta', -alphabet => 'dna');
}


INFO("Preparing the reference");
my $pathParent = File::Spec->join($dirData, $parent, sprintf($parFnPtrn, $parent));
my $fhParent = new IO::Uncompress::AnyUncompress($pathParent);
my $readerParent = Bio::SeqIO->new(-fh => $fhParent, -format => 'fasta', -alphabet => 'dna');


INFO("Generating alignments");
my $locusCnt = 0;
my $alignCnt = 0;
while (my $seqParent = $readerParent->next_seq()) {
	++$locusCnt;
	my $dispId = $seqParent->display_id;
	my %gtSeq;
	for my $gt (@genotypes) {
		my $gts = $gtReader{$gt}->next_seq();
		confess("$gt lacks $dispId locus.") unless $dispId eq $gts->display_id;
		$gtSeq{$gt} = $gts;
	}
	my $locusId = $seqParent->display_id;
	my $fn = sprintf("%s.fa", $locusId);
	my $pathOut = File::Spec->join($dirOut, $fn);
	my $writer = Bio::SeqIO->new(-file => ">$pathOut", -format => 'fasta', -alphabet => 'dna');
	$writer->width($lineWidth) if $lineWidth;
	for my $gt (@genotypes) {
		my $seq = $gtSeq{$gt};
		# The header line should contain only the genotype name
		$seq->desc('');
		$seq->display_id($gt);
		$writer->write_seq($seq);
	}
	$seqParent->desc('');
	$seqParent->display_id($parent);
	$writer->write_seq($seqParent);
	$writer->close();

	++$alignCnt;
}


INFO("Generated $alignCnt alignments out of $locusCnt requested loci.");


__END__

=encoding utf8

=head1 NAME

alignLoci.pl - generates fasta files with locus alignments

=head1 SYNOPSIS

alignLoci.pl --par Ahal --gt genotypes.txt --data 15_loci -o 16_lociAligned

=head1 OPTIONS

=over 8

=item B<--par>

ID of the parental species

=item B<--gt>

Path to the list of genotypes to include

=item B<--data>

Data directory that contains the loci fasta files. Parental ID will be appended automatically.

=item B<-o>

Output directory. Parental subdirectory will be appended automatically.

=item B<--gtFnPtrn>

(Optional) File name pattern of a locus fasta file. Inserts genotype and parental id. 
Default: 'kam%s_%s.fa.xz'

=item B<--parFnPtrn>

(Optional) File name pattern of the parental fasta file. Inserts parental id. Default: '%s.fa.xz'

=item B<-w>

(Optional) Line width in the output fasta file. Default: 100

=back

=head1 DESCRIPTION

The script generates fasta files that contain aligned locus sequences. Each file contains a single
locus but includes all genotypes in the order they appear in the genotype list. The parental
reference sequence is appended to the end. The file is named using the locus label retrieved from
the parental fasta file.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut

