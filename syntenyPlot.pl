#!/usr/bin/env perl

use strict;
use warnings;
use autodie;
use Data::Dumper;
$Data::Dumper::Sortkeys = 1;
use Getopt::Long;
use YAML::XS qw( LoadFile );
use Log::Log4perl qw(:easy);
use Pod::Usage;
use File::Basename;
use File::Path qw( make_path );
use File::Spec;
use POSIX;
use SVG;
use Set::Scalar;

my $delim = "\t";
my $help = 0;
my $man = 0;
my $logLvlStr = 'i';
my %defaultCfg = (
		width          => 1000,
		textPanelHeight=> 100,
		textPanelMargin=> 0,
		scaleHeight    => 25,
		scaleKbp       => 5,
		panelHeight    => 50,
		marginL        => 10,
		marginR        => 100,
		lineWidth      => 2,
		lineColour     => 'black',
		geneHeight     => 7,
		geneBorderWidth=> 0,
		geneArrowCoef  => 1,
		geneBorder     => 'black',
		geneColour     => 'black',
		geneBorderHi   => 'orange',
		geneColourHi   => 'orange',
		font           => 'Helvetica',
		fontSize       => 16,
		fontSizeUnit   => 'px',
		gapHeight      => 12,
		gapFill        => 'white',
		gapColour      => 'blue'
	);
my ($pathCfg, $pathOut);

GetOptions(
		"h|help" => \$help,
		"man"    => \$man,
		"c=s"    => \$pathCfg,
		"o=s"    => \$pathOut,
		"l=s"    => \$logLvlStr,
	) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;

my %logLvlDict = ("d" => $DEBUG, "i" => $INFO, "w" => $WARN, "e" => $ERROR, "f" => $FATAL);
$logLvlStr = "w" unless exists($logLvlDict{$logLvlStr});
Log::Log4perl->easy_init({level => $logLvlDict{$logLvlStr}, layout => "%d %p %m%n"});


sub canRead {
	my ($path, $label, $isReq) = @_;
	if ( ! defined($path) ) {
		LOGCONFESS("$label is required") unless ! $isReq;
	} else {
		LOGCONFESS("'$path' does not exist") unless -f $path;
		LOGCONFESS("'$path' is not readable") unless -r $path;
	}
	return 1;
}

sub canWrite {
	my ($path, $label, $isReq) = @_;
	if ( ! defined($path) ) {
		LOGCONFESS("$label is required") unless ! $isReq;
	} else {
		my $dir = dirname($path);
		make_path($dir) unless -d $dir;
		LOGCONFESS("Unable to write to the $label path '$path'") unless -w $dir;
	}
	return 1;
}


sub readConfig {
	my ($path) = @_;
	my $reqAttr = Set::Scalar->new(
			qw/ Label GffPath SeqId Start Stop Strand NameAttr CommonNameAttr /
		);

	my $cfg = LoadFile($path);

	# Make sure accessions are configured properly
	for my $accCfg (@{$cfg->{'accessions'}}) {
		my $accLabel = $accCfg->{'Label'};
		my $accAttr = Set::Scalar->new(keys %$accCfg);
		my $attrDiff = $reqAttr - $accAttr;
		if ( ! $attrDiff->is_empty) {
			my $errMsg = "Configuration error: accession '%s' is missing the following required attributes %s";
			LOGCONFESS(sprintf($errMsg, $accLabel, join(", ", $attrDiff->elements)));
		}
		if (defined $cfg->{'dirData'}) {
			$accCfg->{'GffPath'} = File::Spec->join($cfg->{'dirData'}, $accCfg->{'GffPath'});
		}
		canRead($accCfg->{'GffPath'}, $accLabel . "GFF file", 1);
	}

	return $cfg;
}


sub configure {
	my ($pathCfg) = @_;

	my $cfg = readConfig($pathCfg);

	# Set defaults for all values that are missing
	while (my ($k, $v) = each(%defaultCfg)) {
		$cfg->{$k} = $v unless defined $cfg->{$k};
	}

	my $accessions = $cfg->{'accessions'};
	my $panelHeight = $cfg->{'panelHeight'};
	my $geneBorderWidth = $cfg->{'geneBorderWidth'};
	my $geneHeight = $cfg->{'geneHeight'};

	my $maxLocusLen = 0;
	for my $accCfg (@$accessions) {
		my $accLocusLen = $accCfg->{"Stop"} - $accCfg->{"Start"} + 1;
		$maxLocusLen = $accLocusLen if $accLocusLen > $maxLocusLen;
	}
	
	$cfg->{'height'} = $cfg->{'textPanelHeight'} + $panelHeight * @$accessions + $cfg->{'scaleHeight'};
	$cfg->{'panelWidth'} = $cfg->{'width'} - $cfg->{'marginL'} - $cfg->{'marginR'};
	# pixels per bp
	$cfg->{'scale'} = $cfg->{'panelWidth'} / $maxLocusLen;

	# Styles
	$cfg->{'textStyle'} = { 
			font => $cfg->{'font'}, 
			'font-size' => $cfg->{'fontSize'} . $cfg->{'fontSizeUnit'},
		};
	$cfg->{'lineStyle'} = { 
			fill   => $cfg->{'lineColour'}, 
			stroke => $cfg->{'lineColour'},
		};
	$cfg->{'geneStyle'} = { 
			fill   => $cfg->{'geneColour'}, 
			stroke => $cfg->{'geneBorder'}, 
			height => $geneHeight,
		};
	$cfg->{'gapLineStyle'} = {
			fill   => $cfg->{'gapFill'},
			stroke => $cfg->{'gapFill'},
		};
	$cfg->{'gapBorderStyle'} = {
			fill   => $cfg->{'gapColour'},
			stroke => $cfg->{'gapColour'},
		};

	my %hiGenes;
	for my $hg (@{$cfg->{'hiGenes'}}) {
		$hiGenes{$hg->{'name'}} = {
				fill   => $hg->{'colour'},
				stroke => $hg->{'border'},
				height => $geneHeight,
			};
	}
	$cfg->{'hiGeneStyles'} = \%hiGenes;

	my $lineWidth = $cfg->{'lineWidth'};
	$cfg->{'lineY'} = ($panelHeight - $lineWidth) / 2;
	# Not sure why lineWidth should be subtracted, but it does not look right without it
	$cfg->{'geneY'} = ($panelHeight - $geneBorderWidth - $lineWidth - $geneHeight) / 2;
	$cfg->{'gapBorderY'} = ($panelHeight - $lineWidth - $cfg->{'gapHeight'}) / 2;
	$cfg->{'gapLineY'} = ($panelHeight - $lineWidth) / 2;
	return $cfg;
}



sub buildLabelPanel {
	my %args = @_;
	my $svg = $args{'-graph'};
	my $cfg = $args{'-cfg'};

	my $panel = $svg->tag(
			'svg', 
			x => $cfg->{'marginL'}, 
			y => 0,
			height => $cfg->{'textPanelHeight'},
			# We can use up the whole space as there is no species label on the right
			width  => $cfg->{'panelWidth'} + $cfg->{'marginR'},
		);
		#print join(", ", $panel->getAttributes()), "\n";
	return $panel;
}


sub buildScalePanel {
	my %args = @_;
	my $svg = $args{'-graph'};
	my $cfg = $args{'-cfg'};

	# This panel is at the very bottom
	my $panel = $svg->tag(
			'svg',
			x => $cfg->{'marginL'},
			y => $cfg->{'height'} - $cfg->{'scaleHeight'},
			height => $cfg->{'scaleHeight'},
			width  => $cfg->{'panelWidth'},
		);
	
	my $lineBeg = $cfg->{'textPanelMargin'};
	my $lineEnd = $lineBeg + $cfg->{'scaleKbp'} * $cfg->{'scale'} * 1000 - 1;
	my $lineY = 2;
	$panel->line(
			id => 'ScaleLine',
			x1 => $lineBeg,
			y1 => $lineY,
			x2 => $cfg->{'scaleKbp'} * $cfg->{'scale'} * 1000 - 1,
			y2 => $lineY,
			style => $cfg->{'lineStyle'},
		);

	$panel->text(
			x => $lineBeg + $cfg->{'fontSize'} / 2,
			y => $cfg->{'scaleHeight'} - 5,
			-cdata => $cfg->{'scaleKbp'} . " kbp",
			style  => $cfg->{'textStyle'},
		);

	return $panel;
}


sub buildPanel {
	my %args = @_;
	my $svg = $args{'-graph'};
	my $cfg = $args{'-cfg'};
	my $panelIdx = $args{'-panelIdx'};

	my $panel = $svg->tag(
			'svg', 
			id => join('_', 'accPanel', $panelIdx),
			x => $cfg->{'marginL'}, 
			y => $cfg->{'textPanelHeight'} + $cfg->{'panelHeight'} * ($panelIdx - 1) + 1,
			height => $cfg->{'panelHeight'},
			width  => $cfg->{'panelWidth'},
		);
		#print join(", ", $panel->getAttributes()), "\n";
	return $panel;
}



sub renderLine {
	my %args = @_;
	my $panel = $args{'-panel'};
	my $accCfg = $args{'-accCfg'};
	my $cfg = $args{'-cfg'};

	my $scaledLen = ($accCfg->{'Stop'} - $accCfg->{'Start'} + 1) * $cfg->{'scale'};
	my $lineY = $cfg->{'lineY'};
	$panel->line(
			id => join('_', 'accLine', $panel->getAttribute('id')),
			x1 => $accCfg->{'origin'},
			y1 => $lineY,
			x2 => $accCfg->{'origin'} + $scaledLen,
			y2 => $lineY,
			style => $cfg->{'lineStyle'},
		);
}



sub renderGenes {
	my %args = @_;
	my $panel = $args{'-panel'};
	my $accCfg = $args{'-accCfg'};
	my $cfg = $args{'-cfg'};

	my $pathGff = $accCfg->{'GffPath'};
	my $locusBeg = $accCfg->{'Start'};
	my $locusEnd = $accCfg->{'Stop'};
	my $locusLen = $locusEnd - $locusBeg + 1;
	# Locus orientation may differ from assembly to assembly
	my $locusStrand = $accCfg->{'Strand'};
	my $origin = $accCfg->{'origin'};
	my $scale = $cfg->{'scale'};
	my $geneY = $cfg->{'geneY'};
	my $geneHeight = $cfg->{'geneHeight'};
	my $geneArrowCoef = $cfg->{'geneArrowCoef'};
	my $geneStyle = $cfg->{'geneStyle'};
	my $hiGeneStyles = $cfg->{'hiGeneStyles'};

	#INFO("Scale $scale");
	my %genes;

	open(FIN, "<", $pathGff);
	while (<FIN>) {
		next unless /\b(mRNA|gap)\b/i;
		chomp;
		my ($seqId, $src, $type, $beg, $end, $score, $strand, $phase, $attrStr) = split("\t", $_);
		# Skip if the gene is outside of the locus
		next if ($beg < $locusBeg || $end > $locusEnd);
		my $begRel = $beg - $locusBeg;
		my $endRel = $end - $locusBeg;
		# If the locus is on the negative strand, we have to reverse everything. Note, that begRel
		# becomes endRel.
		if ($locusStrand eq '-') {
			($begRel, $endRel) = ($locusLen - $endRel, $locusLen - $begRel);
		}
		my $attr = { split(/[=;]/, $attrStr) };
		if ($type =~ /^mRNA$/i) {
			my $commonName = $attr->{ $accCfg->{'CommonNameAttr'} };
			my $name = $commonName ? $commonName : $attr->{ $accCfg->{'NameAttr'} };
			# Absolute fallback
			$name = $attr->{'ID'} unless $name;
			# Polygon with pointing end at 3' end, i.e. gene on + strand points right
			my $xLeft = $origin + $begRel * $scale;
			# The second term is essentially the width
			my $xRight = $xLeft + ($endRel - $begRel + 1) * $scale;
			my $yTop = $geneY;
			my $yBot = $geneY + $geneHeight;
			my $yMid = ($yTop + $yBot) / 2;
			my ($xMid, $xs, $ys);
			# Again, if the locus is on the negative strand, gene on the negative strand is not reversed
			# because the whole locus is reversed. However, gene on the positive strand is reversed.
			# In other words, if locus strand and gene strand are different, it should point left.
			if ($locusStrand ne $strand) {
				$xMid = $xLeft + ($geneHeight * $geneArrowCoef);
				# Make sure we don't make the model longer
				$xMid = $xRight if $xMid > $xRight;
				$xs = [$xLeft, $xMid, $xRight, $xRight, $xMid];
				$ys = [$yMid, $yTop, $yTop, $yBot, $yBot];
			} else {
				$xMid = $xRight - ($geneHeight * $geneArrowCoef);
				# Make sure we don't make the model longer
				$xMid = $xLeft if $xMid < $xLeft;
				$xs = [$xLeft, $xMid, $xRight, $xMid, $xLeft];
				$ys = [$yTop, $yTop, $yMid, $yBot, $yBot];
			}
			my $points = $panel->get_path(x => $xs, y => $ys, -type => 'polygon');
			my $gStyle = defined $hiGeneStyles->{$name} ? $hiGeneStyles->{$name} : $geneStyle;
			$panel->polygon(%$points, id => join("_", $name, $panel->getAttribute('id')), style => $gStyle);
			# x coordinate of the gene's middle point
			$genes{$name} = $origin + ($begRel + $endRel) / 2 * $scale;
			#INFO(join(", ", $attr->{'ID'}, $beg, $end - $beg, ($end - $beg + 1) * $scale));
		} elsif ($type =~ /^gap$/i) {
			my $id = join("_", $attr->{'ID'}, $panel->getAttribute('id'));
			my $lineY = $cfg->{'gapLineY'};
			my $borderY = $cfg->{'gapBorderY'};
			my $lwd = $cfg->{'lineWidth'};
			my $leftX = $origin + $begRel * $scale + $lwd;
			my $rightX = $origin + $endRel * $scale - $lwd;
			# Mask the line
			$panel->line(
					id => join('_', $id, 'line'),
					x1 => $leftX,
					y1 => $lineY,
					x2 => $rightX,
					y2 => $lineY,
					style => $cfg->{'gapLineStyle'},
				);
			$panel->line(
					id => join('_', $id, 'left'),
					x1 => $leftX,
					y1 => $borderY,
					x2 => $leftX,
					y2 => $borderY + $cfg->{'gapHeight'},
					style => $cfg->{'gapBorderStyle'},
				);
			$panel->line(
					id => join('_', $id, 'right'),
					x1 => $rightX,
					y1 => $borderY,
					x2 => $rightX,
					y2 => $borderY + $cfg->{'gapHeight'},
					style => $cfg->{'gapBorderStyle'},
				);
		}
	}
	close(FIN);

	return \%genes;
}


sub renderLabels {
	my %args = @_;
	my $panel = $args{'-panel'};
	my $cfg = $args{'-cfg'};
	my $geneCoords = $args{'-geneCoords'};

	my $y = $cfg->{'textPanelHeight'} - $cfg->{'textPanelMargin'};

	#INFO(scalar(keys %$geneCoords));
	#print Dumper($geneCoords);
	#while (my ($gene, $x) = each(%$geneCoords)) {
	my @sortedGenes = sort { $geneCoords->{$a} <=> $geneCoords->{$b} } keys(%$geneCoords);
	for my $gene (@sortedGenes) {
		my $x = $geneCoords->{$gene};
		my $xAdj = $x + $cfg->{'fontSize'} / 2;
		$panel->text(
				x => $xAdj,
				y => $y,
				-cdata => $gene,
				style => $cfg->{'textStyle'},
				transform => "rotate(-45 $xAdj,$y)",
			);
	}
}


canRead($pathCfg, "configuration file", 1);
canWrite($pathOut, "output file", 1);

my $cfg = configure($pathCfg);

my $svg = SVG->new(width => $cfg->{'width'}, height => $cfg->{'height'});
my $graph = $svg->tag(
		'svg',
		x => $cfg->{'marginL'},
		y => $cfg->{'textPanelHeight'} + 1,
		height => $cfg->{'height'} - $cfg->{'textPanelHeight'},
		width  => $cfg->{'width'} - $cfg->{'marginL'} - $cfg->{'marginR'},
	);


my $panelIdx = 0;
my @geneCoords;
for my $accCfg (@{$cfg->{'accessions'}}) {
	++$panelIdx;

	# Accession panels
	my $panel = buildPanel(
			-graph => $svg, 
			-cfg => $cfg,
			-panelIdx => $panelIdx,
		);


	# We cannot use another tag for the gene line because we need to connect genes across the panels.
	# Therefore, the x coordinates should have the same origin relative to the panel's left side.
	my $scaledLen = ($accCfg->{'Stop'} - $accCfg->{'Start'} + 1) * $cfg->{'scale'};
	$accCfg->{'origin'} = ($cfg->{'panelWidth'} - $scaledLen) / 2;

	# Line
	renderLine(-panel => $panel, -accCfg => $accCfg, -cfg => $cfg);

	# Genes (and gaps)
	my $genes = renderGenes(
			-panel => $panel,
			-accCfg => $accCfg,
			-cfg => $cfg,
		);
	push(@geneCoords, $genes);

	# Label on the right margin
	my $speciesLabelPanel = $svg->tag(
			'svg',
			x => $panel->getAttribute('x') + $panel->getAttribute('width'),
			y => $panel->getAttribute('y'),
			height => $panel->getAttribute('height'),
			width => $cfg->{'marginR'},
		);
	$speciesLabelPanel->text(
			x => $cfg->{'marginL'},
			y => ($cfg->{'panelHeight'} + 0.5 * $cfg->{'fontSize'}) / 2,
			-cdata => $accCfg->{'Label'},
			style => $cfg->{'textStyle'},
		);
}

# Gene label panel (textPanel)
my $labelPanel = buildLabelPanel(-graph => $svg, -cfg => $cfg);
renderLabels(-panel => $labelPanel, -cfg => $cfg, -geneCoords => $geneCoords[0]);

# Panel at the bottom that shows scale
my $scalePanel = buildScalePanel(-graph => $svg, -cfg => $cfg);

# Connectors
for my $i (0..($#geneCoords-1)) {
	my $genesA = $geneCoords[$i];
	my $genesB = $geneCoords[$i + 1];
	my $topMargin = $cfg->{'textPanelHeight'};
	my $yA = $topMargin + ($i + 0.5) * $cfg->{'panelHeight'} + 0.5 * $cfg->{'geneHeight'};
	my $yB = $yA + $cfg->{'panelHeight'} - $cfg->{'geneHeight'};
	for my $gA (keys %$genesA) {
		next unless defined $genesB->{$gA};
		$svg->line(
				x1 => $cfg->{'marginL'} + $genesA->{$gA}, 
				y1 => $yA, 
				x2 => $cfg->{'marginL'} + $genesB->{$gA}, 
				y2 => $yB,
				style => $cfg->{'lineStyle'},
			);
	}

}


open(FOUT, ">", $pathOut) or die("Unable to write to '$pathOut'. $!");
print FOUT $svg->xmlify;
close(FOUT);



__END__

=encoding utf8

=head1 NAME

syntenyPlot.pl - generates a synteny plot for a locus

=head1 SYNOPSIS

syntenyPlot.pl -c config.txt -o synteny.svg

=head1 OPTIONS

=over 8

=item B<-c>

Path to the configuration file. The file should have one line for the header and the following
columns: Label, GffPath, SeqId, Start, Stop, CommonNameAttr

=item B<-o>

Path to the output file (SVG)

=back

=head1 DESCRIPTION

The script generates a synteny graph of the specified loci. Each locus is displayed as a line with
genes depicted as rectangles. The loci are centered.  Genes that reside in the neighbouring panels
and  bear the same commonName attribute in the GFF file will be connected with a line. The
commonName attribute will be displayed in the top panel. It is important that the attribute was
available for the first accession in the configuration file. The accessions will appear in the same
order they occur in the configuration file.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut

