#!/usr/bin/env bash

# Uses GATK to realign reads around indels for the specified genotypes
#
# Usage:
#    04_realignIndels Ahal genotypes.txt threadN
#
# Author:
#    Roman Briskine, Universität Zürich

if [[ $# -ne 3 ]]; then
	echo "Usage:"
	echo "   05_realign Ahal genotypes.txt threadN"
	exit 1
fi

# Make sure the modules are loaded
function checkLib() {
	if [[ ! -f "$1" ]]; then
		echo "ERROR: Please load $2 module"
		exit 1
	fi
}

checkLib $gatkjar "gatk"

set -Eu

trap onExit 1 2 3 15 ERR
 
function onExit() {
	local exitStatus=${1:-$?}
	if [[ $exitStatus -ne 0 ]]; then
		echo "Error occurred while executing $0. Exit status: $exitStatus"
	fi
	exit $exitStatus
}

parent=$1
gtList=$2
threadN=$3
wd=$(pwd)
pathRef=$wd/00_input/${parent}.fa
dirBamIn=$wd/02_align
dirOut=$wd/05_realign/$parent

if [[ ! -d $dirOut ]]; then
	mkdir -p $dirOut
fi

for gt in $(cat $gtList); do
	echo "Processing $gt"
	fn=$(printf "kam%s_%s" $gt $parent)
	pathBamIn=$dirBamIn/${fn}.bam
	pathIntervals=$dirOut/${fn}.intervals
	pathBamOut=$dirOut/${fn}.bam

	java -jar $gatkjar -T RealignerTargetCreator \
			-R $pathRef \
			-I $pathBamIn \
			-nt $threadN \
			-o $pathIntervals

	java -jar $gatkjar -T IndelRealigner \
			-R $pathRef \
			-I $pathBamIn \
			-targetIntervals $pathIntervals \
			-o $pathBamOut
done


onExit
