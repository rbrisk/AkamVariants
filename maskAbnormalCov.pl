#!/usr/bin/env perl
use strict;
use Capture::Tiny qw( capture );
use Getopt::Long;
use Log::Log4perl qw(:easy);
use Pod::Usage;
#use Data::Dumper;
#$Data::Dumper::Sortkeys = 1;
use Bio::SeqIO;
use BamtoolsWrapper;
use AbnormalCovMasker;

our $delim = "\t";
my $man = 0;
my $help = 0;
my $missChar = 'N';
my %logLvlDict = ("d" => $DEBUG, "i" => $INFO, "w" => $WARN, "e" => $ERROR, "f" => $FATAL);
my $minMQ = 0;
my $minCov = 1;
my $maxCov = 1000;
my ($pathRef, $pathOut, $pathBam, $lineWidth, $logLvlStr);

GetOptions(
		"h|help" => \$help,
		"man"    => \$man,
		"l=s"    => \$logLvlStr,
		"ref=s"  => \$pathRef,
		"bam=s"  => \$pathBam,
		"o=s"    => \$pathOut,
		"cmin=i" => \$minCov,
		"cmax=i" => \$maxCov,
		"mq=i"   => \$minMQ,
		"w:i"    => \$lineWidth,
) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;

die("Reference sequence is required") unless $pathRef;
die("BAM file is required") unless $pathBam;
die("Output file is required") unless $pathOut;
my ($stdout, $stderr, $exit) = capture { system("which bamtools"); }; 
die("Please load bamtools module") unless (! $exit && $stdout);

$logLvlStr = "i" unless exists($logLvlDict{$logLvlStr});
Log::Log4perl->easy_init({level => $logLvlDict{$logLvlStr}, layout => "%d %p %m%n"});

INFO("Finding the ranges with abnormal coverage");
my $covFh = BamtoolsWrapper->getCoverageFh(-pathBam => $pathBam, -minMQ => $minMQ);
my ($seqRanges, $seqLastPos) = AbnormalCovMasker->makeMissingRanges(
		-covFh    => $covFh,
		-minCov => $minCov,
		-maxCov => $maxCov,
	);

my $reader = Bio::SeqIO->new(-file => $pathRef, -format => 'fasta');
my $writer = Bio::SeqIO->new(-file => ">$pathOut", -format => 'fasta');
if ($lineWidth) {
	$writer->width($lineWidth);
}

INFO("Masking sequences");
my $seqCnt = 0;
my $noCovCnt = 0;
while (my $seq = $reader->next_seq()) {
	++$seqCnt;
	my $seqId = $seq->display_id();
	# If the sequence has absolutely no coverage, simply output Ns
	if ( ! defined $seqRanges->{$seqId} ) {
		$seq->seq($missChar x $seq->length);
		$writer->write_seq($seq);
		++$noCovCnt;
		DEBUG("$seqId has no coverage");
		next;
	}
	# If the last seen position is less than the length, add another abnormal coverage region
	AbnormalCovMasker->fixLastRange($seq, $seqRanges->{$seqId});
	AbnormalCovMasker->addLastRange($seq, $seqRanges->{$seqId}, $seqLastPos->{$seqId});
	AbnormalCovMasker->maskSeq(-seq => $seq, -ranges => $seqRanges->{$seqId}, -char => $missChar);
	$writer->write_seq($seq);
}
$writer->close();
$reader->close();

INFO("Processed $seqCnt sequences.");
INFO("$noCovCnt sequences have no coverage.");


__END__

=encoding utf8

=head1 NAME

maskAbnormalCov.pl - masks regions with abnormal coverage

=head1 SYNOPSIS

maskAbnormalCov.pl --ref reference.fa --bam alignment.bam -o masked.fa --cmin 2 --cmax 100 --mq 20

=head1 OPTIONS

=over 8

=item B<--ref>

Reference sequence in fasta format.

=item B<--bam>

Alignment file in BAM format

=item B<-o>

Output file in fasta format.

=item B<--cmin>

(Optional) Minimum allowed coverage. Default: 1

=item B<--cmax>

(Optional) Maximum allowed coverage. Default: 1000

=item B<--mq>

(Optional) Minimum allowed mapping quality (MQ). Default: 0

=item B<-w>

(Optional) Line width in the output fasta file.

=back

=head1 DESCRIPTION

The script masks all positions with abnormal coverage. Both lower and upper coverage thresholds may
be set. It is also possible to specify the minimum mapping quality while calculating the coverage.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut


