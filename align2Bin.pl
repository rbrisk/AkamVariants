#!/usr/bin/env perl

use strict;

use Bio::SeqIO;
use Carp;
use File::Basename;
use File::Spec;
use Getopt::Long;
use Log::Log4perl qw(:easy);
use Pod::Usage;

our $delim = "\t";
my $help = 0;
my $man = 0;
my $refPos = -1;
my $keepMiss = 0;
my $keepRef = 0;
my $logLvlStr = "i";
my $alignFnPtrn = qr/^([^_.]+).+$/;
my ($pathGenes, $dirAlign, $pathOut);

GetOptions(
		"h|help"   => \$help,
		"man"      => \$man,
		"genes=s"  => \$pathGenes,
		"aln=s"    => \$dirAlign,
		"o=s"      => \$pathOut,
		"ref=i"    => \$refPos,
		"fnPtrn=s" => \$alignFnPtrn,
	   "keepRef"  => \$keepRef,
		"keepMiss" => \$keepMiss,
		"l=s"      => \$logLvlStr,
) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;

my %logLvlDict = ("d" => $DEBUG, "i" => $INFO, "w" => $WARN, "e" => $ERROR, "f" => $FATAL);
$logLvlStr = "i" unless exists($logLvlDict{$logLvlStr});
Log::Log4perl->easy_init({level => $logLvlDict{$logLvlStr}, layout => "%d %p %m%n"});

die("Gene list is required") unless $pathGenes;
die("Alignment directory is required") unless $dirAlign;
die("Output path is required") unless $pathOut;
die("Alignment directory ($dirAlign) does not exist") unless -d $dirAlign;

# Reads the gene file
sub getGeneList {
	my ($pathIn) = @_;
	my %genes;
	open(my $fh, "<", $pathIn) or confess("Unable to read the gene list ($pathIn). $!");
	while (<$fh>) {
		chomp;
		$genes{$_} = 0;
	}
	return \%genes;
}


sub getFileList {
	my ($dirAlign, $alignFnPtrn, $genes) = @_;
	my @files;
	for my $f (glob("$dirAlign/*.fa*")) {
		my ($fName, $fPath) = fileparse($f);
		(my $geneName = $fName) =~ s/$alignFnPtrn/$1/;
		if (defined $genes->{$geneName}) {
			# We found the gene
			$genes->{$geneName} = 1;
			# Add to the list of files for processing
			push(@files, $f);
		}
	}
	# Make sure we have found all requested genes
	my @missingGenes = grep { $genes->{$_} == 0 } keys(%$genes);
	if (@missingGenes > 0) {
		my $msg = "Alignments for the following genes is not available:\n";
		if (@missingGenes > 20) {
			$msg .= join(", ", @missingGenes[0..19]) . " (first 20 are shown)...";
		} else {
			$msg .= join(", ", @missingGenes);
		}
		croak($msg);
	}
	return \@files;
}


# Determines the order of the accessions from an alignment file
sub getAccessionList {
	my ($pathIn, $refPos) = @_;
	my @accs;
	open(my $fh, "<", $pathIn) or confess("Unable to read the alignment from ($pathIn). $!");
	my $reader = Bio::SeqIO->new(-fh => $fh, -format => 'fasta', -alphabet => 'dna');
	while (my $seq = $reader->next_seq()) {
		push(@accs, $seq->display_name());
	}
	return \@accs;
}


# Checks the nucleotide counts to see if the position is biallelic. The position is not considered
# biallelic if there are deletions or missing values.
sub isBiallelic {
	my ($nucCounts, $keepMiss) = @_;
	my $isBi = 0;
	my %filtNucCounts;
	# Missing data is allowed if the flag is set. So it needs to be removed. Also remove entries with
	# 0 counts if present.
	while (my ($k, $v) = each(%$nucCounts)) {
		$filtNucCounts{$k} = $v unless ($v == 0 || ($keepMiss && $k =~ /^N$/i));
	}
	# If it has two keys, it's biallelic unless it has deletions, or ambiguous data
	if (scalar(keys(%filtNucCounts)) == 2) {
		$isBi = 1;
		while (my ($k, $v) = each(%filtNucCounts)) {
			$isBi = 0 if ($k !~ /^[ACGT]$/i);
		}
	}
	return $isBi;
}


# Converts a sequence alignment to binary SNP format
sub align2Bin {
	my ($pathIn, $refName, $keepRef) = @_;

	my %seqStrs;
	open(my $fh, "<", $pathIn) or confess("Unable to read the alignment ($pathIn). $!");
	my $reader = Bio::SeqIO->new( -fh => $fh, -format => 'fasta', -alphabet => 'dna');
	while (my $seq = $reader->next_seq()) {
		$seqStrs{$seq->display_name} = [ split(//, uc($seq->seq)) ];
	}

	if ( ! defined($seqStrs{$refName}) ) {
		ERROR("Reference ($refName) was not found in ($pathIn). The file will be skipped.");
		return {};
	}
	my $refSeq = $seqStrs{$refName};
	delete $seqStrs{$refName} unless $keepRef;
	my $refLen = scalar(@$refSeq);

	# Counts of each nucleotide by position. Deletion, missing, and ambiguous values are also possible!
	my @nucCounts;
	while (my ($acc, $seq) = each(%seqStrs)) {
		my $accLen = scalar(@$seq);
		if ($accLen != $refLen) {
			confess("Invalid sequence length ($accLen instead of $refLen) for accession $acc in $pathIn");
		}
		for my $i (0..scalar(@$refSeq)) {
			$nucCounts[$i]->{ $seq->[$i] }++;
		}
	}

	# Find where biallelic positions are
	my @biallelic;
	for my $i (0..$#nucCounts) {
		push(@biallelic, $i) if isBiallelic($nucCounts[$i], $keepMiss);
	}
	DEBUG("Found " . scalar(@biallelic) . " biallelic sites in $pathIn");

	my %accBinData;
	while (my ($accName, $accSeq) = each(%seqStrs)) {
		for my $i (@biallelic) {
			my $state;
			if ($accSeq->[$i] eq $refSeq->[$i]) {
				$state = 0;
			} elsif ($accSeq->[$i] =~ /^N$/i) {
				$state = -9;
			} else {
				$state = 1;
			}
			push(@{$accBinData{$accName}}, $state);
		}
	}

	# Look at the return statement above if you plan to change the output type
	return \%accBinData;
}



INFO("Reading the gene list from $pathGenes");
my $genes = getGeneList($pathGenes);

INFO("Looking up the alignment files");
my $alignmentFiles = getFileList($dirAlign, $alignFnPtrn, $genes);

INFO("Determining accession order");
DEBUG("Accession order is based on " . $alignmentFiles->[0]);
my $accs = getAccessionList($alignmentFiles->[0]);
INFO("Found " . scalar(@$accs) . " accessions");

# Depends from which end we are counting
if ($refPos > $#$accs || $refPos < - scalar(@$accs)) {
	confess("Invalid reference position ($refPos). The alignments have only " .
			scalar(@$accs) . " records");
}
my $refName = $accs->[$refPos];
splice(@$accs, $refPos, 1) unless $keepRef;
DEBUG("Reference ($refName) was found at ($refPos)");

INFO("Converting the data");
my %accBinData;
for my $pathAlign (@$alignmentFiles) {
	my $geneBinData = align2Bin($pathAlign, $refName, $keepRef);
	next unless %$geneBinData;
	while (my ($k, $v) = each(%$geneBinData)) {
		push(@{$accBinData{$k}}, @$v);
	}
}

INFO("Found " . scalar(@{$accBinData{$accs->[0]}}) . " markers");

INFO("Printing results");
open(FOUT, ">", $pathOut) or confess("Unable to write to the output file ($pathOut). $!");
for my $a (@$accs) {
	print FOUT join($delim, $a, @{$accBinData{$a}}), "\n";
}

INFO("Done");



__END__


=encoding utf8

=head1 NAME

align2Bin.pl - Extracts SNP information from gene alignments and converts them into binary format

=head1 SYNOPSIS

align2Bin.pl -g genes.txt -a alignmentDir -o BinarySNPs.txt

=head1 OPTIONS

=over 8

=item B<-g>

List of genes to process. The file should have a single column without header.

=item B<-a>

Directory where gene alignments are present. File names should have ".fa" or ".fasta" extension.

=item B<-o>

Path to the output file (txt)

=item B<-p>

(Optional) Alignment file name pattern. Used to retrieve gene name. Default: '^([^_.]).+$'

=item B<-r>

(Optional) Position (origin zero) of the reference sequence in the alignment files. Default: -1
(last)

=item B<-k>

(Optional) If specified, the reference entry will be kept in the output.

=back

=head1 DESCRIPTION

The script extracts all SNPs from the alignment file for all genes specified in the gene list.
Biallelic positions converted into binary format whereby 0 and 1 denote the reference and variant
respectively. Positions without polymorphisms, multiallelic positions, positions with missing or
ambiguous values, and deletions are ignored. All output is combined into a signle file. In the
output file, each row corresponds to an accession. Each column corresponds to a biallelic position
in an alignment file. The output file does not contain a header. Reference sequence is not included
in the output unless C<-k> flag is specified.

The script assumes that the alignment directory has all genes from the list and file have ".fa" or
".fasta" extension. A different file name pattern may be specified with C<-p> parameter. The pattern
needs a single group that extracts the gene name. If a file is not found, the script will terminate
with an error.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut
