use strict;
use warnings;
use Carp qw(confess);

use Test::More tests => 6;

BEGIN {
	use_ok("GffFeature");
}

require_ok("GffFeature");


sub makeBasicFeature {
	my $f = GffFeature->new(
			seqId => 's1',
			src => 'src',
			type => 'mRNA',
			beg => 10,
			end => 20,
			score => '.',
			strand => 1, 
			phase => '.',
			tags => { 'ID' => 'g1.t1', 'Parent' => 'g1' },
		);
	return $f;
}

subtest "Constructor" => sub {
	plan tests => 9;
	my $f = makeBasicFeature();
	cmp_ok($f->seqId, 'eq', 's1', "Sequence Id");
	cmp_ok($f->src, 'eq', 'src', 'Source');
	cmp_ok($f->type, 'eq', 'mRNA', 'Type');
	cmp_ok($f->beg, '==', 10, 'Start position');
	cmp_ok($f->end, '==', 20, 'Stop position');
	cmp_ok($f->score, 'eq', '.', 'Score');
	cmp_ok($f->strand, '==', 1, 'Strand');
	cmp_ok($f->phase, 'eq', '.', 'Phase');
	is_deeply($f->tags, { ID => 'g1.t1', Parent => 'g1' }, 'Tag hash');
};


subtest "Tags" => sub {
	plan tests => 7;
	my $f = makeBasicFeature();
	cmp_ok($f->tagStr, 'eq', 'ID=g1.t1;Parent=g1', 'Tag string');
	cmp_ok($f->hasTag('ID'), '==', 1, 'hasTag ID');
	cmp_ok($f->hasTag('Parent'), '==', 1, 'hasTag parent');
	$f->rmTag("Parent");
	is_deeply($f->tags, { 'ID' => 'g1.t1' }, 'Remove tag');
	cmp_ok($f->tagStr, 'eq', 'ID=g1.t1', 'Remove triggers tag string update');
	$f->addTag('homolog', 'g2');
	is_deeply($f->tags, { 'ID' => 'g1.t1', 'homolog' => 'g2' }, 'Add tag');
	cmp_ok($f->tagStr, 'eq', 'ID=g1.t1;homolog=g2', 'Add triggers tag string update'); 
};


subtest "Strand string parsing" => sub {
	plan tests => 6;
	cmp_ok(GffFeature->parseStrandStr("-"),  '==', -1, "Minus");
	cmp_ok(GffFeature->parseStrandStr("+"),  '==',  1, "Plus");
	cmp_ok(GffFeature->parseStrandStr("."),  '==',  0, "Dot");
	cmp_ok(GffFeature->parseStrandStr("+4"), '==',  1, "Positive");
	cmp_ok(GffFeature->parseStrandStr("-3"), '==', -1, "Negative");
	cmp_ok(GffFeature->parseStrandStr("a"),  '==',  0, "Random value");
};


subtest "Constructor from String" => sub {
	plan tests => 10;
	my $tagStr = 'ID=g1.t1;Parent=g1';
	my $str = join("\t", 's1', 'src', 'mRNA', 10, 20, '.', '-', '.', $tagStr);
	my $f = GffFeature->fromString($str);
	cmp_ok($f->seqId, 'eq', 's1', "Sequence Id");
	cmp_ok($f->src, 'eq', 'src', 'Source');
	cmp_ok($f->type, 'eq', 'mRNA', 'Type');
	cmp_ok($f->beg, '==', 10, 'Start position');
	cmp_ok($f->end, '==', 20, 'Stop position');
	cmp_ok($f->score, 'eq', '.', 'Score');
	cmp_ok($f->strand, '==', -1, 'Strand');
	cmp_ok($f->phase, 'eq', '.', 'Phase');
	is_deeply($f->tags, { ID => 'g1.t1', Parent => 'g1' }, 'Tag hash');
	cmp_ok($f->tagStr, 'eq', $tagStr, 'Tag string');
};

