#!/usr/bin/env perl

use strict;
use Carp;
use Data::Dumper;
use File::Basename;
use File::Path;
use File::Spec;
use Getopt::Long;
use IO::Uncompress::AnyUncompress;
use Log::Log4perl qw(:easy);
use Pod::Usage;


our $delim = "\t";
my $man = 0;
my $help = 0;
my $logLvlStr = "i";
my $seqNamePtrn = "kam%1\$s_%2\$s.fa.xz";
# The context names in the header must be GFF compliant
my @header = qw/ Genotype Total Gene CDS Intron UTR Intergenic /;
my ($lblGt, $lblTotal, $lblGene, $lblCds, $lblIntron, $lblUtr, $lblInterg) = @header;
my @contexts = ($lblGene, $lblIntron, $lblCds);
my ($parent, $pathGt, $pathAnnot, $dirSeq, $pathOut);

GetOptions(
		"h|help" => \$help,
		"man"    => \$man,
		"o=s"    => \$pathOut,
		"v:s"    => \$logLvlStr,
		"parent=s" => \$parent,
		"gt=s"   => \$pathGt,
		"annot=s" => \$pathAnnot,
		"data=s" => \$dirSeq,
		"fnptrn=s" => \$seqNamePtrn,
) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;

die("Parent name is required") unless $parent;
die("List of genotypes is required") unless $pathGt;
die("Annotation file is required") unless $pathAnnot;
die("Data directory is required") unless $dirSeq;
die("Output path is required") unless $pathOut;

my %logLvlDict = ("d" => $DEBUG, "i" => $INFO, "w" => $WARN, "e" => $ERROR, "f" => $FATAL);
$logLvlStr = "i" unless exists($logLvlDict{$logLvlStr});
Log::Log4perl->easy_init({level => $logLvlDict{$logLvlStr}, layout => "%d %p %m%n"});

sub extractContext {
	my ($pathIn, $pathOut, $context) = @_;

	my $fhIn = new IO::Uncompress::AnyUncompress($pathIn);
	open(my $fhOut, '>', $pathOut) or confess("Unable to write the output to '$pathOut'. $!");

	print $fhOut join($delim, qw/ Label SeqId Start Stop Strand /), "\n";

	while (<$fhIn>) {
		chomp;
		my ($seqId, $src, $type, $start, $stop, $score, $strand, $phase, $attrStr)  = split($delim, $_);
		next unless $type =~ /^$context$/i;
		my %attr;
		map { my @pair = split(/=/, $_); $attr{$pair[0]} = $pair[1]; } split(/;/, $attrStr);
		confess("Record starting at $seqId:$start has neither ID nor NAME attribute") unless defined $attr{"ID"} || $attr{"Name"};
		my $label = defined($attr{'ID'}) ? $attr{'ID'} : $attr{'Name'};
		print $fhOut join($delim, $label, $seqId, $start, $stop, $strand), "\n";
	}

	close($fhOut);
	close($fhIn);
}

sub countN {
	my ($path) = @_;

	my $cnt = 0;
	my $fh = new IO::Uncompress::AnyUncompress($path);
	while (<$fh>) {
		next if /^>/;
		$cnt += ($_ =~ tr/Nn//);
	}
	DEBUG("   $cnt $path");
	$fh->close();
	
	return($cnt);
}

DEBUG("Open the output file for writing");
open(my $fhOut, ">", $pathOut) or die("Unable to write to the output file '$pathOut'. $!");
print $fhOut join($delim, @header), "\n";

INFO("Reading the genotype list");
my @genotypes;
open(FGT, "<", $pathGt) or die("Unable to read the genotype list '$pathGt'. $!");
while (<FGT>) {
	chomp;
	push(@genotypes, $_);
}
close(FGT);

INFO("Making context lists");
my %ctxtPaths;
my $dirTmp = File::Spec->join(dirname($pathOut), "tmp_" . time());
if (-e $dirTmp) {
	confess("Cannot create temp dir. Please wait a second until the other instance finishes");
}
File::Path->make_path($dirTmp);
for my $ctxt (@contexts) {
	DEBUG("   $ctxt");
	my $pathCtxt = File::Spec->join($dirTmp, $ctxt . ".txt");
	$ctxtPaths{$ctxt} = $pathCtxt;
	extractContext($pathAnnot, $pathCtxt, $ctxt);
}

INFO("Processing genotypes");
for my $gt (@genotypes) {
	INFO("   $gt");
	my %gtCounts = ($lblGt => $gt);
	my $pathAssembly = File::Spec->join($dirSeq, sprintf($seqNamePtrn, $gt, $parent));
	$gtCounts{$lblTotal} = countN($pathAssembly);
	for my $ctxt (@contexts) {
		DEBUG("      $ctxt");
		my $pathLoci = $ctxtPaths{$ctxt};
		my $pathGtCtxt = File::Spec->join($dirTmp, sprintf("%s_%s.fa", $gt, $ctxt));
		system("extractLoci.pl --asmb $pathAssembly --loci $pathLoci -o $pathGtCtxt -v w");
		$gtCounts{$ctxt} = countN($pathGtCtxt);
		unlink($pathGtCtxt);
	}
	$gtCounts{$lblUtr} = $gtCounts{$lblGene} - ($gtCounts{$lblCds} + $gtCounts{$lblIntron});
	$gtCounts{$lblInterg} = $gtCounts{$lblTotal} - $gtCounts{$lblGene};

	print $fhOut join($delim, @gtCounts{@header}), "\n";
}
close($fhOut);

INFO("Removing temporary files");
File::Path->remove_tree($dirTmp);

__END__

=encoding utf8

=head1 NAME

31_countNs.pl - counts the Ns in various contexts for each genotype.

=head1 SYNOPSIS

31_countNs.pl --parent Ahal --gt genotypes.txt --annot annotation.gff --data datadir -o counts.txt

=head1 OPTIONS

=over 8

=item B<-o>

Output file. Tab delimited with header and the following columns: Genotype, Total, Gene, CDS,
Intron, UTR, Intergenic.

=item B<--parent>

Parent name. Used to populate the name pattern of the input fasta file.

=item B<--gt>

Path to the file containing genotype list

=item B<--annot>

Path to the annotation file in GFF format

=item B<--data>

Path to the data directory

=item B<--fnptrn>

(Optional) File name pattern of the input. First variable is the genotype name and the second is the
parent name. Default: kam%1\$s_%2\$s.fa.xz

=back

=head1 DESCRIPTION

The script counts the number of Ns for each genotype in each context and writes the results into the
output file. The counts may be slightly off because of gene overlapping. For example, Ns encountered
in the CDS of a gene residing within UTR region of another gene will be counted twice: once for CDS
and once for UTR. The script depends on the extractContext script from srcg.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut

