#!/usr/bin/env perl

use strict;
use warnings;
use autodie;

use Carp;
use Getopt::Long;
use Pod::Usage;

use IO::Uncompress::AnyUncompress;

our $delim = "\t";
my $help = 0;
my $man = 0;
my $locusPtrn = qr/^([^:]+):(\d+)\.\.(\d+)$/;
my ($locusStr, $pathAnnot, $pathOut, $tSeqId, $tOffset, $tStrand, $tGap);

GetOptions(
		"h|help" => \$help,
		"man"    => \$man,
		"l=s"    => \$locusStr,
		"a=s"    => \$pathAnnot,
		"o=s"    => \$pathOut,
		"tSeqId=s"  => \$tSeqId,
		"tOffset=i" => \$tOffset,
		"tStrand"   => \$tStrand,
		"tGap=i"	   => \$tGap,
	) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-versbose => 2) if $man;

confess("Invalid locus location") unless $locusStr =~ $locusPtrn;
my ($locusSeqId, $locusBeg, $locusEnd) = ($1, $2, $3);
confess("Start position is after the stop position '$locusBeg > $locusEnd'.") 
		unless ($locusBeg <= $locusEnd);
confess("Offset is required when strand transformation is requested.") if ($tStrand && ! $tOffset);

my $fhIn;
if ($pathAnnot) {
	$fhIn = IO::Uncompress::AnyUncompress->new($pathAnnot);
} else {
	open($fhIn, '<', '-');
}

my @out;
while (<$fhIn>) {
	next if /^#/;
	chomp;
	my ($seqId, $src, $type, $beg, $end, $score, $strand, $phase, $attr) = split($delim, $_);
	next unless (
			$seqId eq $locusSeqId && 
			$beg >= $locusBeg && $beg <= $locusEnd &&
			$end >= $locusBeg && $end <= $locusEnd
		);
	$seqId = $tSeqId if $tSeqId;
	push(@out, [$seqId, $src, $type, $beg, $end, $score, $strand, $phase, $attr]);
}
close($fhIn);

my $gap;
if ($tGap && $tGap > 0) {
	$gap = [
			$out[0][0],
			".",
			"gap",
			$tOffset,
			$tOffset + $tGap - 1,
			".",
			".",
			".",
			"ID=gap_$tOffset"
		];
	# We cannot add it to the @out here because @out may be reversed based on $tStrand
	$tOffset += $tGap;
}

if ($tOffset) {
	for my $f (@out) {
		if ($tStrand) {
			my $beg = $f->[3];
			my $end = $f->[4];
			$f->[3] = $locusEnd - $end + $tOffset;
			$f->[4] = $locusEnd - $beg + $tOffset;
			$f->[6] = $f->[6] eq '+' ? '-' : '+';
		} else {
			$f->[3] = $f->[3] - $locusBeg + $tOffset;
			$f->[4] = $f->[4] - $locusBeg + $tOffset;
		}
	}
}

if ($tStrand) {
	my @genes;
	my %transcripts;
	my %other;
	my ($tId, $gId);
	for my $f (@out) {
		if ($f->[2] eq 'gene') {
			push(@genes, $f);
			$transcripts{$f} = [];
			$gId = $f;
		} elsif ($f->[2] eq 'mRNA' || $f->[2] eq 'transcript') {
			push(@{$transcripts{$gId}}, $f);
			$other{$f} = [];
			$tId = $f;
		} else {
			push(@{$other{$tId}}, $f);
		}
	}
	@genes = sort { $a->[3] <=> $b->[3] || $a->[4] <=> $b->[4] } @genes;
	@out = ();
	for my $g (@genes) {
		push(@out, $g);
		for my $t (@{$transcripts{$g}}) {
			push(@out, $t);
			push(@out, reverse(@{$other{$t}}));
		}
	}
}

# Add the gap if necessary
if ($tGap) {
	unshift(@out, $gap);
}


open(my $fhOut, '>' . ($pathOut || '-'));
for my $f (@out) {
	print $fhOut join($delim, @$f), "\n";
}
close($fhOut);


__END__

=encoding utf8

=head1 NAME

selectFeaturesGff.pl - extracts all features located within the specified locus.

=head1 SYNOPSIS

selectFeaturesGff.pl -l chr1:1000..2000 -a annotation.gff -o output.gff

=head1 OPTIONS

=over 8

=item B<-l>

Locus in the following format chr1:1000..2000

=item B<-a>

Annotation file in GFF format.

=item B<-o>

Output file in GFF format.

=item B<--tSeqId>

(Optional) Replaces sequence ID with the specified value. Default: '' (keep the current ID).

=item B<--tOffset>

(Optional) If specified, all positions are set relative to the locus start position and shifted by
this number of bases minus 1. For example, if locus start is 4, offset is 20, and position is 10,
the position value will be changed to 10 - 4 + 20 = 26. Thus, if you want the locus start to be
position 1, you need to set the C<--tOffset> parameter to 1. Default: 0 (all positions remain
unchanged).

=item B<--tStrand>

(Optional) If specified, the strand is changed to the opposite and the positions are modified
relative to the end of the locus. Requires C<--tOffset> to be set to a non-zero value. Genes will be
sorted by the new position, the order of transcripts within genes will be preserved, and transcript 
features will be returned in reverse order within each transcript.

=item B<--tGap>

(Optional) If specified and greater than 0, a gap feature with C<tGap> length is added at the
position C<1+tOffset>.

=back

=head1 DESCRIPTION

Extracts all features located within the specified locus and optionally transforms them. This script
does not make sure that all parental features are present. It simply selects all features whose
start and stop positions are within the specified locus and optionally applies the requested
transformations. The output can be optionally sorted by position.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut


