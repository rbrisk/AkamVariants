package GffFeature;

use namespace::autoclean;
use Moose;

has 'seqId' => (
	is  => 'rw',
	isa => 'Str',
	required => 1,
);

has 'src' => (
	is  => 'rw',
	isa => 'Str',
	default => '.',
);

has 'type' => (
	is  => 'rw',
	isa => 'Str',
	required => 1,
);

has 'beg' => (
	is  => 'rw',
	isa => 'Int',
	required => 1,
);

has 'end' => (
	is  => 'rw',
	isa => 'Int',
	required => 1,
);

has 'score' => (
	is  => 'rw',
	isa => 'Str',
	default => '.',
);

has 'strand' => (
	is  => 'rw',
	isa => 'Int',
	default => '.',
);

has 'phase' => (
	is  => 'rw',
	isa => 'Str',
	default => '.',
);

has 'tags' => ( 
	is  => 'rw',
	isa => 'HashRef',
	default => sub { {} },
);


=head2 tagStr
	Function : Returns all tags as a GFF formatted attribute string
	Returns  : GFF formatted attribute string. ID is always first; order of other tags is random.
=cut

sub tagStr {
	my $self = shift;
	my $tags = $self->tags;
	my @pairs;
	if (defined $tags->{'ID'}) {
		push(@pairs, "ID=" . $tags->{'ID'});
	}
	while (my ($k, $v) = each %$tags) {
		next if $k eq 'ID';
		push(@pairs, "$k=$v");
	}
	return join(';', @pairs);
}

=head2 parseTagStr
	Function : Parses the tag string to produce a hash of attributes
	Args     : $str - string that contains tag attributes in standard GFF format
	Returns  : Hash reference that maps attribute names to attribute values
=cut

sub parseTagStr {
	my ($self, $str) = @_;
	my %tags = split(/=|;/, $str);
	return \%tags;
}


=head2 hasTag
	Function : Returns true if tag is defined
	Args     : $tagName - name of the tag (attribute)
	Returns  : 1 if tag is defined
=cut 

sub hasTag {
	my ($self, $tagName) = @_;
	return defined($self->tags->{$tagName});
}


=head2 rmTag
	Function : Removes the specified tag
	Args     : $tagName - name of the tag (attribute)
=cut

sub rmTag {
	my ($self, $tagName) = @_;
	delete $self->tags->{$tagName} if defined $self->tags->{$tagName};
}


=head2 addTag
	Function : Adds the specified tag overwriting the current value if it exists
	Args     : $tagName - name of the tag (attribute)
			$tagValue - value of the tag
=cut

sub addTag {
	my ($self, $tagName, $tagValue) = @_;
	$self->tags->{$tagName} = $tagValue;
}


=head2 tagValue
	Function : Returns the value of the tag (attribute)
	Args     : $tagName - name of the tag (attribute)
	Returns  : Value of the tag
=cut

sub tagValue {
	my ($self, $tagName) = @_;
	return $self->tags->{$tagName};
}


=head2 fromString
	Usage    : my $f = GffFeature->fromString($gffRow)
	Function : Creates a new GffFeature object from the specified string that represents a row in GFF file
	Returns  : GffFeature
	Args     : $gffRow - String representing a row in GFF file. String should not end in EOL.
=cut

sub fromString {
	my ($self, $s) = @_;
	my ($seqId, $src, $type, $beg, $end, $score, $strandStr, $phase, $tagStr) = split(/\t/, $s);
	my $f = $self->new(
			seqId => $seqId,
			src   => $src,
			type  => $type,
			beg   => $beg,
			end   => $end,
			score => $score,
			strand => $self->parseStrandStr($strandStr),
			phase => $phase,
			tags  => $self->parseTagStr($tagStr),
		);
	return $f;
}


=head2 toString
	Function : Generate GFF string from the object
	Returns  : Object representation in GFF3 format
=cut

sub toString {
	my $self = shift;
	my $s = join("\t",
			$self->seqId,
			$self->src,
			$self->type,
			$self->beg,
			$self->end,
			$self->score,
			$self->strandStr,
			$self->phase,
			$self->tagStr,
		);
	return $s;
}


=head2 parseStrandStr
	Usage    : $f->parseStrandStr($strandStr)
	Function : Parses strand string. Positive values and '+' are converted to 1, negative values and 
			'-' to -1, and all other values become 0.
	Args     : $strandStr - string representing strand
=cut

sub parseStrandStr {
	my ($self, $str) = @_;
	my $strand;
	if ($str =~ /^-\d*$/) {
		$strand = -1;
	} elsif ($str =~ /^(\+?\d+|\+)$/) {
		$strand = 1;
	} else  {
		$strand = 0;
	}
	return $strand;
}


=head2 strandStr
	Usage    : $f->strandStr
	Function : Returns strand as a string ('+', '-', or '.')
=cut

sub strandStr {
	my ($self) = @_;
	my $s;
	if ($self->strand > 0) {
		$s = '+';
	} elsif ($self->strand < 0) {
		$s = '-';
	} else {
		$s = '.';
	}
	return $s;
}


__PACKAGE__->meta->make_immutable;

1;


__END__

=encoding utf8

=head1 NAME

GffFeature - Implementation of GFF feature object with little to none validation.

=head1 DESCRIPTION

Implementation of GFF feature object with little to none validation that might be useful for quick
and dirty processing of GFF files. Unlike the standard implementation, this class does not support
tags with multiple values. When parsing, only the last value will be retained. When updating, the
previous value will be overwritten.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut
