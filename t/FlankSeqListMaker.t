use strict;
use warnings;

use GffFeature;
use GffFeatureWithFlanks;
use Carp qw(confess);

use Test::More tests => 9;

BEGIN {
	use_ok("FlankSeqListMaker");
}

require_ok("FlankSeqListMaker");

sub mkFh {
	my $s = shift;
	open(my $fh, '<', \$s);
	return $fh;
}

sub mkFeatureFh {
	my ($features) = @_;
	my @strFeatures = map { $_->toString } @$features;
	my $s = join("\n", @strFeatures) . "\n";
	return mkFh($s);
}

sub getFastaFh {
	my $s = <<END;
>Seq1
GAGCTACGAACAATCAACGAACGTATACGCAGAGAACTATGACTAACGAACGAAGATATACGCAAATAACTACGAACAACCAACGAACGTATACGCACAG
>Seq2
AAAACTTTTTTTTTATCATATCTATAGTAGTAGTATTTTCTATTTCTAGG
>Seq3
TGATTATTTTTGGCACGTGGAGTCGTGGACAACAACACACCTCCATATTTTTTTTTGAATGTAATGTTAAATTATATTTAAAAGAAAAAATAAAACGTTT
TTACAACTTGTGCAACAGTTTGCTTAAAGAGAAAGGCCGAGAAACAAAGCTTTAAAAAATTCAAATAGAAAAAATCTTAAAGACGAGCTTTGATCCAAGT
END
	return mkFh($s);
}


sub mkFeature {
	my ($gId, $sId, $beg, $end, $strand) = @_;
	my $geneId = sprintf('g%02d', $gId);
	my $f = GffFeature->new(
			seqId => 'Seq' . $sId,
			type  => 'mRNA',
			beg   => $beg,
			end   => $end,
			strand => $strand,
			tags  => { ID => $geneId . '.t1', Parent => $geneId }
		);
	return $f;
}


sub getGffFhBounds {
	my $i = 0;
	my @features = (
			mkFeature(++$i, 1, 10, 20, 1), # Left out
			mkFeature(++$i, 1, 30, 50, 1), # In bounds
			mkFeature(++$i, 1, 80, 90, 1), # Right out
			mkFeature(++$i, 2, 21, 30, 1), # First bp beg
			mkFeature(++$i, 2, 22, 30, 1), # Last bp end
			mkFeature(++$i, 3, 30, 40, 1), # Spanned feature
			mkFeature(++$i, 3, 30, 70, 1),
			mkFeature(++$i, 3, 35, 80, 1), # Overlapping feature
			mkFeature(++$i, 3, 100, 130, 1), # Multiple features (mRNA) per gene
			mkFeature(  $i, 3, 120, 130, 1), 
			mkFeature(  $i, 3, 115, 120, 1), 
			mkFeature(++$i, 3, 150, 160, 1), # Another gene
		);
	return mkFeatureFh(\@features);
}


sub getGffFhSpanned {
	my $i = 0;
	my @features = (
			mkFeature(++$i, 1, 10, 20, 1), # Contiguous
			mkFeature(++$i, 1, 21, 30, 1),
			mkFeature(++$i, 1, 40, 45, 1), # Spanning left
			mkFeature(++$i, 1, 40, 48, 1),
			mkFeature(++$i, 1, 50, 59, 1), # Spanning internal
			mkFeature(++$i, 1, 52, 55, 1),
			mkFeature(++$i, 1, 60, 65, 1), # Spanning right
			mkFeature(++$i, 1, 63, 65, 1),
			mkFeature(++$i, 1, 70, 80, 1), # Overlapping
			mkFeature(++$i, 1, 75, 85, 1),
		);
	return mkFeatureFh(\@features);
}


# Flank-flank overlap
sub getGffFhOverlapFf {
	my $i = 0;
	my @features = (
			mkFeature(++$i, 3,   6,  10, 1), # Contiguous flanks
			mkFeature(++$i, 3,  21,  25, 1),
			mkFeature(++$i, 3,  40,  45, 1), # Contiguous genes
			mkFeature(++$i, 3,  46,  50, 1),
			mkFeature(++$i, 3,  65,  70, 1), # Overlapping flanks
			mkFeature(++$i, 3,  80,  85, 1),
			mkFeature(++$i, 3, 100, 105, 1), # Overlapping genes
			mkFeature(++$i, 3, 105, 110, 1),
			mkFeature(++$i, 3, 190, 200, 1), # Bad gene
		);
	return mkFeatureFh(\@features);
}

# Flank-cds overlap
sub getGffFhOverlapFc {
	my $i = 0;
	my @features = (
			mkFeature(++$i, 3,   6,  10, 1), # Contiguous flanks
			mkFeature(++$i, 3,  21,  25, 1),
			mkFeature(++$i, 3,  40,  44, 1), # Gene-flank overlap
			mkFeature(++$i, 3,  47,  50, 1),
			mkFeature(++$i, 3,  65,  70, 1), # Overlapping flanks
			mkFeature(++$i, 3,  80,  85, 1),
			mkFeature(++$i, 3, 100, 105, 1), # Overlapping genes
			mkFeature(++$i, 3, 105, 110, 1),
			mkFeature(++$i, 3, 190, 200, 1), # Bad gene
		);
	return mkFeatureFh(\@features);
}


sub getGffFhPrint {
	my $i = 0;
	my @features = (
			mkFeature(++$i, 3,  10, 15,  1), # Overlapping genes
			mkFeature(++$i, 3,  20, 25,  1),
			mkFeature(++$i, 3,  40, 50,  1), # Plus strand
			mkFeature(++$i, 3,  65, 75, -1), # Minus strand
		);
	return mkFeatureFh(\@features);
}


sub getGffFhPrintDistances {
	my $i = 0;
	my @features = (
			mkFeature(++$i, 3,   6,  10, 1), # Contiguous flanks
			mkFeature(++$i, 3,  21,  25, 1),
			mkFeature(++$i, 3,  40,  45, 1), # Contiguous genes
			mkFeature(++$i, 3,  46,  50, 1),
			mkFeature(++$i, 3,  65,  70, 1), # Overlapping flanks
			mkFeature(++$i, 3,  80,  85, 1),
			mkFeature(++$i, 3, 100, 105, 1), # Overlapping genes
			mkFeature(++$i, 3, 105, 110, 1),
			mkFeature(++$i, 3, 140, 160, 1), # Spanned genes
			mkFeature(++$i, 3, 145, 154, 1),
			mkFeature(++$i, 3, 190, 200, 1), # Too close to the end
		);
	return mkFeatureFh(\@features);
}

subtest 'Sequence lengths' => sub {
	plan tests => 1;
	my $x = FlankSeqListMaker->new;
	$x->loadSeqLengths(getFastaFh());
	is_deeply($x->seqLengths, { 'Seq1' => 100, 'Seq2' => 50, 'Seq3' => 200 });
};


subtest 'Feature loading' => sub {
	plan tests => 14;
	my $x = FlankSeqListMaker->new;
	$x->loadSeqLengths(getFastaFh());
	$x->loadFeatures(getGffFhBounds(), 'mRNA', 20);
	cmp_ok(keys %{$x->features}, '==', 3, 'Features hash');
	my $sf1 = $x->features->{'Seq1'};
	cmp_ok(scalar(@$sf1), '==', 3, 'Out of bounds features loaded');
	cmp_ok($sf1->[0]->flankBeg, '==', -10, 'Left flank beg');
	ok($sf1->[0]->isBad, 'Left out of bounds');
	ok(! $sf1->[1]->isBad, 'In bounds feature');
	cmp_ok($sf1->[2]->flankEnd, '==', 110, 'Right flank end');
	ok($sf1->[2]->isBad, 'Right out of bounds');

	my $sf2 = $x->features->{'Seq2'};
	ok(! $sf2->[0]->isBad, 'Left boundary case');
	ok(! $sf2->[1]->isBad, 'Right boundary case');

	my $sf3 = $x->features->{'Seq3'};
	cmp_ok(scalar(@$sf3), '==', 5, 'Overlapping features loaded');
	cmp_ok($sf3->[1]->end, '==', 40, 'Included feature follows spanning feature');
	cmp_ok($sf3->[0]->end, '==', 70, 'Order of overlapping features');
	cmp_ok($sf3->[3]->beg, '==', 100, 'Longest feature');
	cmp_ok($sf3->[4]->beg, '==', 150, 'Feature after multi-feature gene');
};
	

subtest 'Filter spanned features' => sub {
	plan tests => 16;
	my $x = FlankSeqListMaker->new;
	$x->loadSeqLengths(getFastaFh());
	$x->loadFeatures(getGffFhSpanned(), 'mRNA', 5);
	my $sf1Before = $x->features->{'Seq1'};
	cmp_ok(scalar(@$sf1Before), '==', 10, 'Spanned features loaded');

	$x->filterSpanned();
	my $sf1 = $x->features->{'Seq1'};

	cmp_ok($sf1->[0]->label, 'eq', 'g01', 'Contiguous feature 1');
	cmp_ok($sf1->[1]->label, 'eq', 'g02', 'Contiguous feature 2');

	ok(! $sf1->[0]->isBad, 'Contiguous feature 1 is good');
	ok(! $sf1->[1]->isBad, 'Contiguous feature 2 is good');

	cmp_ok($sf1->[2]->label, 'eq', 'g04', 'Spanned left align');
	cmp_ok($sf1->[3]->label, 'eq', 'g05', 'Spanned internal');
	cmp_ok($sf1->[4]->label, 'eq', 'g07', 'Spanned right align');

	ok($sf1->[2]->isBad, 'Spanning left is bad');
	ok($sf1->[3]->isBad, 'Spanning is bad');
	ok($sf1->[4]->isBad, 'Spanning right is bad');

	cmp_ok($sf1->[5]->label, 'eq', 'g09', 'Overlapping 1');
	cmp_ok($sf1->[6]->label, 'eq', 'g10', 'Overlapping 2');

	ok(! $sf1->[5]->isBad, 'Overlapping 1 is good');
	ok(! $sf1->[6]->isBad, 'Overlapping 2 is good');

	cmp_ok(scalar(@$sf1), '==', 7, 'No extra features');
};


subtest 'Mark flank-flank overlaps' => sub {
	plan tests => 12;
	my $x = FlankSeqListMaker->new;
	$x->loadSeqLengths(getFastaFh());
	$x->loadFeatures(getGffFhOverlapFf(), 'mRNA', 5);
	$x->filterSpanned();
	my $sf3Before = $x->features->{'Seq3'};
	cmp_ok(scalar(@$sf3Before), '==', 9, 'Overlapping features (ff) loaded');

	$x->markOverlapping();
	my $sf3 = $x->features->{'Seq3'};
	cmp_ok(scalar(@$sf3), '==', 9, 'Marking does not remove features');

	my @expFNames = map { sprintf("g%02d", $_) } 1..9;
	my @fNames = map { $_->label } @$sf3;
	is_deeply(\@fNames, \@expFNames, 'Sort order');

	ok(! $sf3->[0]->isBad, 'Contiguous flank 1');
	ok(! $sf3->[1]->isBad, 'Contiguous flank 2');
	ok($sf3->[2]->isBad, 'Contiguous gene 1');
	ok($sf3->[3]->isBad, 'Contiguous gene 2');
	ok($sf3->[4]->isBad, 'Overlapping flank 1');
	ok($sf3->[5]->isBad, 'Overlapping flank 2');
	ok($sf3->[6]->isBad, 'Overlapping gene 1');
	ok($sf3->[7]->isBad, 'Overlapping gene 2');
	ok($sf3->[8]->isBad, 'Bad remains bad');
};


subtest 'Mark flank-cds overlaps' => sub {
	plan tests => 12;
	my $x = FlankSeqListMaker->new;
	$x->loadSeqLengths(getFastaFh());
	$x->loadFeatures(getGffFhOverlapFc(), 'mRNA', 5);
	$x->filterSpanned();
	my $sf3Before = $x->features->{'Seq3'};
	cmp_ok(scalar(@$sf3Before), '==', 9, 'Overlapping features (fc) loaded');

	$x->markOverlapping('fc');
	my $sf3 = $x->features->{'Seq3'};
	cmp_ok(scalar(@$sf3), '==', 9, 'Marking does not remove features');

	my @expFNames = map { sprintf("g%02d", $_) } 1..9;
	my @fNames = map { $_->label } @$sf3;
	is_deeply(\@fNames, \@expFNames, 'Sort order');

	ok(! $sf3->[0]->isBad, 'Contiguous flank 1');
	ok(! $sf3->[1]->isBad, 'Contiguous flank 2');
	ok($sf3->[2]->isBad, 'Contiguous gene 1');
	ok($sf3->[3]->isBad, 'Contiguous gene 2');
	ok(! $sf3->[4]->isBad, 'Overlapping flank 1');
	ok(! $sf3->[5]->isBad, 'Overlapping flank 2');
	ok($sf3->[6]->isBad, 'Overlapping gene 1');
	ok($sf3->[7]->isBad, 'Overlapping gene 2');
	ok($sf3->[8]->isBad, 'Bad remains bad');
};


subtest 'Print' => sub {
	plan tests => 3;
	my $x = FlankSeqListMaker->new;
	$x->loadSeqLengths(getFastaFh());
	$x->loadFeatures(getGffFhPrint(), 'mRNA', 5);
	$x->filterSpanned();
	$x->markOverlapping();

	my $sf3 = $x->features->{'Seq3'};
	cmp_ok(scalar(@$sf3), '==', 4, 'Features loaded');

	my ($s5p, $s3p);
	open(my $fh5p, '>', \$s5p);
	open(my $fh3p, '>', \$s3p);
	$x->printResults($fh5p, $fh3p);
	$fh5p->close();
	$fh3p->close();

	my $header = join("\t", qw/ Label SeqId Start Stop Strand /);
	my $exp5p = join("\n",
			$header,
			join("\t", qw/ g03 Seq3 35 39 + /),
			join("\t", qw/ g04 Seq3 76 80 - /),
		) . "\n";
	my $exp3p = join("\n",
			$header,
			join("\t", qw/ g03 Seq3 51 55 + /),
			join("\t", qw/ g04 Seq3 60 64 - /),
		) . "\n";
	cmp_ok($s5p, 'eq', $exp5p, '5 prime end');
	cmp_ok($s3p, 'eq', $exp3p, '3 prime end');
};


subtest 'printDistances' => sub {
	plan tests => 12;
	my $x = FlankSeqListMaker->new;
	$x->loadSeqLengths(getFastaFh());
	$x->loadFeatures(getGffFhPrintDistances(), 'mRNA', 5);
	$x->filterSpanned();

	my $sf3 = $x->features->{'Seq3'};
	cmp_ok(scalar(@$sf3), '==', 10, 'Features loaded');

	my $resStr;
	open(my $fhRes, '>', \$resStr);
	$x->printDistances($fhRes);
	$fhRes->close();
	my @res = split("\n", $resStr);
	my $header = join("\t", qw/ Gene SeqId Strand Dist5 Dist3 Spanning /);
	my $i = 0;
	cmp_ok($res[$i++], 'eq', $header, 'Header');
	cmp_ok($res[$i++], 'eq', join("\t", qw/ g01 Seq3 +  5 10 0 /), 'First gene');
	cmp_ok($res[$i++], 'eq', join("\t", qw/ g02 Seq3 + 10 14 0 /), 'Contiguous flank 2');
	cmp_ok($res[$i++], 'eq', join("\t", qw/ g03 Seq3 + 14  0 0 /), 'Contiguous gene 1');
	cmp_ok($res[$i++], 'eq', join("\t", qw/ g04 Seq3 +  0 14 0 /), 'Contiguous gene 2');
	cmp_ok($res[$i++], 'eq', join("\t", qw/ g05 Seq3 + 14  9 0 /), 'Overlapping flank 1');
	cmp_ok($res[$i++], 'eq', join("\t", qw/ g06 Seq3 +  9 14 0 /), 'Overlapping flank 2');
	cmp_ok($res[$i++], 'eq', join("\t", qw/ g07 Seq3 + 14 -1 0 /), 'OVerlapping gene 1');
	cmp_ok($res[$i++], 'eq', join("\t", qw/ g08 Seq3 + -1 29 0 /), 'Overlapping gene 2');
	cmp_ok($res[$i++], 'eq', join("\t", qw/ g09 Seq3 + 29 29 1 /), 'Spanning gene');
	cmp_ok($res[$i++], 'eq', join("\t", qw/ g11 Seq3 + 29  0 1 /), 'Spanned gene and last gene');
};
