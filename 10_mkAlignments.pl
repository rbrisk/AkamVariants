#!/usr/bin/env perl
use strict;
use Getopt::Long;
use Log::Log4perl qw(:easy);
use Pod::Usage;
#use Data::Dumper;
#$Data::Dumper::Sortkeys = 1;
use Bio::SeqIO;
use File::Path qw(make_path);
use File::Spec;
use IO::Uncompress::AnyUncompress;


our $delim = "\t";
my $man = 0;
my $help = 0;
# Genotype file name pattern
my $gtFnPtrn = "kam%s_%s.fa.xz";
# Parental reference file name pattern
my $parFnPtrn = "%s_cds.fa";
my $dirInput = "00_input";
my $dirCds = "09_cds";
my $dirOut = "10_aligned";
my $lineWidth = 100;

my %logLvlDict = ("d" => $DEBUG, "i" => $INFO, "w" => $WARN, "e" => $ERROR, "f" => $FATAL);
my ($parent, $pathGtList, $pathRbh, $logLvlStr);

GetOptions(
		"h|help" => \$help,
		"man"    => \$man,
		"l=s"    => \$logLvlStr,
		"p=s"    => \$parent,
		"g=s"    => \$pathGtList,
		"r:s"    => \$pathRbh,
		"o:s"    => \$dirOut,
		"gtFnPtrn:s" => \$gtFnPtrn,
		"dirInput:s" => \$dirInput,
		"dirCds:s"   => \$dirCds,
		"w:i"    => \$lineWidth,
) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;

die("Genotype list is required") unless $pathGtList;
#die("Path to reciprocal best hits is required") unless $pathRbh;
die("Parent name is required") unless $parent;

$logLvlStr = "i" unless exists($logLvlDict{$logLvlStr});
Log::Log4perl->easy_init({level => $logLvlDict{$logLvlStr}, layout => "%d %p %m%n"});


sub getFileHandle {
	my ($path) = @_;
	my %extHash = ("bz2" => "bzip2", "gz" => "gzip", "xz" => "xz", "zip" => "zip");
	(my $ext = $path) =~ s/^.+\.([^.]+)$/$1/;
	my ($cmd, $fh);
	if (defined($extHash{$ext})) {
		$cmd = sprintf("| %s -c > '%s'", $extHash{$ext}, $path);
	} else {
		$cmd = "> $path";
	}
	open($fh, $cmd) or die("Unable to write to ($path). $!");
	return $fh;
}


DEBUG("Checking the output directory");
$dirOut = File::Spec->join($dirOut, $parent);
if ( ! -d $dirOut) {
	make_path($dirOut);
}
DEBUG("Output will be in $dirOut");


INFO("Reading the list of genotypes");
my @genotypes;
open(FGT, "<", $pathGtList) or die("Unable to read '$pathGtList'. $!");
while (<FGT>) {
	chomp;
	push(@genotypes, $_);
}
close(FGT);


my %rbh;
if ($pathRbh) {
	INFO("Reading the reciprocal best hits");
	open(FRBH, "<", $pathRbh) or die("Unable to read '$pathRbh'. $!");
	while (<FRBH>) {
		chomp;
		my ($parGene, $rbhGene) = split($delim, $_);
		$rbh{$parGene} = $rbhGene;
	}
	close(FRBH);
}


INFO("Spawning a reader for each genotype");
my %gtReader;
for my $gt (@genotypes) {
	my $path = File::Spec->join($dirCds, $parent, sprintf($gtFnPtrn, $gt, $parent));
	my $fh = new IO::Uncompress::AnyUncompress($path);
	$gtReader{$gt} = Bio::SeqIO->new(-fh => $fh, -format => 'fasta', -alphabet => 'dna');
}


INFO("Preparing the reference");
my $pathParent = File::Spec->join($dirInput, sprintf($parFnPtrn, $parent));
my $fhParent = new IO::Uncompress::AnyUncompress($pathParent);
my $readerParent = Bio::SeqIO->new(-fh => $fhParent, -format => 'fasta', -alphabet => 'dna');


INFO("Generating alignments");
my $geneCnt = 0;
my $alignCnt = 0;
while (my $seqParent = $readerParent->next_seq()) {
	++$geneCnt;
	my %gtSeq;
	for my $gt (@genotypes) {
		$gtSeq{$gt} = $gtReader{$gt}->next_seq();
	}
	my $parGeneId = $seqParent->display_id;
	my $fn;
	if ($pathRbh) {
		next unless defined $rbh{$parGeneId};
		$fn = sprintf("%s_%s.fa", $rbh{$parGeneId}, $parGeneId);
	} else {
		$fn = $parGeneId . ".fa";
	}
	my $pathOut = File::Spec->join($dirOut, $fn);
	my $writer = Bio::SeqIO->new(-file => ">$pathOut", -format => 'fasta', -alphabet => 'dna');
	$writer->width($lineWidth) if $lineWidth;
	for my $gt (@genotypes) {
		my $seq = $gtSeq{$gt};
		# The header line should contain only the genotype name
		$seq->desc('');
		$seq->display_id($gt);
		$writer->write_seq($seq);
	}
	$seqParent->display_id($parent);
	$writer->write_seq($seqParent);
	$writer->close();

	++$alignCnt;
}


INFO("Generated $alignCnt alignments out of $geneCnt gene records.");


__END__

=encoding utf8

=head1 NAME

10_mkAlignments.pl - generates fasta files with gene alignments

=head1 SYNOPSIS

10_mkAlignments.pl -p Ahal -g genotypes.txt -r rbh.txt

=head1 OPTIONS

=over 8

=item B<-p>

Reference sequence of the parent in fasta format.

=item B<-g>

Path to the list of genotypes to include

=item B<-r>

(Optional) Path to the list of the reciprocal best hits

=item B<-o>

(Optional) Output directory. Parental subdirectory will be created automatically. Default: '10_aligned'

=item B<--dirInput>

(Optional) Input directory of the project. Default: '00_input'

=item B<--dirCds>

(Optional) Directory with genotype fasta files containing CDS. Default: '09_cds'

=item B<--gtGnPtrn>

(Optional) File name pattern of the genotypes CDS fasta file. Default: '%s_cds.fa'

=item B<-w>

(Optional) Line width in the output fasta file.

=back

=head1 DESCRIPTION

The script generates fasta files that contain aligned gene sequences. Each file contains a single
gene but includes all genotypes in the order they appear in the genotype list. The parental
reference sequence is appended to the end. If RBH path is specified, then the file is named using
the RBH gene name and parental gene name and only genes with RBH are selected. Otherwise, the file
name is based on the parental gene name and all genes are processed.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut

