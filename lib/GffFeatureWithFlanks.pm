package GffFeatureWithFlanks;

use namespace::autoclean;
use Moose;
use GffFeature;

extends 'GffFeature';

has 'flankLeft' => (
		is   => 'rw',
		isa  => 'Int',
		default => 0,
		documentation => 'Length of the left flanking region',
	);

has 'flankRight' => (
		is   => 'rw',
		isa  => 'Int',
		default => 0,
		documentation => 'Length of the right flanking region',
	);

has 'isBad' => (
		is  => 'rw',
		isa => 'Bool',
		default => 0,
		documentation => q{Indicates that the feature's flanking sequences should not be reported},
	);


=head2 flankBeg
	Function : Determines the begining position of the flanking region. The value may be out of bounds!
	Returns  : Beginning position of the flanking region
=cut

sub flankBeg {
	my $self = shift;
	return $self->beg - $self->flankLeft;
}


=head2 flankEnd
	Function : Determines the ending position of the flanking region. The value may be out of bounds!
	Returns  : Ending position of the flanking region
=cut

sub flankEnd {
	my $self = shift;
	return $self->end + $self->flankRight;
}


=head2 flankRecord
	Function : Generates flank records
	Args     : $left - if true, the left flank is returned. Otherwise, the right flank is returned.
	Returns  : Flank record containing label, sequence id, coordinates, and strand
=cut

sub flankRecord {
	my ($self, $left) = @_;
	my $beg = $left ? $self->flankBeg : $self->end + 1;
	my $end = $left ? $self->beg - 1 : $self->flankEnd;
	return [ $self->label, $self->seqId, $beg, $end, $self->strandStr ];
}


=head2 flank5p
	Function : Generates flank record for the 5' end
	Returns  : Flank record containing label, sequence id, coordinates, and strand
=cut

sub flank5p {
	my ($self) = @_;
	return $self->flankRecord($self->strand >= 0);
}


=head2 flank3p
	Function : Generates flank record for the 3' end
	Returns  : Flank record containing label, sequence id, coordinates, and strand
=cut

sub flank3p {
	my ($self) = @_;
	return $self->flankRecord($self->strand < 0);
}


=head2 setFlankLength
	Function : Sets the length of both flanks simultaneously to the same value
	Args     : $length - flank length
=cut

sub setFlankLength {
	my ($self, $length) = @_;
	$self->flankLeft($length);
	$self->flankRight($length);
}


=head2 label
	Function : Constructs the label for the feature
	Returns  : Parent if parent exists or ID truncated at the first dot
=cut

sub label {
	my $self = shift;
	my $tags = $self->tags;
	my $label;
	if (defined $tags->{'Parent'}) {
		$label = $tags->{'Parent'};
	} else {
		($label = $tags->{'ID'}) =~ s/^([^.]+).*$/$1/;
	}
	return $label;
}


__PACKAGE__->meta->make_immutable;

1;

__END__

=encoding utf8

=head1 NAME

GffFeatureWithFlanks - extends GffFeature to include flanking regions on each side of the feature.

=head1 DESCRIPTION

This is a helper class for FlankSeqListMaker.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut
