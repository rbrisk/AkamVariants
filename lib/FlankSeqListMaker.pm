package FlankSeqListMaker;

use namespace::autoclean;
use Moose;
use Bio::SeqIO;
use Data::Dumper;
use GffFeatureWithFlanks;

# These are not declared strictly because we want to save time on validation.
has seqLengths => (
		is  => 'ro',
		isa => 'HashRef',
		default => sub { {} },
		documentation => q{Hash that maps sequence id to its length},
	);

has sequences => (
		is  => 'ro',
		isa => 'ArrayRef',
		default => sub { [] },
		documentation => q{Ordered list of sequences (to preserve the order in the output},
	);

has features => (
		is  => 'ro',
		isa => 'HashRef',
		default => sub { {} },
		documentation => q{Hash that maps sequence id to the list of GffFeature objects it contains},
	);

has overlapTypes => (
		is  => 'ro',
		isa => 'HashRef',
		default => sub { { 'ff' => 'flank-flank', 'fc' => 'flank-cds' } },
		documentation => q{Hash of supported overlap types that maps code to description},
	);


=head2 loadSeqLengths
	Function : Loads sequence lengths from fasta file
	Args     : $fh - File handle for the input file in fasta format
=cut

sub loadSeqLengths {
	my ($self, $fh) = @_;
	my $seqLengths = $self->seqLengths;
	my $sequences = $self->sequences;
	my $reader = Bio::SeqIO->new(-fh => $fh, -type => 'fasta', -alphabet => 'dna');
	while (my $seq = $reader->next_seq) {
		$seqLengths->{$seq->display_id} = $seq->length;
		push(@$sequences, $seq->display_id);
	}
}

	
=head2 loadFeatures
	Function : Loads features of the specified type from the specified file and pads them with the
			flanking length. Features on each sequence are sorted by $beg in ascending order then by
			$end in descending order, so that it was easier to identify spanned features, i.e spanned
			feature would always follow spanning feature. If the input file contains multiple features
			with the same label (e.g. multiple transcripts per gene), the function will select the
			first record and ignore the rest. The first record is usually the longest transcript.
	Args     : $fh - file handle to the input file in GFF format
			$type - type of the features to load
			$flankLength - Length of the flanking regions
=cut

sub loadFeatures {
	my ($self, $fh, $type, $flankLength) = @_;
	my $seqLengths = $self->seqLengths;
	my $features = $self->features;

	while (<$fh>) {
		next unless /\b$type\b/;
		chomp;
		my $f = GffFeatureWithFlanks->fromString($_);
		# Make sure it is indeed the right feature
		next unless $f->type eq $type;
		$f->setFlankLength($flankLength);
		# flankBeg and flankEnd might be out of bounds. Mark them and filter out later
		$f->isBad(1) if $f->flankBeg <= 0 || $f->flankEnd > $seqLengths->{$f->seqId};
		if (defined $features->{$f->seqId}) {
			# Skip if it is the same gene but another feature
			next if $f->label eq $features->{$f->seqId}[-1]->label;
		} else {
			$features->{$f->seqId} = [];
		}
		push(@{$features->{$f->seqId}}, $f);
	}

	# Sort by beg in ascending order then by end in descending order. That sorting would make it
	# easier to identify spanned features.
	while (my ($seqId, $seqFeatures) = each(%$features)) {
		my @sorted = sort { $a->beg <=> $b->beg || $b->end <=> $a->end } @$seqFeatures;
		$features->{$seqId} = \@sorted;
	}
	#print Dumper($features);
}


=head2 filterSpanned
	Function : Removes features that are entirely spanned by other features and mark spanning features as 'bad'.
=cut

sub filterSpanned {
	my ($self) = @_;
	my $features = $self->features;

	# Filter features that are entirely spanned by other features.
	while (my ($seqId, $seqFeatures) = each(%$features)) {
		my @filtered = ( $seqFeatures->[0] );
		for my $i (1..$#$seqFeatures) {
			my $prevF = $seqFeatures->[$i - 1];
			my $thisF = $seqFeatures->[$i];
			if ($thisF->beg >= $prevF->beg && $thisF->end <= $prevF->end) {
				# Bad because it spans another feature
				$filtered[-1]->isBad(1);
			} else {
				push(@filtered, $thisF);
			}
		}
		$features->{$seqId} = \@filtered;
	}
}


=head2 markOverlapping
	Function : Marks overlapping features as 'bad'
	Args     : $type - overlap type: 'ff' for flank-flank or 'fc' for flank-cds overlaps. Default: 'ff'
=cut

sub markOverlapping {
	my ($self, $type) = @_;
	if ( ! defined $type || $type !~ /^(ff|fc)$/) {
		$type = 'ff';
	}
	my $features = $self->features;
	
	while (my ($seqId, $seqFeatures) = each(%$features)) {
		for my $i (1..$#$seqFeatures) {
			my $prevF = $seqFeatures->[$i - 1];
			my $thisF = $seqFeatures->[$i];
			my ($pos1, $pos2);
			if ($type eq 'ff') {
				$pos1 = $prevF->flankEnd;
				$pos2 = $thisF->flankBeg;
			} elsif ($type eq 'fc') {
				$pos1 = $prevF->end;
				$pos2 = $thisF->flankBeg;
			}
			if ($pos2 <= $pos1) {
				$prevF->isBad(1);
				$thisF->isBad(1);
			}
		}
	}
}



=head2 printResults
	Function : Prints results to the specified file handles (5' or 3') based on the gene orientation.
	Args     : $fh5p - Output file handle for 5' flanking sequences
			$fh3p - Output file handle for 3' flanking sequences
=cut

sub printResults {
	my ($self, $fh5p, $fh3p) = @_;
	my $sequences = $self->sequences;
	my $features = $self->features;

	my $header = join("\t", qw/ Label SeqId Start Stop Strand /);
	print $fh5p "$header\n";
	print $fh3p "$header\n";

	for my $seqId (@$sequences) {
		my $seqFeatures = $features->{$seqId};
		for my $f (@$seqFeatures) {
			next if $f->isBad;
			print $fh5p join("\t", @{$f->flank5p}), "\n";
			print $fh3p join("\t", @{$f->flank3p}), "\n";
		}
	}
}


=head2 printDistances
	Function : Prints the distance between the gene and its flanking neighbours.
	Args     : $fhOut - Output file handle
=cut

sub printDistances {
	my ($self, $fhOut) = @_;
	my $sequences = $self->sequences;
	my $features = $self->features;

	my $header = join("\t", qw/ Gene SeqId Strand Dist5 Dist3 Spanning /);
	print $fhOut "$header\n";

	for my $seqId (@$sequences) {
		my $seqFeatures = $features->{$seqId};
		for my $i (0..$#$seqFeatures) {
			my $thisF = $seqFeatures->[$i];
			my ($prevDist, $nextDist) = ("NA", "NA");
			if ($i > 0) {
				my $prevF = $seqFeatures->[$i - 1];
				$prevDist = $thisF->beg - $prevF->end - 1;
			} else {
				$prevDist = $thisF->beg - 1;
			}
			if ($i < $#$seqFeatures) {
				my $nextF = $seqFeatures->[$i + 1];
				$nextDist = $nextF->beg - $thisF->end - 1;
			} else {
				my $seqLen = $self->seqLengths->{$thisF->seqId};
				$nextDist = $seqLen - $thisF->end;
			}
			my @out = ( 
					$thisF->label, 
					$thisF->seqId, 
					$thisF->strand < 0 ? '-' : '+', 
					$prevDist, 
					$nextDist,
					$thisF->isBad,
				);
			print $fhOut join("\t", @out), "\n"
		}
	}
}


__PACKAGE__->meta->make_immutable;

1;

__END__

=encoding utf8

=head1 NAME

FlankSeqListMaker - generates a list of flanking sequence that do not overlap.

=head1 SYNOPSIS

	my $fslm = FlankSeqListMaker->new();
	$fslm->loadSeqLengths($fhAssembly);
	$fslm->loadFeatures($fhGff, 'mRNA', 500);
	$fslm->filterSpanned();
	$fslm->markOverlapping();
	$fslm->printResults($fh3p, $fh5p);

=head1 DESCRIPTION

The script generates two lists of flanking sequences (5' and 3') that do not overlap. In order to be
included, the originating feature should not overlap, span, or be included into another feature. In
addition, its flanking sequences should not overlap with any other flanking sequences. If only one
of the flanking sequences overlaps with another, neither of the feature's flanking sequences is
reported.  The script makes sure that all flanking sequences have the specified length, i.e. if the
feature is too close to the sequence end resulting in a truncated flanking sequence, the feature's
flanking sequences are dropped.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut
