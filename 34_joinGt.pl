#!/usr/bin/env perl

use v5.10;

use strict;
use warnings;
use autodie;

use Getopt::Long;
use File::Basename;
use IO::Uncompress::AnyUncompress;
use Log::Log4perl qw(:easy);
use Pod::Usage;

my $help = 0;
my $man = 0;
my $logLvlStr = "i";
my $delim = "\t";
#my @types = qw/ frameshift_variant start_lost stop_gained stop_lost /;
#my $gtPtrn = qr/(?!^HIGH)_gt$/;
my $gtPtrn = qr/_gt$/;
my ($pathGtA, $pathGtB, $pathRbhA, $pathRbhB, $labelA, $labelB, $pathOut);

GetOptions(
		"h|help"   => \$help,
		"man"      => \$man,
		"a=s"      => \$pathGtA,
		"b=s"      => \$pathGtB,
		"rbha=s"   => \$pathRbhA,
		"rbhb=s"   => \$pathRbhB,
		"lbla=s"   => \$labelA,
		"lblb=s"   => \$labelB,
		"o|out=s"  => \$pathOut,
		"l|loglvl:s"  => \$logLvlStr,
) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;

my %logLvlDict = ("d" => $DEBUG, "i" => $INFO, "w" => $WARN, "e" => $ERROR, "f" => $FATAL);
$logLvlStr = "i" unless exists($logLvlDict{$logLvlStr});
Log::Log4perl->easy_init({level => $logLvlDict{$logLvlStr}, layout => "%d %p %m%n"});
my $log = get_logger();

$log->logdie("Genotype A file is required") unless $pathGtA;
$log->logdie("Genotype B file is required") unless $pathGtB;
$log->logdie("Atha RBH file for genotype A is required") unless $pathRbhA;
$log->logdie("Atha RBH file for genotype B is required") unless $pathRbhB;
$log->logdie("Output path is required") unless $pathOut;


sub readRbh {
	my ($path, $label) = @_;
	my %rbh;
	my $fh = new IO::Uncompress::AnyUncompress($path);
	while (<$fh>) {
		next if /^#/;
		chomp;
		my ($g1, $g2) = split($delim, $_);
		$rbh{$g1} = $g2;
	}
	$fh->close();
	DEBUG("Loaded " . scalar(keys(%rbh)) . " RBH for $label");
	return \%rbh;
}


sub readGt {
	my ($path, $rbh, $label) = @_;
	my %gt;
	my $fh = new IO::Uncompress::AnyUncompress($path);
	my $headerStr = <$fh>;
	chomp($headerStr);
	my @header = split($delim, $headerStr);
	while (<$fh>) {
		chomp;
		my @F = split($delim, $_);
		my $ortholog = $rbh->{$F[1]};
		next unless defined $ortholog;
		my %geneRec;
		map { $geneRec{$header[$_]} = $F[$_] } 0..$#header;
		$geneRec{"AthaOrtholog"} = $ortholog;
		$gt{$F[1]} = \%geneRec;
	}
	$fh->close();
	DEBUG("Loaded " . scalar(keys(%gt)) . " gene genotype records for $label");
	return \%gt;
}


# We want to preserve the order of genotype headers
sub findGtFields {
	my ($path, $ptrn) = @_;
	my $fh= new IO::Uncompress::AnyUncompress($path);
	my $headerStr = <$fh>;
	chomp($headerStr);
	my @F = split($delim, $headerStr);
	my @gtF = grep { /$ptrn/ } @F;
	return \@gtF;
}


INFO("Loading RBH");
my $rbhA = readRbh($pathRbhA, $labelA);
my $rbhB = readRbh($pathRbhB, $labelB);
my %rbhAthaB;
while (my ($k, $v) = each(%$rbhB)) {
	$rbhAthaB{$v} = $k;
}
my %orthologs;
while (my ($k, $v) = each(%$rbhA)) {
	my $orthoB = $rbhAthaB{$v};
	$orthologs{$k} = $orthoB if defined $orthoB;
}
DEBUG("Found " . scalar(keys(%orthologs)) . " orthologs between $labelA and $labelB");

INFO("Loading genotype data");
my $gtA = readGt($pathGtA, $rbhA, $labelA);
my $gtB = readGt($pathGtB, $rbhB, $labelB);


INFO("Merging genotype data");
my @out;
my @gtFields = @{findGtFields($pathGtA, $gtPtrn)};
# This will work as long as Null values are not allowed in genotype fields
my $gtN = length([%$gtA]->[1]{$gtFields[0]});
for my $geneA (sort keys(%$gtA)) {
	my $geneB = $orthologs{$geneA};
	next unless defined $geneB;
	my $recA = $gtA->{$geneA};
	my $recB = $gtB->{$geneB};
	next unless defined $recB;
	my %recAB = (
			"Gene_$labelA" => $geneA,
			"Gene_$labelB" => $geneB,
			"Gene_Atha"    => $rbhA->{$geneA},
			"HIGH_impact_$labelA" => $recA->{"HIGH_impact"},
			"HIGH_impact_$labelB" => $recB->{"HIGH_impact"},
		);
	# my $cumStrA = "0" x $gtN;
	# my $cumStrB = "0" x $gtN;
	for my $f (@gtFields) {
		$log->logdie(join(", ", $geneA, $geneB, $f)) unless $recA->{$f} && $recB->{$f};
		# Mutation in the same genotype in both parental genomes
		$recAB{$f} = $recA->{$f} & $recB->{$f};
		# $cumStrA |= $recA->{$f};
		# $cumStrB |= $recB->{$f};
	}
	# $recAB{"Any_high_gt"} = $cumStrA & $cumStrB;
	push(@out, \%recAB);
}

INFO("Printing results");
my $n = 0;
open(my $fhOut, '>', $pathOut);
my @fieldsOut = (
		"Gene_$labelA", "Gene_$labelB",
		"Gene_Atha",
		"HIGH_impact_$labelA", "HIGH_impact_$labelB",
		@gtFields,
	);
say $fhOut  join($delim, @fieldsOut);
for my $rec (@out) {
	say $fhOut join($delim, @$rec{@fieldsOut});
	++$n;
}
$fhOut->close();
DEBUG("Merged $n genes");

INFO("Done");


__END__


=encoding utf8

=head1 NAME

34_joinGt.pl - Merges summarised genotype files of two parents into a single file using Atha RBH.

=head1 SYNOPSIS

34_joinGt.pl -a gtA.txt -b gtB.txt -rbha rbhA.txt -rbhb rbhB.txt -lbla Ahal -lblb Alyr -o output.txt

=head1 OPTIONS

=over 8

=item B<-a>

Path to the summarised genotype file for the first parent.

=item B<-b>

Path to the summarised genotype file for the second parent.

=item B<--rbha>

Path to the RBH file for the first parent.

=item B<--rbhb>

Path to the RBH file for the second parent.

=item B<--lbla>

Label for the first parent.

=item B<--lblb>

Label for the second parent.

=item B<-o>, B<--out>

Output file.

=back

=head1 DESCRIPTION

The script joins summarised genotype files of two parents. The genotype fields are joined with the
C<AND> operator.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut


