#!/usr/bin/env perl

use strict;
use Carp;
use Capture::Tiny qw(capture);
use Data::Dumper;
use Getopt::Long;
use Log::Log4perl qw(:easy);
use Pod::Usage;

use Bio::SeqIO;
use File::Basename;
use File::Path qw(make_path);
use File::Spec;
use IO::Uncompress::AnyUncompress;

our $delim = "\t";
my $man = 0;
my $help = 0;
my %logLvlDict = ("d" => $DEBUG, "i" => $INFO, "w" => $WARN, "e" => $ERROR, "f" => $FATAL);
my $logLvlStr = "i";
my $fnPtrn = "%s.fa";
my ($pathOrtho, $pathOrthoAtha, $dirData, $prefixOut, $parentA, $parentB, $keepParent);

GetOptions(
		"h|help"  => \$help,
		"man"     => \$man,
		"l=s"     => \$logLvlStr,
		"ortho=s" => \$pathOrtho,
		"data=s"  => \$dirData,
		"pref=s"  => \$prefixOut,
		"a=s"     => \$parentA,
		"b=s"     => \$parentB,
		"atha:s"  => \$pathOrthoAtha,
		"fnPtrn:s" => \$fnPtrn,
		"keepParent" => \$keepParent,
	) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;

die("Path to the ortholog map is required") unless $pathOrtho;
die("Path to the data directory is required") unless $dirData;
die("Output prefix is required") unless $prefixOut;
die("Parent A is required") unless $parentA;
die("Parent B is required") unless $parentB;
die("Data directory '$dirData' does not exist") unless -d $dirData;
my $dirDataA = File::Spec->join($dirData, $parentA);
my $dirDataB = File::Spec->join($dirData, $parentB);
die("Parent A data directory '$dirDataA' does not exist") unless -d $dirDataA;
die("Parent B data directory '$dirDataB' does not exist") unless -d $dirDataB;

# Make sure muscle is available
my ($stdout, $stderr, $exit) = capture { system('which muscle') };
die("muscle module is not loaded") unless ($stdout && $exit == 0);


$logLvlStr = "i" unless exists($logLvlDict{$logLvlStr});
Log::Log4perl->easy_init({level => $logLvlDict{$logLvlStr}, layout => "%d %p %m%n"});


sub getFileHandle {
	my ($path) = @_;
	my %extHash = ("bz2" => "bzip2", "gz" => "gzip", "xz" => "xz", "zip" => "zip");
	(my $ext = $path) =~ s/^.+\.([^.]+)$/$1/;
	my ($cmd, $fh);
	if (defined($extHash{$ext})) {
		$cmd = sprintf("| %s -c > '%s'", $extHash{$ext}, $path);
	} else {
		$cmd = "> $path";
	}
	open($fh, $cmd) or confess("Unable to write to ($path). $!");
	return $fh;
}


sub readOrthologs {
	my ($path) = @_;
	my %ortho;
	my $fh= IO::Uncompress::AnyUncompress->new($path);
	while (<$fh>) {
		next if /^#/;
		chomp;
		my ($geneA, $geneB) = split($delim, $_);
		$ortho{$geneA} = $geneB;
	}
	return \%ortho;
}


sub makeGroupFile {
	my ($pathIn, $parent, $dirOut, $keepParent) = @_;
	my $dropParent = ! $keepParent;
	my $pathOut = File::Spec->join($dirOut, join("_", "Tmp", $parent, basename($pathIn)) );
	my $fhIn = IO::Uncompress::AnyUncompress->new($pathIn);
	my $reader = Bio::SeqIO->new(-fh => $fhIn, -format => 'fasta', -alphabet => 'dna');
	# muscle can only handle uncompressed files
	open(my $fhOut, ">", $pathOut) or confess("Unable to write to '$pathOut'. $!");
	my $writer = Bio::SeqIO->new(-fh => $fhOut, -format => 'fasta', -alphabet => 'dna');
	while (my $seq = $reader->next_seq()) {
		next if ($dropParent && $seq->display_id eq $parent);
		my $displayId = join('_', $seq->display_id, $parent);
		$seq->display_id($displayId);
		$seq->desc('');
		$writer->write_seq($seq);
	}
	return $pathOut;
}


my $dirOut = File::Spec->join($dirData, $prefixOut);
make_path($dirOut) unless -d $dirOut;
DEBUG("Output in $dirOut");

INFO("Reading ortholog map");
my $orthologs = readOrthologs($pathOrtho);
DEBUG("Loaded " . scalar(keys %$orthologs) . " orthologs");
my $orthologsAtha = {};
if ($pathOrthoAtha) {
	INFO("Reading Atha ortholog map");
	$orthologsAtha = readOrthologs($pathOrthoAtha);
}
DEBUG("Loaded ". scalar(keys %$orthologsAtha) . " Atha orthologs");


INFO("Processing genes");
my $cnt = 0;
for my $geneA (keys %$orthologs) {
	my $geneB = $orthologs->{$geneA};
	my $pathGeneA = File::Spec->join($dirDataA, sprintf($fnPtrn, $geneA));
	my $pathGeneB = File::Spec->join($dirDataB, sprintf($fnPtrn, $geneB));
	# We have to rename the entries by appending parent name. We may also have to remove the
	# parental sequence
	my $pathTmpA = makeGroupFile($pathGeneA, $parentA, $dirOut, $keepParent);
	my $pathTmpB = makeGroupFile($pathGeneB, $parentB, $dirOut, $keepParent);
	# Build output file name
	my @names = ( $geneA, $geneB);
	my $geneAtha = $orthologsAtha->{$geneA};
	push(@names, $geneAtha) if defined $geneAtha;
	my $pathOut = File::Spec->join($dirOut, join("_", @names) . ".fa");

	my $cmd = "muscle -profile -in1 $pathTmpA -in2 $pathTmpB -out $pathOut";
	DEBUG($cmd);
	my ($stdout, $stderr, $exit) = capture {
		system($cmd);
	};
	if ($exit != 0) {
		confess("Failed to align the groups. Exit status '$exit'. Error: $stderr");
	}
	if (++$cnt % 1000 == 0) {
		INFO("Processed $cnt genes");
	}
	unlink $pathTmpA;
	unlink $pathTmpB;
}

INFO("Done");


__END__

=encoding utf8

=head1 NAME

18_alignParentGroups.pl - aligns the parental alignments as groups.

=head1 SYNOPSIS

18_alignParentGroups.pl --ortho orthologMap.txt --data dataDir --pref outputPrefix -a ParentA -b parentB

=head1 OPTIONS

=over 8

=item B<--ortho>

Path to the file that maps genes in parent A to genes in parent B.

=item B<--data>

Path to the data directory. The script expects to find both parental directories within the data directory. The parental directories should match the supplied names.

=item B<--pref>

Output prefix. The directory with this name will be created in the data directory.

=item B<-a>

Label of the first parent. It should match the parent's directory in the data directory.

=item B<-b>

Label of the second parent. It should match the parent's directory in the data directory.

=item B<--keepParent>

(Optional) If specified as flag, the final alignment will include both parental genes.

=item B<--atha>

(Optional) Path to the map from the gene names of the first parent to Atha genes. If specified, the output file name will include Atha gene name whenever available.

=item B<--fnPtrn>

(Optional) File name pattern for the input files. Default: "%s.fa" where the string is the gene's name.

=back

=head1 DESCRIPTION

The script aligns parental gene alignment as groups using muscle. The correspondance between
parental genes is based on the supplied ortholog list. By default, the script will remove both
parental sequences from the input files before alignment. (The originals are not actually modified.)
Only genes specified in that list will be processed. The output file name will be "%s_%s.fa" where
strings are gene of parent A and gene of parent B. The output files will be written to the
dataDir/prefixOut directory. Optionally, a map from genes of the first parent to Atha genes may be
specified. In that case, Atha gene will be appended to the output file name.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut

