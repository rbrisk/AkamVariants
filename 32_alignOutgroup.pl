#!/usr/bin/env perl

use strict;
use warnings;
use autodie;
use Getopt::Long;
use Log::Log4perl qw(:easy);
use Pod::Usage;

use Bio::SeqIO;
use Capture::Tiny qw( capture );
use File::Path qw( make_path);
use File::Basename;
use File::Spec;
use File::Temp;
use IO::Uncompress::AnyUncompress;


our $delim = "\t";
my $alignCmdPtrn = "muscle -profile -in1 %s -in2 %s -out %s";
my $man = 0;
my $help = 0;
my $logLvlStr = 'i';
my ($dirIn, $pathOutgroup, $dirOut, $outgroupLbl, $rmRefSeqId, $pathRbhIng, $pathRbhOutg);

GetOptions(
	"h|help" => \$help,
	"man"    => \$man,
	"v=s"    => \$logLvlStr,
	"ing=s"  => \$dirIn,
	"outg=s" => \$pathOutgroup,
	"rbhing=s" => \$pathRbhIng,
	"rbhoutg=s" => \$pathRbhOutg,
	"o=s"    => \$dirOut,
	"outgLbl=s" => \$outgroupLbl,
	"rmRefSeqId:i" => \$rmRefSeqId,
) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;

die("Input directory is required") unless $dirIn;
die("FASTA file with outgroup sequences is required") unless $pathOutgroup;
die("Output directory is required") unless $dirOut;
die("Outgroup species label is required") unless $outgroupLbl;

# The id is either -1 for the last sequence or positive number for a specific sequence. It needs to
# be converted into origin 0.
--$rmRefSeqId if ($rmRefSeqId && $rmRefSeqId > 0);

my %logLvlDict = ("d" => $DEBUG, "i" => $INFO, "w" => $WARN, "e" => $ERROR, "f" => $FATAL);
$logLvlStr = "i" unless exists($logLvlDict{$logLvlStr});
Log::Log4perl->easy_init({level => $logLvlDict{$logLvlStr}, layout => "%d %p %m%n"});


# Loads RBH
sub loadRbh {
	my ($pathRbh) = @_;
	my $fh = new IO::Uncompress::AnyUncompress($pathRbh);
	my %rbh;
	while(<$fh>) {
		next if /^#/;
		chomp;
		my ($k, $v) = split($delim, $_);
		$rbh{$k} = $v;
	}
	$fh->close();
	return \%rbh;
}


# Reversed the hash (keys are swapped with values)
sub reverseHash {
	my ($hash) = @_;
	my $rev = {};
	while (my ($k, $v) = each(%$hash)) {
		$rev->{$v} = $k;
	}
	return $rev;
}


# Loads FASTA file into a hash using display id as key and Bio::Seq as value
sub loadFasta {
	my ($pathFasta) = @_;
	my $fh = new IO::Uncompress::AnyUncompress($pathFasta);
	my $reader = new Bio::SeqIO( -fh => $fh, -format => 'fasta' );
	my %out;
	while (my $seq = $reader->next_seq()) {
		# Remove description and set display name to the outgroup label
		my $geneId = $seq->display_id;
		$seq->desc('');
		$seq->display_id($outgroupLbl);
		$out{$geneId} = $seq;
	}
	$reader->close();
	return \%out;
}


INFO("Loading outgroup sequences");
my $outgroupSeqs = loadFasta($pathOutgroup);

INFO("Loading RBH");
my $rbhIng  = loadRbh($pathRbhIng);
my $rbhOutg = reverseHash( loadRbh($pathRbhOutg) );
my $geneMap = {};
while (my ($k, $v) = each(%$rbhIng)) {
	$geneMap->{$k} = $rbhOutg->{$v} if defined $rbhOutg->{$v};
}

INFO("Preparing the output directory");
make_path($dirOut) unless (-d $dirOut);

INFO("Aligning outgroup sequences");
my $cnt = 0;
for my $pathGene (glob("$dirIn/*.fa*")) {
	my ($nameIng, $p, $sfx) = fileparse($pathGene, qr/\.[^.]*/);
	next unless defined $geneMap->{$nameIng};
	my $nameOutg = $geneMap->{$nameIng};
	my $nameRbh = $rbhIng->{$nameIng};
	my $pathOut = File::Spec->join($dirOut, join('_', $nameIng, $nameOutg, $nameRbh) . $sfx);

	my $fhOutgroup = File::Temp->new(TEMPLATE => 'tempOutgroupXXXXXXX', DIR => $dirOut, SUFFIX => '.fa');
	my $writerOutgroup = new Bio::SeqIO(-fh => $fhOutgroup, -format => 'fasta', -alphabet => 'dna');
	my $outgroupSeq = $outgroupSeqs->{$nameOutg};
	# Ingroup and outgroup genes may only intersect on a very limited number of sequences. We will be
	# skipping any pairs that do not have both sequences available.
	next unless defined $outgroupSeq;
	$writerOutgroup->write_seq($outgroupSeq);

	# We have to loop twice in case we need to remove the reference
	my @ingroup;
	my $readerIngroup = new Bio::SeqIO(-file => $pathGene, -format => 'fasta', -alphabet => 'dna');
	while (my $seq = $readerIngroup->next_seq()) {
		push(@ingroup, $seq);
	}
	
	splice(@ingroup, $rmRefSeqId, 1) if $rmRefSeqId;
	
	my $fhIngroup = File::Temp->new(TEMPLATE => 'tempIngroupXXXXXXX', DIR => $dirOut, SUFFIX => '.fa');
	my $writerIngroup = new Bio::SeqIO(-fh => $fhIngroup, -format => 'fasta', -alphabet => 'dna');
	for my $seq (@ingroup) {
		$writerIngroup->write_seq($seq);
	}

	my ($alignCmdOut, $alignCmdErr, $alignCmdExit) = capture {
		system(sprintf($alignCmdPtrn, $fhIngroup->filename, $fhOutgroup->filename, $pathOut));
	};
	if ($alignCmdExit != 0) {
		ERROR("($nameIng $nameOutg $nameRbh) alignment failed with exit code $alignCmdExit");
		ERROR($alignCmdErr);
	}
	if (++$cnt % 1000 == 0) {
		INFO("Aligned $cnt genes");
	}
}

INFO "Aligned $cnt genes total";
INFO("Done");



__END__

=head1 NAME

32_alignOutgroup.pl - aligns outgroup sequence to previously aligned ingroup sequences

=head1 SYNOPSIS

alignOutgroup.pl --ing dirIngroup --outg outgroupSeqs.fa --rbhing ingroupRbh.txt --rbhoutg
outgroupRbh.txt -o dirOut --outgLbl Atha --rmRefSeqId '-1'

=head1 OPTIONS

=over 8

=item B<--ing>

Directory where aligned ingroup sequences reside (one file per gene). The file name should match the
gene name.

=item B<--outg>

Fasta file with outgroup sequences. The display id should match gene names used in the outgroup RBH
file.

=item B<--rbhoutg>

Path to the list of RBH between the outgroup and another species (the same species as ingroup RBH)

=item B<--rbhing>

Path to the list of RBH between the ingroup and another species (the same species as ougroup RBH)

=item B<-o>

Output directory. Output file names will have the following format: ingGene_outgGene_rbhGene.fa

=item B<--outgLbl>

Outgroup label to use as display_id

=item B<--rmRefSeqId>

(Optional) Origin 1 id of the reference sequence to be removed. Use -1 to remove the last sequence
or 0 to keep all sequences. Default: 0

=back

=head1 DESCRIPTION

The script aligns the corresponding outgroup sequence to each of the ingroup alignments that reside
in the input directory. The aligned sequence will be named according to the specified C<--outgLbl>
while any description will be removed. Reference sequence may be removed by specifying
C<--rmRefSeqId>. The script uses C<muscle> aligner, so its installation path should be in C<$PATH>.
The output file names will consist of ingroup gene name, outgroup gene name and the gene name from
the species used for common RBH.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut

