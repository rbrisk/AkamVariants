#!/usr/bin/env perl

use strict;
use Bio::Tools::GFF;
use Bio::SeqFeature::Generic;
use File::Spec;
use Getopt::Long;
use IO::Uncompress::AnyUncompress;
use Pod::Usage;

our $delim = "\t";
my $help = 0;
my $hNameTag = "homolog";
my $cNameTag = "common";
my $descTag = "Description";
my ($pathAnnot, $pathHomologs, $pathCNames, $pathOut, $directCMap, $woDesc, $addToMrna);

GetOptions(
		"h|help" => \$help,
		"o=s"    => \$pathOut,
		"annot=s"    => \$pathAnnot,
		"hmlg=s"     => \$pathHomologs,
		"cNames=s"   => \$pathCNames,
		"hNameTag=s" => \$hNameTag,
		"cNameTag=s" => \$cNameTag,
		"descTag=s"  => \$descTag,
		"direct"     => \$directCMap,
		"woDesc"     => \$woDesc,
		"addToMrna"  => \$addToMrna,
) or pod2usage(2);

pod2usage(1) if $help;

die("Annotation file is required") unless $pathAnnot;
die("Homolog file is required") unless $pathHomologs;
die("Output path is required") unless $pathOut;

sub getFileHandle {
	my ($path) = @_;
	my $cmdTempl = sprintf("| %%s -c > '%s'", $path);
	my ($cmd, $fh);
	if ($path =~ /\.bz2$/) {
		$cmd = sprintf($cmdTempl, "bzip2");
	} elsif ($path =~ /\.gz$/) {
		$cmd = sprintf($cmdTempl, "gzip");
	} elsif ($path =~ /\.xz$/) {
		$cmd = sprintf($cmdTempl, "xz");
	} elsif ($path =~ /\.zip$/) {
		$cmd = sprintf($cmdTempl, "zip");
	} else {
		$cmd = "> $path";
	}
	open($fh, $cmd);
	return $fh;
}


# Load homologs
my $fhHom = new IO::Uncompress::AnyUncompress($pathHomologs);
my %homologs;
while (<$fhHom>) {
	chomp;
	my @F = split($delim);
	$homologs{$F[0]} = $F[1];
}
close($fhHom);

# Load common names if necessary
my %cNames;
my %descs;
if ($pathCNames) {
	my $fhCNames = new IO::Uncompress::AnyUncompress($pathCNames);
	while (<$fhCNames>) {
		chomp;
		my @F = split($delim);
		$cNames{$F[0]} = $F[1];
		if (@F > 2 && $F[2]) {
			$descs{$F[0]} = $F[2];
		}
	}
	close($fhCNames);
}


my $fhGffAnn = new IO::Uncompress::AnyUncompress($pathAnnot);
my $fhGffOut = getFileHandle($pathOut);
my $gffAnn = Bio::Tools::GFF->new(-fh => $fhGffAnn, -gff_version => 3);
# This may or may not speed up the processing
$gffAnn->ignore_sequence(1);
my $gffOut = Bio::Tools::GFF->new(-fh => $fhGffOut, -gff_version => 3);
while (my $f = $gffAnn->next_feature()) {
	my $isMrna = $f->primary_tag =~ /^(mRNA|Transcript)$/i;
	my $isGene = $f->primary_tag =~ /^gene$/i;
	if ($isGene ||	($addToMrna && $isMrna)) {
		my $fId;
		if ($isGene) {
			($fId) = $f->get_tag_values('ID');
		} else {
			# If Parent is not there, the record is malformed.
			($fId) = $f->get_tag_values('Parent');
		}
		if ($fId) {
			my $hId = $homologs{$fId};
			if (defined $hId) {
				$f->add_tag_value($hNameTag, $hId);
			}
			my $key = $directCMap ? $fId : $hId;
			if (defined $cNames{$key}) {
				$f->add_tag_value($cNameTag, $cNames{$key});
				if ( ! $woDesc && defined $descs{$key} ) {
					$f->add_tag_value($descTag, $descs{$key});
				}
			}
		}
	}
	$gffOut->write_feature($f);
}


__END__

=head1 NAME

appendHomologTag.pl - appends homolog tags (gene id and common name) to the annotation

=head1 SYNOPSIS

appendHomologTag.pl --annot gene_annot.gff --hmlg homologs.txt --cNames commonNames.txt -o output.gff

=head1 OPTIONS

=over 8

=item B<-o>

Output GFF file. Supports compression.

=item B<--annot>

Annotation file in GFF format that lists gene models.

=item B<--hmlg>

File that lists homologous pairs. Only single mapping for a gene is allowed. If multiple mappings
are specified, the last one will be used. The file should have two columns: gene id from organism A
and gene id from organism B.

=item B<--cNames>

(Optional) File that lists gene id, common name, and description for each gene. The script assumes
that the specified gene ids come from the homologues. If the gene ids are from the species itself,
C<--direct> tag should be specified. If no value is given, the common names are not appended.

=item B<--hNameTag>

(Optional) Tag to be used for the homologue name attribute. Default: homolog

=item B<--cNameTag>

(Optional) Tag to be used for the common name attribute. Default: common

=item B<--descTag>

(Optional) Tag to be used for the description attribute. Default: Description

=item B<--direct>

(Optional) If specified, the script assumes that the gene ids in the common name file are from the
same species, i.e. the mapping is direct.

=item B<--woDesc>

(Optional) If specified, description attribute is not appended even if C<--cNames> file is
specified.

=item B<--addToMrna>

(Optional) If specified, the attributes are also appended to the transcript ("mRNA" or "Transcript")
records.

=back

=head1 DESCRIPTION

For each record in the input GFF file, the script looks up the homologue, common name, and
description. If found, it appends the gene id (homologId), common name (homologName), and optionally
description (homologDesc) to the list of attributes.  In addition, it fixes the incorrect
attributes, i.e.  attributes that were specified without tags such as "Description=unknown protein;
Has 23 Blast hits". The script retrieves gene id from ID attribute, so the GFF file should be
well-formed.

Note: Custom attributes should start with a lower case letter.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut


