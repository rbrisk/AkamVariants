use strict;
use warnings;
use Carp qw(confess);

use Test::More tests => 5;

BEGIN {
	use_ok("GffFeatureWithFlanks");
}

require_ok("GffFeatureWithFlanks");

sub makeBasicFeature {
	my $f = GffFeatureWithFlanks->new(
			seqId => 's1',
			src => 'src',
			type => 'mRNA',
			beg => 100,
			end => 200,
			score => '.',
			strand => 1, 
			phase => '.',
			tags => { ID => 'g1.t1', Parent => 'g1' },
		);
	return $f;
}

subtest "Constructor" => sub {
	plan tests => 11;
	my $f = makeBasicFeature();
	cmp_ok($f->seqId, 'eq', 's1', 'SeqId');
	cmp_ok($f->src, 'eq', 'src', 'Src');
	cmp_ok($f->type, 'eq', 'mRNA', 'Type');
	cmp_ok($f->beg, '==', 100, 'Beg');
	cmp_ok($f->end, '==', 200, 'End');
	cmp_ok($f->score, 'eq', '.', 'Score');
	cmp_ok($f->strand, '==', 1, 'Strand');
	cmp_ok($f->phase, 'eq', '.', 'Phase');
	is_deeply($f->tags, { ID => 'g1.t1', Parent => 'g1' }, 'Tag hash');
	cmp_ok($f->flankLeft, '==', 0, 'Flank left');
	cmp_ok($f->flankRight, '==', 0, 'Flank right');
};


subtest "Attributes" => sub {
	plan tests => 12;
	my $f = makeBasicFeature();
	$f->setFlankLength(20);
	cmp_ok($f->flankLeft, '==',  20, 'Flank left');
	cmp_ok($f->flankRight, '==', 20, 'Flank right');

	$f->flankLeft(10);
	cmp_ok($f->flankLeft, '==', 10, 'Flank left only');
	$f->flankRight(30);
	cmp_ok($f->flankRight, '==', 30, 'Flank right only');

	cmp_ok($f->flankBeg, '==',  90, 'Flank beg');
	cmp_ok($f->flankEnd, '==', 230, 'Flank end');
	cmp_ok($f->beg, '==', 100, 'Beg preserved');
	cmp_ok($f->end, '==', 200, 'End preserved');

	cmp_ok($f->isBad, '==', 0, 'Bad attribute');
	$f->isBad(1);
	cmp_ok($f->isBad, '==', 1, 'Bad attribute is settable');

	cmp_ok($f->label, 'eq', 'g1', 'Label');
	$f->rmTag('Parent');
	$f->addTag('ID', 'g15');
	$f->addTag('Homolog', 'g2');
	cmp_ok($f->label, 'eq', 'g15', 'Label falls back to ID');
};


subtest 'Flank records' => sub {
	plan tests => 8;
	my $f = makeBasicFeature();
	$f->setFlankLength(20);

	my $expLeftPositive  = [ 'g1', 's1',  80,  99, '+' ];
	my $expRightPositive = [ 'g1', 's1', 201, 220, '+' ];

	is_deeply($f->flankRecord(1), $expLeftPositive, 'Left positive record');
	is_deeply($f->flankRecord(0), $expRightPositive, 'Right positive record');
	is_deeply($f->flank5p, $expLeftPositive, '5p positive');
	is_deeply($f->flank3p, $expRightPositive, '3p positive');

	$f->strand(-1);
	my $expLeftNegative  = [ 'g1', 's1',  80,  99, '-' ];
	my $expRightNegative = [ 'g1', 's1', 201, 220, '-' ];

	is_deeply($f->flankRecord(1), $expLeftNegative, 'Left negative record');
	is_deeply($f->flankRecord(0), $expRightNegative, 'Right negative record');
	is_deeply($f->flank5p, $expRightNegative, '5p negative');
	is_deeply($f->flank3p, $expLeftNegative, '3p negative');
};

