package ch.uzh.shimizulab.akam.Variants

import org.broadinstitute.gatk.queue.QScript
import org.broadinstitute.gatk.queue.extensions.gatk._
import org.broadinstitute.gatk.queue.extensions.picard._
import org.broadinstitute.gatk.queue.function.JavaCommandLineFunction
import org.broadinstitute.gatk.queue.util.Logging

import htsjdk.samtools.ValidationStringency
import htsjdk.samtools.SAMFileHeader.SortOrder

import scala.util.matching._



class JointCall extends QScript with Logging {

	qs =>

	@Input(doc="The reference file in FASTA format", shortName="R")
	var pathRef: File = _

	@Input(doc="Realigned BAM file. May be specified more than once.", shortName="I")
	var bamFiles: List[File] = Nil

	@Argument(doc="Number of CPU threads to use for jobs that support -nct. Scatter jobs will get " +
			"ct / sj threads each", shortName="ct", required=false)
	var cpuThreadN: Int = 10

	@Argument(doc="Number of scatter jobs to generate", shortName="sj", required=false)
	var scatterN: Int = 10

	@Argument(doc="Maximum amount of memory to use in GB", shortName="maxMem", required=false)
	var maxMem: Int = 64

	@Argument(doc="Optional list of filter names", fullName="filterName", 
			shortName="filterName", required=false)
	var filterNames: List[String] = Nil

	@Argument(doc="Optional list of filter expressions", fullName="filterExpression",
			shortName="filter", required=false)
	var filterExpressions: List[String] = Nil

	@Argument(doc="Optional list of genotype filter names", fullName="gFilterName",
			shortName="gFilterName", required=false)
	var gFilterNames: List[String] = Nil

	@Argument(doc="Optional list of genotype filer expressions", fullName="gFilterExpression",
			shortName="gFilter", required=false)
	var gFilterExpressions: List[String] = Nil


	trait gatkCommonArgs extends CommandLineGATK {
		this.reference_sequence = qs.pathRef
	}


	def script() {
		// Call variants
		val haplotypeCaller = new HaplotypeCaller with gatkCommonArgs {
			this.isIntermediate = false
			this.scatterCount = qs.scatterN
			this.num_cpu_threads_per_data_thread = (qs.cpuThreadN / qs.scatterN).toInt
			this.memoryLimit = (qs.maxMem / qs.scatterN).toInt
			this.input_file ++= bamFiles
			this.out = "unfiltered.vcf.gz"
		}
		add(haplotypeCaller)

		// Evaluate unfiltered variants
		val evalUnfiltered = new VariantEval with gatkCommonArgs {
			this.isIntermediate = false
			this.memoryLimit = qs.maxMem
			this.eval :+= haplotypeCaller.out
			this.out = "unfiltered_report.txt"
		}
		add(evalUnfiltered)

		// Filter variants
		val variantFilter = new VariantFiltration with gatkCommonArgs {
			this.isIntermediate = false
			this.variant = haplotypeCaller.out
			this.out = "filtered.vcf.gz"
			this.filterName = qs.filterNames
			this.filterExpression = qs.filterExpressions
			this.genotypeFilterName = qs.gFilterNames
			this.genotypeFilterExpression = qs.gFilterExpressions
		}

		// Evaluate filtered variants
		val evalFiltered = new VariantEval with gatkCommonArgs {
			this.isIntermediate = false
			this.memoryLimit = qs.maxMem
			this.eval :+= variantFilter.out
			this.out = "filtered_report.txt"
		}

		// Convert to table
		val vtt = new VariantsToTable with gatkCommonArgs {
			this.isIntermediate = false
			this.variant :+= variantFilter.out
			this.out = "filtered.txt"
			this.fields ++= List("CHROM", "POS", "TYPE", "QUAL", "QD", "MQ", "FS", "MQRankSum", 
					"ReadPosRankSum")
			this.genotypeFields ++= List("AD", "GQ")
			this.allowMissingData = true
		}
		
		if (filterNames.size > 0) {
			add(variantFilter, evalFiltered, vtt)
		}
	}

}



