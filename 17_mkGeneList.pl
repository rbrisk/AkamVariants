#!/usr/bin/env perl

use strict;
use Carp;
use Data::Dumper;
use Getopt::Long;
use Log::Log4perl qw(:easy);
use Pod::Usage;


our $delim = "\t";
my $man = 0;
my $help = 0;
my ($pathIn, $pathOut);

GetOptions(
		"h|help" => \$help,
		"man"    => \$man,
		"i:s"    => \$pathIn,
		"o:s"    => \$pathOut,
) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;


open(my $fhIn,  '<' . ($pathIn  || '-')) or confess("Unable to read the input from '$pathIn'. $!");
open(my $fhOut, '>' . ($pathOut || '-')) or confess("Unable to write the output to '$pathOut'. $!");

print $fhOut join($delim, qw/ Label SeqId Start Stop Strand /), "\n";

while (<$fhIn>) {
	next unless /\tgene\t/;
	chomp;
	my ($seqId, $src, $type, $start, $stop, $score, $strand, $phase, $attrStr)  = split($delim, $_);
	my %attr;
	map { my @pair = split(/=/, $_); $attr{$pair[0]} = $pair[1]; } split(/;/, $attrStr);
	confess("Gene starting at $seqId:$start does not have ID attribute") unless defined $attr{"ID"};
	print $fhOut join($delim, $attr{"ID"}, $seqId, $start, $stop, $strand), "\n";
}

close($fhOut);
close($fhIn);


__END__

=encoding utf8

=head1 NAME

17_mkGeneList.pl - generates gene list that can be used with extractLoci.pl

=head1 SYNOPSIS

17_mkGeneList.pl -i <( xzcat annotation.gff.xz) -o genes.txt

=head1 OPTIONS

=over 8

=item B<-i>

(Optional) Input file in GFF format. Default: STDIN

=item B<-o>

(Optional) Output file. Default: STDOUT

=back

=head1 DESCRIPTION

The script extracts gene records from the specified GFF file and prints them out in the format compatible with extractLoci.pl. If the input file is not specified, the script reads from STDIN. If the output is not specified, the script writes to STDOUT.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut

