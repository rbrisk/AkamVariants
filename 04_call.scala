package ch.uzh.shimizulab.akam.Variants

import org.broadinstitute.gatk.queue.QScript
import org.broadinstitute.gatk.queue.extensions.gatk._
import org.broadinstitute.gatk.queue.extensions.picard._
import org.broadinstitute.gatk.queue.function.JavaCommandLineFunction
import org.broadinstitute.gatk.queue.util.Logging

import htsjdk.samtools.ValidationStringency
import htsjdk.samtools.SAMFileHeader.SortOrder

import scala.util.matching._



class CallVariants extends QScript with Logging {

	qs =>

	@Input(doc="The reference file in FASTA format", shortName="R")
	var pathRef: File = _

	@Input(doc="BAM file", shortName="I")
	var bamFile: File = _

	@Argument(doc="Number of data threads to use for jobs that support -nt. This does not apply to " +
			"jobs that support scattering.", shortName="t", required=false)
	var threadN: Int = 8

	@Argument(doc="Number of CPU threads to use for jobs that support -nct. Scatter jobs will get " +
			"ct / sj threads each", shortName="ct", required=false)
	var cpuThreadN: Int = 8

	@Argument(doc="Number of scatter jobs to generate", shortName="sj", required=false)
	var scatterN: Int = 8

	@Argument(doc="Maximum amount of memory to use in GB", shortName="maxMem", required=false)
	var maxMem: Int = 32

	@Argument(doc="Maximum records in RAM", shortName="maxRec", required=false)
	var maxRecordsInRam: Int = 1e+7.toInt

	@Argument(doc="Maximum file handles for MarkDuplicates", shortName="maxFh", required=false)
	var maxFileHandleN: Int = 300

	@Argument(doc="Optional list of filter names", fullName="filterName", 
			shortName="filterName", required=false)
	var filterNames: List[String] = Nil

	@Argument(doc="Optional list of filter expressions", 
			shortName="filter", required=false)
	var filterExpressions: List[String] = Nil


	trait commonArgs extends CommandLineGATK {
		this.reference_sequence = qs.pathRef
	}


	def script() {
		// Fix mates
		val fixMates = new FixMateInformation {
			this.isIntermediate = true
			this.input = qs.bamFile
			this.out = "matesFixed.bam"
			this.sortOrder = SortOrder.coordinate
			this.createIndex = Some(true)
			this.validationStringency = ValidationStringency.LENIENT
			this.maxRecordsInRam = qs.maxRecordsInRam
			this.javaMemoryLimit = qs.maxMem
		}
		add(fixMates)

		// Mark duplicates
		val markDups = new MarkDuplicates {
			this.isIntermediate = true
			this.input :+= fixMates.out
			this.output = "dupMarked.bam"
			// Previously, it tried to get output value before it was frozen. 
			// Not sure if it is still true.
			this.metrics = "dupMarked.metrics"
			this.REMOVE_DUPLICATES = true
			this.MAX_FILE_HANDLES_FOR_READ_ENDS_MAP = qs.maxFileHandleN
			this.javaMemoryLimit = qs.maxMem
		}
		add(markDups)

		// Create target intervals and realign
		val rtc = new RealignerTargetCreator with commonArgs {
			this.isIntermediate = true
			this.num_threads = qs.threadN
			this.memoryLimit = qs.maxMem
			this.input_file :+= markDups.output
			this.out = "realigned.intervals"
		}

		val realigner = new IndelRealigner with commonArgs {
			this.isIntermediate = true
			this.scatterCount = qs.scatterN
			this.num_cpu_threads_per_data_thread = 1
			this.num_threads = 1
			this.memoryLimit = (qs.maxMem / qs.scatterN).toInt
			this.input_file :+= markDups.output
			this.targetIntervals = rtc.out
			this.out = "realigned.bam"
		}
		add(rtc, realigner)

		// Call variants
		val haplotypeCaller = new HaplotypeCaller with commonArgs {
			this.isIntermediate = false
			this.scatterCount = qs.scatterN
			this.num_cpu_threads_per_data_thread = (qs.cpuThreadN / qs.scatterN).toInt
			this.memoryLimit = (qs.maxMem / qs.scatterN).toInt
			this.input_file :+= realigner.out
			this.out = "raw.vcf.gz"
		}
		add(haplotypeCaller)

		// Evaluate unfiltered variants
		val evalUnfiltered = new VariantEval with commonArgs {
			this.isIntermediate = false
			this.memoryLimit = qs.maxMem
			this.eval :+= haplotypeCaller.out
			this.out = "raw_report.txt"
		}
		add(evalUnfiltered)

		// Filter variants
		val variantFilter = new VariantFiltration with commonArgs {
			this.isIntermediate = false
			this.variant = haplotypeCaller.out
			this.out = "filtered.vcf.gz"
			this.filterName = qs.filterNames
			this.filterExpression = qs.filterExpressions
		}

		// Evaluate filtered variants
		val evalFiltered = new VariantEval with commonArgs {
			this.isIntermediate = false
			this.memoryLimit = qs.maxMem
			this.eval :+= variantFilter.out
			this.out = "filtered_report.txt"
		}
		if (filterNames.size > 0) {
			add(variantFilter, evalFiltered)
		}

		// Convert to table
		val vtt = new VariantsToTable with commonArgs {
			this.isIntermediate = false
			this.fields ++= List("CHROM", "POS", "TYPE", "QUAL", "QD", "MQ", "FS", "SOR", "MQRankSum", 
					"ReadPosRankSum")
			this.genotypeFields ++= List("AD", "GQ")
			this.allowMissingData = true
		}
		if (filterNames.size > 0) {
			vtt.out = "filtered.txt"
			vtt.variant :+= variantFilter.out
		} else {
			vtt.out = "raw.txt"
			vtt.variant :+= haplotypeCaller.out
		}
		add(vtt)
	}


	class FixMateInformation extends JavaCommandLineFunction with PicardBamFunction  {
		analysisName = "FixMateInformation"
		javaMainClass = "picard.sam.FixMateInformation"

		@Input(doc = "The input SAM or BAM file to analyze. Must be coordinate sorted.", 
				required = true)
		var input: File = _

		@Output(doc="The output file to write fixed records to", required = true)
		var out: File = _

		@Output(doc="The output bam index", required = false)
		var outIndex: File = _

		@Argument(doc="If true, Adds the mate CIGAR tag (MC)", required = false)
		var addMateCigar: Boolean = false

		override def freezeFieldValues() {
			super.freezeFieldValues()
			if (outIndex == null && out != null) {
				outIndex = new File(out.getName.stripSuffix(".bam") + ".bai")
			}
		}

		override def inputBams = Seq(input)
		override def outputBam = out
		this.createIndex = Some(true)

		override def commandLine = super.commandLine +
				conditional(addMateCigar, "ADD_MATE_CIGAR=true")
	}



}



