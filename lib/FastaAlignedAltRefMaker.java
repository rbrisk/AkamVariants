import org.broadinstitute.gatk.tools.walkers.fasta.*;
import org.broadinstitute.gatk.utils.commandline.Argument;
import org.broadinstitute.gatk.utils.commandline.ArgumentCollection;
import org.broadinstitute.gatk.engine.CommandLineGATK;
import org.broadinstitute.gatk.engine.arguments.StandardVariantContextInputArgumentCollection;
import org.broadinstitute.gatk.utils.contexts.AlignmentContext;
import org.broadinstitute.gatk.utils.contexts.ReferenceContext;
import org.broadinstitute.gatk.utils.refdata.RefMetaDataTracker;
import org.broadinstitute.gatk.engine.walkers.*;
import org.broadinstitute.gatk.utils.GenomeLoc;
import org.broadinstitute.gatk.engine.SampleUtils;
import org.broadinstitute.gatk.utils.collections.Pair;
import org.broadinstitute.gatk.utils.exceptions.UserException;
import org.broadinstitute.gatk.utils.help.DocumentedGATKFeature;
import org.broadinstitute.gatk.utils.help.HelpConstants;
import htsjdk.variant.variantcontext.Genotype;
import htsjdk.variant.variantcontext.VariantContext;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * Applies SNPs and deletions to a FASTA reference sequence to generate a perfectly alignable alternate reference
 * sequence.
 *
 * <p>This tool is similar to FastaAlternateReferenceMaker except it ignores the insertions, replaces deleted regions
 * with dashes, and masks heterozygous loci with Ns. Sample (genotype) name should be specified with -sName argument.
 * The output sequences will have exactly the same names as the reference sequences.</p>
 *
 * <h3>Input</h3>
 * <p>
 * The reference, variant ROD file, sample (genotype) name, and optionally intervals.
 * </p>
 *
 * <h3>Output</h3>
 * <p>
 * A fasta file that contains alternate reference sequences that can be perfectly aligned to the original reference
 * sequence.
 * </p>
 *
 * <h3>Usage example</h3>
 * <pre>
 * java -jar GenomeAnalysisTK.jar \
 *   -T FastaAlignedAltRefMaker \
 *   -R reference.fasta \
 *   -sName ALK |
 *   -V input.vcf |
 *   -o output.fasta \
 *   -L input.intervals
 * </pre>
 *
 */
@DocumentedGATKFeature( groupName = HelpConstants.DOCS_CAT_REFUTILS, extraDocs = {CommandLineGATK.class} )
@Reference(window=@Window(start=-1,stop=50))
@Requires(value={DataSource.REFERENCE})
public class FastaAlignedAltRefMaker extends FastaReferenceMaker {
	/**
	 * Variants from this input file are used by this tool to construct an alternate reference.
	 */
	@ArgumentCollection
	protected StandardVariantContextInputArgumentCollection variantCollection = new StandardVariantContextInputArgumentCollection();

	/**
	 * This option will generate an error if the specified sample does not exist in the VCF.
	 */
	@Argument(fullName = "sampleName", shortName = "sName",
			doc = "Sample name from which the genotype should be selected", required = true)
	private String sampleName = null;

	/**
	 * (Optional) The character to be used for deleted bases. Default: '-'
	 */
	@Argument(fullName = "deletedBase", shortName = "delBase",
			doc = "The character to be used for deleted bases.", required = false)
	private String deletedBase = "-";

	/**
	 * (Optional) The character to be used for ambiguous bases. Default: 'N'
	 */
	@Argument(fullName = "ambiguousBase", shortName = "ambBase",
			doc = "The character to be used for ambiguous bases.", required = false)
	private String ambiguousBase = "N";

	/**
	 * If this flag is set, the sequence names from the reference fasta file will be inherited in the output
	 */
	@Argument(fullName = "inheritSeqNames", shortName = "isn",
			doc = "Inherit sequence name from the reference", required = false)
	private boolean inheritSeqNames = false;

	private int deletionBasesRemaining = 0;
	private int ambiguousBasesRemaining = 0;

	@Override
	public void initialize() {
		super.initialize();
		if ( sampleName != null ) {
			final List<String> rodName = Arrays.asList(variantCollection.variants.getName());
			final Set<String> samples = SampleUtils.getUniqueSamplesFromRods(getToolkit(), rodName);
			if ( !samples.contains(sampleName) )
				throw new UserException.BadInput("The specified sample is not present in the provided VCF file");
		} else {
			throw new UserException.BadInput("Sample name is required.");
		}
	}

	@Override
	public Pair<GenomeLoc, String> map(final RefMetaDataTracker tracker, final ReferenceContext ref, final AlignmentContext context) {

		String base = String.valueOf((char)ref.getBase());

		if (deletionBasesRemaining > 0) {
			deletionBasesRemaining--;
			base = deletedBase;
		} else if (ambiguousBasesRemaining > 0) {
			ambiguousBasesRemaining--;
			base = ambiguousBase;
		} else {
			// Check to see if we have a called variant
			for (final VariantContext vc : tracker.getValues(variantCollection.variants, ref.getLocus())) {
				// Skip filtered variants and insertions
				if (vc.isFiltered() || vc.isSimpleInsertion())
					continue;

				final Genotype genotype = vc.getGenotype(sampleName);

				// If the genotype is missing, has been filtered, or is heterozygous, the ambiguous bases will be used
				final boolean isAmbiguous = !genotype.isCalled() || genotype.isFiltered() || genotype.isHet();

				if (isAmbiguous) {
					if (vc.isSimpleDeletion()) {
						// mark the next n bases, not this one
						ambiguousBasesRemaining = vc.getReference().length() - 1;
					} else if (vc.isSNP()) {
						base = ambiguousBase;
					}
					// If multiple VCF files are provided, we use the first record that we encounter
					break;
				} else if (genotype.isHomVar()) {
					if (vc.isSimpleDeletion()) {
						// mark the next n bases, not this one
						deletionBasesRemaining = vc.getReference().length() - 1;
					} else if (vc.isSNP()) {
						// Since we are looking only at homozygous variants, all alleles should be the same, so we can just
						// grab the first one
						base = genotype.getAllele(0).toString();
					}
					// If multiple VCF files are provided, we use the first record that we encounter
					break;
				}
			}
		}

		return new Pair<>(context.getLocation(), base);
	}


	@Override
	public GenomeLoc reduce(Pair<GenomeLoc, String> value, GenomeLoc sum) {
		if (value == null)
			return sum;

		// if there is no interval to the left, then this is the first one
		if ( sum == null ) {
			sum = value.first;
			if (inheritSeqNames)
				fasta.setName(sum.getContig());
			fasta.append(value.second);
		}
		// if the intervals are not contiguous, print out the leftmost one and start a new one
		// (end of contig or new interval)
		else if ( value.first.getStart() != sum.getStop() + 1 || ! value.first.getContig().equals(sum.getContig()) ) {
			fasta.flush();
			sum = value.first;
			if (inheritSeqNames)
				fasta.setName(sum.getContig());
			fasta.append(value.second);
		}
		// otherwise, merge them
		else {
			sum = sum.setStop(sum, value.first.getStop());
			fasta.append(value.second);
		}
		return sum;
	}

}
